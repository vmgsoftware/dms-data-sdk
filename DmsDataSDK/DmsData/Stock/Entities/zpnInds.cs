﻿using VMGSoftware.DataSDK.LDap.Attributes;

namespace VMGSoftware.DataSDK.DmsData.Stock.Entities
{
    [TableInfo("zpnInds")]
    [Dapper.Contrib.Extensions.Table("zpnInds")]
    public class zpnInds
    {
        public zpnInds()
        {
            this.I1 = 0;
            this.I2 = 0;
            this.I3 = 0;
            this.I4 = 0;
            this.I5 = 0;
            this.I6 = 0;
            this.I7 = 0;
            this.I8 = 0;
            this.I9 = 0;
            this.I10 = 0;
            this.I11 = 0;
            this.I12 = 0;
            this.I13 = 0;
            this.I14 = 0;
            this.I15 = 0;
            this.I16 = 0;
            this.I17 = 0;
            this.I18 = 0;
            this.I19 = 0;
            this.I20 = 0;
            this.Transfer = false;
            this.Remove = false;
        }

        [ColumnInfo(true)]
        [Dapper.Contrib.Extensions.Key]
        public int I { get; set; } // int, not null
        public int? I1 { get; set; } // int, null
        public int? I2 { get; set; } // int, null
        public int? I3 { get; set; } // int, null
        public int? I4 { get; set; } // int, null
        public int? I5 { get; set; } // int, null
        public int? I6 { get; set; } // int, null
        public int? I7 { get; set; } // int, null
        public int? I8 { get; set; } // int, null
        public int? I9 { get; set; } // int, null
        public int? I10 { get; set; } // int, null
        public int? I11 { get; set; } // int, null
        public int? I12 { get; set; } // int, null
        public int? I13 { get; set; } // int, null
        public int? I14 { get; set; } // int, null
        public int? I15 { get; set; } // int, null
        public int? I16 { get; set; } // int, null
        public int? I17 { get; set; } // int, null
        public int? I18 { get; set; } // int, null
        public int? I19 { get; set; } // int, null
        public int? I20 { get; set; } // int, null
        public bool? Transfer { get; set; } // bit, null
        public bool? Remove { get; set; } // bit, null
    }
}
