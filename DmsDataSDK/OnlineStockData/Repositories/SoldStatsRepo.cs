﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMGSoftware.DataSDK.OnlineStockData.Repositories
{
    public class SoldStatsRepo
    {
        public static DataTable GetVehicleStats(string mmcode,string statsDbConnectionString)
        {
            var statsDt = new DataTable();
            using (var pgsqlConnection = new NpgsqlConnection(statsDbConnectionString))
            {
                using (var comand = pgsqlConnection.CreateCommand())
                {
                    comand.CommandText = $"select statistics_v1.get_vehicle_stats('{mmcode}')";
                    var dataAdapter = new NpgsqlDataAdapter(comand);
                    dataAdapter.Fill(statsDt);
                }
            }
            return statsDt;
        }
    }
}
