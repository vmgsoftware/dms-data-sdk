﻿using System.ComponentModel.DataAnnotations;

namespace VMGSoftware.DataSDK.OnlineStockData.Entities.Feeds
{
    [System.ComponentModel.DataAnnotations.Schema.Table("stock_feeds")]
    public class StockFeed
    {
        [Key]
        public int id { get; set; }

        public int stock_data_id { get; set; }
        public Feed feed { get; set; }
        public FeedUploadState feed_upload_state { get; set; }

    }
}
