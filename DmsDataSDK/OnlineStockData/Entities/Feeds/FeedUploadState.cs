﻿using System.Collections.Generic;
using Dapper.Contrib.Extensions;

namespace VMGSoftware.DataSDK.OnlineStockData.Entities.Feeds
{
    [System.ComponentModel.DataAnnotations.Schema.Table("upload_states")]
    public class FeedUploadState
    {
        [Key]
        public int id { get; set; }
        public string state { get; set; }
        public int dms_web_fag { get; set; }

        public ICollection<StockFeed> StockFeed { get; set; }
    }
}
