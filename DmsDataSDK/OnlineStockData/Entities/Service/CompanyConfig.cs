﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMGSoftware.DataSDK.OnlineStockData.Entities.Service
{
    [Table("company_configs")]
    public class CompanyConfig
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int company_id { get; set; }

        public string connection_string { get; set; }

        public ICollection<ServiceCheckin> check_ins { get; set; }

    }
}
