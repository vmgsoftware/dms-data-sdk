﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VMGSoftware.DataSDK.DmsData.Stock.Entities;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Feeds;

namespace VMGSoftware.DataSDK.OnlineStockData.Entities.Stock
{
    [Table("company_stock_data")]
    public class StockData
    {
        [Key]
        public int stock_data_id { get; set; }

        public virtual StockImageUrls StockImageUrls { get; set; } 
        public virtual StockExtraData StockExtraData { get; set; }
        public ICollection<StockFeed> StockFeed { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime date_of_upload { get; set;} // smalldatetime, null

        public int? company_id { get; set; }
        public int stock_id { get; set; } // int, not null
        public string stock_code { get; set; } // nvarchar(14), null
        public string make { get; set; } //  *******************
        public string model { get; set; } // ********************
        public int? mm_code { get; set; }
        public DateTime? purchase_date { get; set; } // smalldatetime, null
        public decimal? purchase_price { get; set; } // money, null
        public decimal? selling_price { get; set; } // money, null
        public string bank { get; set; } // ********************
        public string bank_branch { get; set; } // ********************

        public string payment_type { get; set; } // int, null
        public decimal? year { get; set; } // numeric(4,0), null
        public string engine_no { get; set; } // nvarchar(30), null
        public string chassis_no { get; set; } // nvarchar(30), null
        public decimal? km_in { get; set; } // numeric(8,0), null
        public DateTime? sold_date { get; set; } // smalldatetime, null
        public decimal? sold_price { get; set; } // money, null
        public string sales_personnel { get; set; } // ********************
        public string colour { get; set; } // ********************
        public string condition { get; set; } // ********************
        public string reg_nr { get; set; } // varchar(50), null
        public string description { get; set; } // varchar(1000), null
        public string notes { get; set; } // varchar(500), null
        public decimal? deposit_amount { get; set; } // money, null
        public DateTime? deposit_date { get; set; } // datetime, null
        public string vehicle_type { get; set; } // ********************
        public string client_title { get; set; } // ********************
        public string client_name { get; set; } // varchar(200), null
        public string client_surname { get; set; } // varchar(30), null
        public string client_contact_nr { get; set; } // varchar(20), null
        public string client_vat_nr { get; set; } // varchar(20), null
        public int? trade_in_stockid { get; set; } // int, null
        public string supplier { get; set; } // ********************
        public string location { get; set; } // ********************
        public DateTime? date_created { get; set; } // datetime, null
        public DateTime? date_updated { get; set; } // datetime, null
        public string last_user { get; set; } // nvarchar(50), null
        public decimal? invoice_extra_amt1 { get; set; } // money, null
        public decimal? invoice_extra_amt2 { get; set; } // money, null
        public decimal? invoice_extra_amt3 { get; set; } // money, null
        public decimal? invoice_extra_amt4 { get; set; } // money, null
        public bool? invoice_printed { get; set; } // bit, null
        public decimal? lic_reg_amt { get; set; } // money, null
        public string payment_ref { get; set; } // varchar(50), null
        public string supplier_invoice_nr { get; set; } // varchar(20), null
        public bool? is_private_sale { get; set; } // bit, null
        public bool? is_dealer_sale { get; set; } // bit, null
        public string sold_to_dealer { get; set; } // int, null
        public bool? archived { get; set; } // bit, null
        public string bank_discount_desc { get; set; } // varchar(50), null
        public decimal? bank_discount_amt { get; set; } // money, null
        public string invoice_number { get; set; } // nvarchar(14), null
        public DateTime? bank_receipt_date { get; set; } // smalldatetime, null
        public decimal? invoice_extra_amt5 { get; set; } // money, null
        public decimal? invoice_extra_amt6 { get; set; } // money, null
        public bool? is_new { get; set; } // bit, null
        public int? web_flag { get; set; } // int, null
        public decimal? fueling_fee { get; set; } // money, null
        public decimal? settlement_amt { get; set; } // money, null
        public bool? consignment { get; set; } // bit, null
        public bool? gl_purchase { get; set; } // bit, null
        public bool? gl_sale { get; set; } // bit, null
        public int? trade_in_stockid2 { get; set; } // int, null
        public DateTime? floorplan_date { get; set; } // smalldatetime, null
        public decimal? floorplan_price { get; set; } // money, null
        public decimal? floorplan_bank_interest { get; set; } // int, null
        public bool? floorplan { get; set; } // bit, null
        public DateTime? floorplan_date_settled { get; set; } // smalldatetime, null
        public DateTime? last_interest_run { get; set; } // datetime, null
        public bool? second_gross1 { get; set; } // bit, null
        public bool? second_gross2 { get; set; } // bit, null
        public bool? second_gross3 { get; set; } // bit, null
        public bool? second_gross4 { get; set; } // bit, null
        public bool? second_gross5 { get; set; } // bit, null
        public bool? second_gross6 { get; set; } // bit, null
        public bool? company_car { get; set; } // bit, null
        public string client_id_number { get; set; } // varchar(20), null
        public bool? spare_key { get; set; } // bit, null
        public bool? service_book { get; set; } // bit, null
        public bool? second_gross1_otp { get; set; } // bit, null
        public bool? second_gross2_otp { get; set; } // bit, null
        public bool? second_gross3_otp { get; set; } // bit, null
        public bool? second_gross4_otp { get; set; } // bit, null
        public bool? second_gross5_otp { get; set; } // bit, null
        public bool? second_gross6_otp { get; set; } // bit, null
        public bool? second_gross7 { get; set; } // bit, null
        public bool? second_gross8 { get; set; } // bit, null
        public bool? second_gross7_otp { get; set; } // bit, null
        public bool? second_gross8_otp { get; set; } // bit, null
        public string invoice_extra1 { get; set; } // *********
        public string invoice_extra2 { get; set; } // *********
        public string invoice_extra3 { get; set; } // *********
        public string invoice_extra4 { get; set; } //*********
        public string invoice_extra5 { get; set; } // *********
        public string invoice_extra6 { get; set; } // *********
        public bool? payment_made { get; set; } // bit, null
        public string sales_source { get; set; } // *********
        public string client_code { get; set; } // varchar(8), null
        public bool? wholesale { get; set; } // bit, null
        public bool? gl_purchase_removed { get; set; } // bit, null
        public bool? gl_sale_removed { get; set; } // bit, null
        public string advert_colour { get; set; } // varchar(9), null
        public string advert { get; set; } // nvarchar(2000), null
        public string advert_email { get; set; } // varchar(100), null
        public bool? stock_report { get; set; } // bit, null
        public string licence_nr { get; set; } // varchar(50), null
        public decimal? trade_price { get; set; } // money, null
        public bool? selling_price_changed { get; set; } // bit, null
        public bool? enatis_docs { get; set; } // bit, null
        public bool? dealer_2_dealer { get; set; } // bit, null
        public string province { get; set; } // *******
        public decimal? selling_price_min { get; set; } // money, null
        public decimal? retail_price { get; set; } // money, null
        public string client_contact_nr2 { get; set; } // varchar(20), null
        public string client_email { get; set; } // varchar(50), null
        public string video_url { get; set; } // varchar(300), null
        public DateTime? first_payment_date { get; set; } // smalldatetime, null
        public string client_address1 { get; set; } // varchar(100), null
        public string client_address2 { get; set; } // varchar(100), null
        public string client_address3 { get; set; } // varchar(100), null
        public decimal? km_out { get; set; } // numeric(8,0), null
        public bool? zero_rated { get; set; } // bit, null
        public decimal? mm_trade_price { get; set; } // money, null
        public string fi { get; set; } // ***************
        public string key_number { get; set; } // varchar(10), null
        public string purchased_by { get; set; } // ************
        public DateTime? first_reg_date { get; set; } // datetime, null
        public bool? on_hold { get; set; } // bit, null
        public string on_hold_person { get; set; } // varchar(200), null
        public bool? reconned { get; set; } // bit, null
        public decimal? motor_plan_km { get; set; } // numeric(7,0), null
        public DateTime? motor_plan_date { get; set; } // smalldatetime, null
        public bool? delivered { get; set; } // bit, null
        public decimal? invoice_extra_amt7 { get; set; } // money, null
        public decimal? invoice_extra_amt8 { get; set; } // money, null
        public decimal? invoice_extra_amt9 { get; set; } // money, null
        public decimal? invoice_extra_amt10 { get; set; } // money, null
        public string invoice_extra7 { get; set; } // **************
        public string invoice_extra8 { get; set; } // **************
        public string invoice_extra9 { get; set; } // **************
        public string invoice_extra10 { get; set; } // **************
        public bool? second_gross9 { get; set; } // bit, null
        public bool? second_gross10 { get; set; } // bit, null
        public bool? second_gross11 { get; set; } // bit, null
        public bool? second_gross12 { get; set; } // bit, null
        public string purchase_inv_nr { get; set; } // nvarchar(14), null
        public DateTime? tu_date_updated { get; set; } // datetime, null
        public string bank_contract_number { get; set; } // varchar(20), null
        public string floor_plan_invoice { get; set; } // varchar(20), null
        public bool? service_plan { get; set; } // bit, null
        public decimal? service_plan_km { get; set; } // numeric(7,0), null
        public DateTime? service_plan_date { get; set; } // smalldatetime, null
        public bool? warranty { get; set; } // bit, null
        public decimal? warranty_km { get; set; } // numeric(7,0), null
        public DateTime? warranty_date { get; set; } // smalldatetime, null
        public decimal? mm_trade_price_estimated { get; set; } // money, null
        public decimal? mm_retail_price_estimated { get; set; } // money, null
        public string invoice_custom_note { get; set; } // nchar(1400), null
        public decimal? next_service_km { get; set; } // numeric(7,0), null
        public DateTime? next_service_date { get; set; } // smalldatetime, null
        public string identified_by { get; set; } // ******************
        public decimal? paid_to_seller { get; set; } // money, null
        public string client_suburb { get; set; } // nvarchar(100), null
        public string client_city { get; set; } // nvarchar(100), null
        public string client_postal_code { get; set; } // nvarchar(4), null
        public bool? purchase_excludes_vat { get; set; } // bit, null
        public DateTime? license_disc_expiry_date { get; set; } // datetime, null
        public bool? was_lead { get; set; } // bit, null
        public string branch { get; set; } // ************
        public bool? sold_as_used { get; set; } // bit, null


    }
}
