﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Client.Entities
{
    [Dapper.Contrib.Extensions.Table("tblClientDetailsLog")]
    public class ClientDetailsLog
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Log Id is required")]
        public int LogId { get; set; } // int, not null
        [Required(ErrorMessage = "Date Of Change is required")]
        public DateTime DateOfChange { get; set; } // datetime, not null
        [MaxLength(500)]
        [StringLength(500)]
        [Required(ErrorMessage = "Client Code is required")]
        public string ClientCode { get; set; } // nvarchar(100), not null
        [MaxLength(500)]
        [StringLength(500)]
        [Required(ErrorMessage = "Changed is required")]
        public string Changed { get; set; } // nvarchar(500), not null
        [MaxLength(1000)]
        [StringLength(1000)]
        [Required(ErrorMessage = "Old Value is required")]
        public string OldValue { get; set; } // nvarchar(1000), not null
        [MaxLength(1000)]
        [StringLength(1000)]
        [Required(ErrorMessage = "New Value is required")]
        public string NewValue { get; set; } // nvarchar(1000), not null
        [Required(ErrorMessage = "User Id is required")]
        public int UserId { get; set; } // int, not null
    }
}
