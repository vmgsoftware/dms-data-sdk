﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.OnlineStockData.Entities.Feeds
{
    [Table("feeds")]
    public class Feed
    {
        [Key]
        public int id { get; set; }
        public string feed_name { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime date_added { get; set; }
        public bool is_active { get; set; }

        public ICollection<StockFeed> StockFeed { get; set; }
        public ICollection<CompanyFeed> CompanyFeed { get; set; }

        public Feed()
        {
            feed_name = null;
            is_active = false;
        }

        public Feed(string feedName)
        {
            feed_name = feedName;
            is_active = false;
        }
    }
}
