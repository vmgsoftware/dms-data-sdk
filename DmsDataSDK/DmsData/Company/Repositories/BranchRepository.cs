﻿using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using VMGAuto.Data;
using VMGSoftware.DataSDK.DmsData.Company.Entities;

namespace VMGSoftware.DataSDK.DmsData.Company.Repositories
{
    public class BranchRepository : GenericRepository
    {
        public void SaveBranch(Branch branch, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                sqlConnection.Insert(branch);
            }
        }

        public static int[] StockIdsInBranch(int branchId, SqlConnection ctx)
        {
            using (ctx)
            {
                var query = "select stockid from tblstock where branchId = @BranchId";
                var results = ctx.Query<int>(query, new {BranchId = branchId}).ToArray();

                return results;
            }
        }

        public static int[] SundryInvoiceIdsInBranch(int branchId, SqlConnection ctx)
        {
            using (ctx)
            {
                var query = "select SundryInvoiceId from tblSundryInvoices where branchId = @BranchId";
                var results = ctx.Query<int>(query, new {BranchId = branchId}).ToArray();

                return results;
            }
        }

        public static bool BranchHasGlAccounts(int branchId, SqlConnection ctx)
        {
            using (ctx)
            {
                var query = "select count(*) from tblGLAccounts where branchId = @BranchId";
                var results = ctx.ExecuteScalar<int>(query, new {BranchId = branchId});

                return results > 0;
            }
        }

        
    }
}
