﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using VMGSoftware.DataSDK.DmsData.Accounting.Entities;

namespace VMGSoftware.DataSDK.DmsData.Accounting.Repositories
{
    public class VatRepository
    {
        public static decimal GetCurrentVatRate(SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                
                var rate = sqlConnection.ExecuteScalar<decimal>($"select dbo.GetVatRateForDate('NULL')");

                return rate;

            }
        }

        public static decimal GetVatRatefor(DateTime date, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                var rate = sqlConnection.ExecuteScalar<decimal>($"select dbo.GetVatRateForDate('{date}')");

                return rate;

            }
        }

        public static List<Vat> LoadAll(SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                var vats = sqlConnection.GetAll<Vat>();
                return vats.ToList();
            }
        }
    }
}
