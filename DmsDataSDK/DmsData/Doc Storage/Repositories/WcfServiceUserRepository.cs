﻿using System.Data;
using System.Data.SqlClient;
using Dapper;
using VMGAuto.Data;
using VMGSoftware.DataSDK.DmsData.Doc_Storage.Entities;

namespace VMGSoftware.DataSDK.DmsData.Doc_Storage.Repositories
{
    public class WcfServiceUserRepository : GenericRepository
    {
        public static WcfServiceUser GetServiceUserByUserId(int userId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != ConnectionState.Open) sqlConnection.Open();

               return sqlConnection.QueryFirstOrDefault<WcfServiceUser>($"SELECT * FROM tblWcfServiceUsers WHERE UserId = {userId}");
            }
        }
    }
}
