﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Google.Apis.Sheets.v4;

namespace VMGSoftware.DataSDK.GoogleSheetData.Repositories
{
    public class SheetRepository <Entity> where Entity : new()
    {
        private readonly SheetsService _service;
        private readonly Entity _entity;
        private readonly PropertyInfo[] _propertyInfo;
        private readonly SheetInteraction _sheetInteraction;
        private readonly int _rowsStartAt;

        public SheetRepository(string sheetId,int rowsStartAt, SheetsService service)
        {
            _service = service;
            _entity = new Entity();
            _propertyInfo = _entity.GetType().GetProperties();
            _sheetInteraction = new SheetInteraction(sheetId,service);
            _rowsStartAt = rowsStartAt;
        }

        public SheetRepository(string sheetId,int rowsStartAt, string clientId,string clientSecret)
        {
            _service = AppClient.AuthorizeGoogleApp(clientId,clientSecret);
            _entity = new Entity();
            _propertyInfo = _entity.GetType().GetProperties();
            _sheetInteraction = new SheetInteraction(sheetId,_service);
            _rowsStartAt = rowsStartAt;
        }

        public SheetRepository(string sheetId,int rowsStartAt, string clientId,string clientSecret,string refreshToken)
        {
            _service = AppClient.AuthorizeGoogleApp(clientId,clientSecret,refreshToken);
            _entity = new Entity();
            _propertyInfo = _entity.GetType().GetProperties();
            _sheetInteraction = new SheetInteraction(sheetId,_service);
            _rowsStartAt = rowsStartAt;
        }

        public SheetRepository(string sheetId,int rowsStartAt)
        {
            _service = AppClient.AuthorizeGoogleApp();
            _entity = new Entity();
            _propertyInfo = _entity.GetType().GetProperties();
            _sheetInteraction = new SheetInteraction(sheetId,_service);
            _rowsStartAt = rowsStartAt;
        }

        public Entity Load(string id)
        {
            int rowIndex = FindRowIndexById(id);

            foreach (var property in _propertyInfo)
            {
                char columnLetter = GetCellInfo(property).ColumnLetter;
                object value = _sheetInteraction.GetCellValue(columnLetter, rowIndex);             
                property.SetValue(_entity,Convert.ChangeType(value,property.PropertyType));
            }
            return _entity;
        }

        private int FindRowIndexById(string id)
        {
            var idColumnLetter = GetIdColumnLetter();

            int idIndex = 9999999;
            IList<string> sheetIds = _sheetInteraction.GetColumn(idColumnLetter, (_rowsStartAt)).Cast<string>().ToList();
            for (int i = 0; i < sheetIds.Count; i++)
            {
                if (sheetIds[i] == id.ToString())
                {
                   return i+(_rowsStartAt - 1) +1;
                }
            }

            return idIndex;
        }

        public CellInfo GetCellInfo(PropertyInfo propertyInfo)
        {
            return (CellInfo)propertyInfo.GetCustomAttributes(typeof(CellInfo), false)[0];
        }

        public char GetIdColumnLetter()
        {
            char idColumnLetter = 'x';
            foreach (var p in _propertyInfo)
            {
                if (GetCellInfo(p).IsIdColumn)
                {
                    idColumnLetter = GetCellInfo(p).ColumnLetter;
                }
            }

            return idColumnLetter;
        }

        public void Update(Entity entity)
        {
            int rowIndex = FindRowIndexById(GetIdFromEntity(entity));

            foreach (var property in _propertyInfo)
            {
                char columnLetter = GetCellInfo(property).ColumnLetter;
                object value = property.GetValue(entity);
                if (value != null)
                {
                    _sheetInteraction.UpdateCell(value,columnLetter,rowIndex);
                }
            }
        }


        private string GetIdFromEntity(Entity entity)
        {
            foreach (var p in _propertyInfo)
            {
                if (GetCellInfo(p).IsIdColumn)
                {
                    return p.GetValue(entity).ToString();
                }
            }

            return "0";
        }

        public void Insert(Entity entity)
        {
            int nextRow = GetNextAvalibleRow();
            foreach (var property in _propertyInfo)
            {
                char columnLetter = GetCellInfo(property).ColumnLetter;
                object value = property.GetValue(entity);
                if (value != null)
                {
                    _sheetInteraction.UpdateCell(value,columnLetter,nextRow);
                }
            }
        }

        private int GetNextAvalibleRow()
        {
            return _sheetInteraction.GetColumn(GetIdColumnLetter(), _rowsStartAt).Count + _rowsStartAt;
        }

        public void Upsert(Entity entity)
        {
            string id = GetIdFromEntity(entity);
            IList<object> idList = _sheetInteraction.GetColumn(GetIdColumnLetter(), _rowsStartAt);

            if (idList.Count(x => x?.ToString() == id.ToString()) > 0)
            {
                Update(entity);
            }
            else
            {
                Insert(entity);
            }
        }
    }
}
