﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using VMGSoftware.DataSDK.DmsData.ServiceQueue.Entities;

namespace VMGSoftware.DataSDK.DmsData.ServiceQueue.Reposiotories
{
    public class ServiceQueueRepository : GenericRepository
    {
        public static void QueueACommand(string connectionString,string command, bool isPriority,params string[] commandParameters)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                var commandToQueue = new QueuedCommand();
                commandToQueue.Command = command;
                commandToQueue.DateQueued = DateTime.Now;
                commandToQueue.IsPriority = isPriority;
                commandToQueue.Status = "QUEUED";
                commandToQueue.Parameters =  String.Join("|", commandParameters.ToArray());

                GenericRepository.Insert(commandToQueue, sqlConnection);
            }
        }

        public static void QueueACommand(string connectionString,string command, bool isPriority, List<string> commandParameters)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                var commandToQueue = new QueuedCommand();
                commandToQueue.Command = command;
                commandToQueue.DateQueued = DateTime.Now;
                commandToQueue.IsPriority = isPriority;
                commandToQueue.Status = "QUEUED";
                commandToQueue.Parameters =  String.Join("|", commandParameters.ToArray());

                GenericRepository.Insert(commandToQueue, sqlConnection);
            }
        }

        public static void QueueACommandNoDuplicates(string connectionString,string command, bool isPriority,params string[] commandParameters)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                var queuedItemCount = sqlConnection.QueryFirstOrDefault<int>(
                    $"SELECT COUNT(*) FROM dbo.tblServiceQueue WHERE [Status] = 'QUEUED' AND Command = '{command}'");
                if (queuedItemCount != 0) return;

                var commandToQueue = new QueuedCommand();
                commandToQueue.Command = command;
                commandToQueue.DateQueued = DateTime.Now;
                commandToQueue.IsPriority = isPriority;
                commandToQueue.Status = "QUEUED";
                commandToQueue.Parameters =  String.Join("|", commandParameters.ToArray());

                GenericRepository.Insert(commandToQueue, sqlConnection);
            }
        }

        public static void QueueACommandNoDuplicates(string connectionString,string command, bool isPriority, List<string> commandParameters)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                var queuedItemCount = sqlConnection.QueryFirstOrDefault<int>(
                    $"SELECT COUNT(*) FROM dbo.tblServiceQueue WHERE [Status] = 'QUEUED' AND Command = '{command}'");
                if (queuedItemCount != 0) return;

                var commandToQueue = new QueuedCommand();
                commandToQueue.Command = command;
                commandToQueue.DateQueued = DateTime.Now;
                commandToQueue.IsPriority = isPriority;
                commandToQueue.Status = "QUEUED";
                commandToQueue.Parameters =  String.Join("|", commandParameters.ToArray());

                GenericRepository.Insert(commandToQueue, sqlConnection);
            }
        }
    }
}
