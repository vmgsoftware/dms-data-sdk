﻿using System.Data;
using System.Data.SqlClient;
using Dapper;
using VMGAuto.Data;
using VMGSoftware.DataSDK.DmsData.Auditing.Entities;

namespace VMGSoftware.DataSDK.DmsData.Auditing.Repositories
{
    public class StockMappingRepository : GenericRepository
    {
        public static StockMappings GetStockMappingByColumnName(string columnName, SqlConnection sqlConnection)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                sqlConnection.Open();
            }

            return sqlConnection.QueryFirstOrDefault<StockMappings>(
                $"Select * from tblStockMappings where StockColumnName = '{columnName}'");
        }

        public static string GetDescriptorValue(StockMappings stockMapping,string foreignKeyId, SqlConnection sqlConnection)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                sqlConnection.Open();
            }


            return sqlConnection.QueryFirstOrDefault<string>(
                $"Select {stockMapping.MapsToTableDescriptor} from {stockMapping.MapsToTableName} where {stockMapping.MapsToTablePK} = '{foreignKeyId}'");
        }
    }
}
