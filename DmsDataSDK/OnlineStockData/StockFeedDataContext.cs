﻿using System.Data.Common;
using System.Data.Entity;
using Npgsql;
using VMGSoftware.DataSDK.DmsData.Stock.Entities;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Feeds;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Products;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Service;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Stock;

namespace VMGSoftware.DataSDK.OnlineStockData.Migrations
{
    public class StockFeedDataContext : DbContext
    {
        private const string StagingConnectionString =
            "Server=154.66.198.70;database=vmg_statistics;User id=postgres;password=z6+gBAcCVLkiRtbWwUX3%a?nEHqD_hNjm6FGyk&amp;e7o8s~DY4ru;port=16839";

        private const string ProductionConnectionString =
            "Server=154.66.198.70;database=vmg_statistics;User id=postgres;password=v%4TLYXKymrHUeqN6~Vb7afo9GJ6gQdh~MFk?tDc_-AE+v3Uxj;port=16848";

        public DbSet<StockData> StockData { get; set; }
        public DbSet<StockExtraData> StockExtras { get; set; }
        public DbSet<CompanyFeed> CompanyFeeds { get; set; }
        public DbSet<Feed> Feeds { get; set; }
        public DbSet<StockFeed> StockFeeds { get; set; }
        public DbSet<StockImageUrls> StockImageUrls { get; set; }
        public DbSet<FeedUploadState> FeedUploadStates { get; set; }
        public DbSet<CompanyConfig> CompanyConfigs { get; set; }
        public DbSet<ServiceCheckin> ServiceCheckIns { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<CompanyProduct> CompanyProducts { get; set; }
        public DbSet<ProductMaping> ProductMappings { get; set; }

        public StockFeedDataContext() : base(GetDbConnection(ProductionConnectionString), true)
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<StockFeedDataContext>());
        }

        public StockFeedDataContext(bool production) : base(
            GetDbConnection(production ? ProductionConnectionString : StagingConnectionString), true)
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<StockFeedDataContext>());
        }

        public StockFeedDataContext(string connectionString) : base(GetDbConnection(connectionString), true)
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<StockFeedDataContext>());
        }

        public static DbConnection GetDbConnection(string connectionString)
        {
            return new NpgsqlConnection(connectionString);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("company_data_v1");
            
            base.OnModelCreating(modelBuilder);
        }
    }
}