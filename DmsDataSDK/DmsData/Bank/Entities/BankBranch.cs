﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Bank.Entities
{
    [Dapper.Contrib.Extensions.Table("tblBankBranches")]
    public class BankBranch
    {
        public BankBranch()
        {
            this.Deleted = false;
            this.DateCreated = DateTime.Now;
            this.BankSuburb = "";
            this.Bankaddress3 = "";
            this.BankCity = "";
            this.BankPostalCode = "";
        }

        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Bank Ref is required")]
        public int BankRef { get; set; } // int, not null
        public int? BankID { get; set; } // int, null
        [MaxLength(60)]
        public string BranchName { get; set; } // varchar(60), null
        [MaxLength(10)]
        public string BranchCode { get; set; } // varchar(10), null
        [MaxLength(30)]
        public string BankTelNo { get; set; } // varchar(30), null
        public bool? Deleted { get; set; } // bit, null
        public DateTime? DateCreated { get; set; } // datetime, null
        public DateTime? DateUpdated { get; set; } // datetime, null
        public int? LastUserID { get; set; } // int, null
        [MaxLength(80)]
        public string bankaddress { get; set; } // varchar(80), null
        [MaxLength(80)]
        public string bankaddress2 { get; set; } // varchar(80), null
        [MaxLength(25)]
        public string BankSuburb { get; set; } // nvarchar(25), null
        [MaxLength(25)]
        public string Bankaddress3 { get; set; } // nvarchar(25), null
        [MaxLength(25)]
        public string BankCity { get; set; } // nvarchar(25), null
        [MaxLength(4)]
        public string BankPostalCode { get; set; } // nvarchar(4), null
    }

}
