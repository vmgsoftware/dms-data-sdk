﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Stock.Entities
{
    [Table("company_stock_data")]
    public class StockDataToUpload
    {
        [Key]
        public int id { get; set; }
        public int StockID { get; set; } // int, not null
        public string StockCode { get; set; } // nvarchar(14), null
        public string MakeID { get; set; } // nvarchar(3), null
        public int? ModelID { get; set; } // int, null
        public int? BankRef { get; set; } // int, null
        public DateTime? PurchaseDate { get; set; } // smalldatetime, null
        public decimal? PurchasePrice { get; set; } // money, null
        public int? PaymentTypeID { get; set; } // int, null
        public decimal? SellingPrice { get; set; } // money, null
        public decimal? Year { get; set; } // numeric(4,0), null
        public string EngineNo { get; set; } // nvarchar(30), null
        public string ChassisNo { get; set; } // nvarchar(30), null
        [Column("KM-in")]
        public decimal? KM_in { get; set; } // numeric(8,0), null
        public DateTime? SoldDate { get; set; } // smalldatetime, null
        public decimal? SoldPrice { get; set; } // money, null 
        public int? SalesPersonnelID { get; set; } // int, null
        public int? ColourID { get; set; } // int, null
        public int? ConditionID { get; set; } // int, null
        public string RegNr { get; set; } // varchar(50), null
        public string Description { get; set; } // varchar(1000), null
        public string Notes { get; set; } // varchar(500), null
        public decimal? DepositAmount { get; set; } // money, null
        public DateTime? DepositDate { get; set; } // datetime, null
        public int? VehicleTypeID { get; set; } // int, null
        public int? ClientTitleID { get; set; } // int, null
        public string ClientName { get; set; } // varchar(200), null
        public string ClientSurname { get; set; } // varchar(30), null
        public string ClientContactNr { get; set; } // varchar(20), null
        public string ClientVATNr { get; set; } // varchar(20), null
        public int? TradeInStockID { get; set; } // int, null
        public int? SupplierID { get; set; } // int, null
        public int? LocationID { get; set; } // int, null
        public DateTime? DateCreated { get; set; } // datetime, null
        public DateTime? DateUpdated { get; set; } // datetime, null
        public string LastUser { get; set; } // nvarchar(50), null
        public decimal? InvoiceExtraAmt1 { get; set; } // money, null
        public decimal? InvoiceExtraAmt2 { get; set; } // money, null
        public decimal? InvoiceExtraAmt3 { get; set; } // money, null
        public decimal? InvoiceExtraAmt4 { get; set; } // money, null
        public bool? InvoicePrinted { get; set; } // bit, null
        public decimal? LicRegAmt { get; set; } // money, null
        public string Paymentref { get; set; } // varchar(50), null
        public string SupplierInvoiceNr { get; set; } // varchar(20), null
        public bool? IsPrivateSale { get; set; } // bit, null
        public bool? isDealerSale { get; set; } // bit, null
        public int? DealerID { get; set; } // int, null
        public bool? Archived { get; set; } // bit, null
        public string Bankdiscountdesc { get; set; } // varchar(50), null
        public decimal? bankdiscountamt { get; set; } // money, null
        public string InvoiceNumber { get; set; } // nvarchar(14), null
        public DateTime? BankReceiptDate { get; set; } // smalldatetime, null
        public decimal? InvoiceExtraAmt5 { get; set; } // money, null
        public decimal? InvoiceExtraAmt6 { get; set; } // money, null
        public bool? New { get; set; } // bit, null
        public int? WebFlag { get; set; } // int, null
        public decimal? Fuelingfee { get; set; } // money, null
        public decimal? SettlementAmt { get; set; } // money, null
        public bool? Consignment { get; set; } // bit, null
        public bool? glPurchase { get; set; } // bit, null
        public bool? glSale { get; set; } // bit, null
        public int? TradeInStockID2 { get; set; } // int, null
        public DateTime? floorplandate { get; set; } // smalldatetime, null
        public decimal? floorplanprice { get; set; } // money, null
        public int? floorplanbank { get; set; } // int, null
        public bool? floorplan { get; set; } // bit, null
        public DateTime? floorPlanDateSettled { get; set; } // smalldatetime, null
        public DateTime? LastInterestRun { get; set; } // datetime, null
        public bool? SecondGross1 { get; set; } // bit, null
        public bool? SecondGross2 { get; set; } // bit, null
        public bool? SecondGross3 { get; set; } // bit, null
        public bool? SecondGross4 { get; set; } // bit, null
        public bool? SecondGross5 { get; set; } // bit, null
        public bool? SecondGross6 { get; set; } // bit, null
        public bool? CompanyCar { get; set; } // bit, null
        public string ClientIDNr { get; set; } // varchar(20), null
        public int? companyID { get; set; } // int, null
        public bool? SpareKey { get; set; } // bit, null
        public bool? ServiceBook { get; set; } // bit, null
        public bool? SecondGross1OTP { get; set; } // bit, null
        public bool? SecondGross2OTP { get; set; } // bit, null
        public bool? SecondGross3OTP { get; set; } // bit, null
        public bool? SecondGross4OTP { get; set; } // bit, null
        public bool? SecondGross5OTP { get; set; } // bit, null
        public bool? SecondGross6OTP { get; set; } // bit, null
        public bool? SecondGross7 { get; set; } // bit, null
        public bool? SecondGross8 { get; set; } // bit, null
        public bool? SecondGross7OTP { get; set; } // bit, null
        public bool? SecondGross8OTP { get; set; } // bit, null
        public int? InvoiceExtraTypeID1 { get; set; } // int, null
        public int? InvoiceExtraTypeID2 { get; set; } // int, null
        public int? InvoiceExtraTypeID3 { get; set; } // int, null
        public int? InvoiceExtraTypeID4 { get; set; } // int, null
        public int? InvoiceExtraTypeID5 { get; set; } // int, null
        public int? InvoiceExtraTypeID6 { get; set; } // int, null
        public bool? PaymentMade { get; set; } // bit, null
        public int? SalesSourceID { get; set; } // int, null
        public string clientCode { get; set; } // varchar(8), null
        public bool? Wholesale { get; set; } // bit, null
        public bool? glPurchaseRemoved { get; set; } // bit, null
        public bool? glSaleRemoved { get; set; } // bit, null
        public string AdvertColour { get; set; } // varchar(9), null
        public string advert { get; set; } // nvarchar(2000), null
        public string advertEmail { get; set; } // varchar(100), null
        public bool? StockReport { get; set; } // bit, null
        public string licenceNr { get; set; } // varchar(50), null
        public decimal? TradePrice { get; set; } // money, null
        public bool? sellingPriceChanged { get; set; } // bit, null
        public bool? enatisDocs { get; set; } // bit, null
        public int? ProvinceID { get; set; } // int, null
        public decimal? SellingPriceMin { get; set; } // money, null
        public decimal? RetailPrice { get; set; } // money, null
        public string ClientContactNr2 { get; set; } // varchar(20), null
        public string ClientEmail { get; set; } // varchar(50), null
        public string VideoURL { get; set; } // varchar(300), null
        public DateTime? FirstPaymentDate { get; set; } // smalldatetime, null
        public string ClientAddress1 { get; set; } // varchar(100), null
        public string ClientAddress2 { get; set; } // varchar(100), null
        public string ClientAddress3 { get; set; } // varchar(100), null
        public decimal? KMout { get; set; } // numeric(8,0), null
        public bool? ZeroRated { get; set; } // bit, null
        public decimal? MMTradePrice { get; set; } // money, null
        public int? fiID { get; set; } // int, null
        public string KeyNumber { get; set; } // varchar(10), null
        public int? PurchasedByID { get; set; } // int, null
        public DateTime? FirstRegDate { get; set; } // datetime, null
        public bool? OnHold { get; set; } // bit, null
        public string OnHoldPerson { get; set; } // varchar(200), null
        public bool? Reconned { get; set; } // bit, null
        public decimal? MotorPlanKM { get; set; } // numeric(7,0), null
        public DateTime? MotorPlanDate { get; set; } // smalldatetime, null
        public bool? Delivered { get; set; } // bit, null
        public decimal? InvoiceExtraAmt7 { get; set; } // money, null
        public decimal? InvoiceExtraAmt8 { get; set; } // money, null
        public decimal? InvoiceExtraAmt9 { get; set; } // money, null
        public decimal? InvoiceExtraAmt10 { get; set; } // money, null
        public int? InvoiceExtraTypeID7 { get; set; } // int, null
        public int? InvoiceExtraTypeID8 { get; set; } // int, null
        public int? InvoiceExtraTypeID9 { get; set; } // int, null
        public int? InvoiceExtraTypeID10 { get; set; } // int, null
        public bool? SecondGross9 { get; set; } // bit, null
        public bool? SecondGross10 { get; set; } // bit, null
        public bool? SecondGross11 { get; set; } // bit, null
        public bool? SecondGross12 { get; set; } // bit, null
        public string PurchaseInvNr { get; set; } // nvarchar(14), null
        public DateTime? TUDateUpdated { get; set; } // datetime, null
        public string BankContractNumber { get; set; } // varchar(20), null
        public string FloorPlanInvoice { get; set; } // varchar(20), null
        public bool? ServicePlan { get; set; } // bit, null
        public decimal? ServicePlanKM { get; set; } // numeric(7,0), null
        public DateTime? ServicePlanDate { get; set; } // smalldatetime, null
        public bool? Warranty { get; set; } // bit, null
        public decimal? WarrantyKM { get; set; } // numeric(7,0), null
        public DateTime? WarrantyDate { get; set; } // smalldatetime, null
        public decimal? MMTradePriceEstimated { get; set; } // money, null
        public decimal? MMRetailPriceEstimated { get; set; } // money, null
        public string InvoiceCustomNote { get; set; } // nchar(1400), null
        public decimal? NextServiceKM { get; set; } // numeric(7,0), null
        public DateTime? NextServiceDate { get; set; } // smalldatetime, null
        public int? IdentifiedById { get; set; } // int, null
        public decimal? paid_to_seller { get; set; } // money, null
        public string ClientSuburb { get; set; } // nvarchar(100), null
        public string ClientCity { get; set; } // nvarchar(100), null
        public string ClientPostalCode { get; set; } // nvarchar(4), null
        public bool? purchaseExcludesVat { get; set; } // bit, null
        public DateTime? LicenseDiscExpiryDate { get; set; } // datetime, null
        public bool? wasLead { get; set; } // bit, null
        public int? BranchId { get; set; } // int, null
        public bool? SoldAsUsed { get; set; } // bit, null


        public short swarm { get; set; } // smallint, not null
        public int? BpfmcWeb { get; set; } // int, null
        public int? pricecheck { get; set; } // int, null
        public int? hippoCoZa { get; set; } // int, null
        public int? capitec { get; set; } // int, null
        public int? redfeed { get; set; } // int, null
        public int? gumtree { get; set; } // int, null
        public int? allauto { get; set; } // int, null
        public int? bluechip { get; set; } // int, null
        public int? carscoza { get; set; } // int, null
        public int? surf4cars { get; set; } // int, null
        public int? carfind { get; set; } // int, null
        public int? autodealer { get; set; } // int, null
        public int? d2d { get; set; } // int, null
        public int? carplace { get; set; } // int, null
        public int? olx { get; set; } // int, null
        public int? automart { get; set; } // int, null
        
        public bool? c2 { get; set; } // bit, null
        public bool? c3 { get; set; } // bit, null
        public bool? c4 { get; set; } // bit, null
        public bool? c5 { get; set; } // bit, null

    }
}
