﻿using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions;

namespace VMGSoftware.DataSDK.DmsData.User.Entities
{
    [Dapper.Contrib.Extensions.Table("tblSecurity")]
    public class Security
    {
        public Security()
        {
            this.S1 = false;
            this.S2 = false;
            this.S3 = false;
            this.S4 = false;
            this.S5 = false;
            this.S6 = false;
            this.S7 = false;
            this.S8 = false;
            this.S9 = false;
            this.S10 = false;
            this.S11 = false;
            this.S12 = false;
            this.S13 = false;
            this.S14 = false;
            this.S15 = false;
            this.S16 = false;
            this.S17 = false;
            this.S18 = false;
            this.S19 = false;
            this.S20 = false;
            this.S21 = false;
            this.S22 = false;
            this.S23 = false;
            this.S24 = false;
            this.S25 = false;
            this.S26 = false;
            this.S27 = false;
            this.S28 = false;
            this.S29 = false;
            this.S30 = false;
            this.S31 = false;
            this.S32 = false;
            this.S33 = false;
            this.S34 = false;
            this.S35 = false;
            this.S36 = false;
            this.S37 = false;
            this.S38 = false;
            this.S39 = false;
            this.S40 = false;
            this.S41 = false;
            this.S42 = false;
            this.S43 = false;
            this.S44 = false;
            this.S45 = false;
            this.S46 = false;
            this.S47 = false;
            this.S48 = false;
            this.S49 = false;
            this.S50 = false;
            this.S51 = false;
            this.S52 = false;
            this.S53 = false;
            this.S54 = false;
            this.S55 = false;
            this.S56 = false;
            this.S57 = false;
            this.S58 = false;
            this.S59 = false;
            this.S60 = false;
            this.S61 = false;
            this.S62 = false;
            this.S63 = false;
            this.S64 = false;
            this.S65 = false;
            this.S66 = false;
            this.S67 = false;
            this.S68 = false;
            this.S69 = false;
            this.S70 = false;
            this.S71 = false;
            this.S72 = false;
            this.S73 = false;
            this.S74 = false;
            this.S75 = false;
            this.S76 = false;
            this.S77 = false;
            this.S78 = false;
            this.S79 = false;
            this.S80 = false;
            this.S81 = false;
            this.S82 = false;
            this.S83 = false;
            this.S84 = false;
            this.S85 = false;
            this.S86 = false;
            this.S87 = false;
            this.S88 = false;
            this.S89 = false;
            this.S90 = false;
            this.S91 = false;
            this.S92 = false;
            this.S93 = false;
            this.S94 = false;
            this.S95 = false;
            this.S96 = false;
            this.S97 = false;
            this.s98 = false;
            this.s99 = false;
            this.s100 = false;
            this.s101 = false;
            this.s102 = false;
            this.s103 = false;
            this.s104 = false;
            this.s105 = false;
            this.s106 = false;
            this.s107 = false;
            this.s108 = false;
            this.S109 = false;
            this.s110 = false;
            this.s111 = false;
            this.s112 = false;
            this.s113 = false;
            this.s114 = false;
            this.s115 = false;
            this.s116 = false;
            this.s117 = false;
            this.s118 = false;
            this.S119 = false;
            this.S120 = false;
            this.S121 = false;
            this.S122 = false;
            this.S123 = false;
            this.S134 = false;
            this.S135 = false;
            this.S124 = false;
            this.S125 = false;
            this.S126 = false;
            this.S127 = false;
            this.S128 = false;
            this.S129 = false;
            this.S130 = false;
            this.S131 = false;
            this.S132 = false;
            this.S133 = false;
            this.S136 = false;
            this.S137 = false;
            this.s138 = false;
        }

        [Dapper.Contrib.Extensions.Key]
        [Required(ErrorMessage = "User Level ID is required")]
        public int UserLevelID { get; set; } // int, not null
        public bool? S1 { get; set; } // bit, null
        public bool? S2 { get; set; } // bit, null
        public bool? S3 { get; set; } // bit, null
        public bool? S4 { get; set; } // bit, null
        public bool? S5 { get; set; } // bit, null
        public bool? S6 { get; set; } // bit, null
        public bool? S7 { get; set; } // bit, null
        public bool? S8 { get; set; } // bit, null
        public bool? S9 { get; set; } // bit, null
        public bool? S10 { get; set; } // bit, null
        public bool? S11 { get; set; } // bit, null
        public bool? S12 { get; set; } // bit, null
        public bool? S13 { get; set; } // bit, null
        public bool? S14 { get; set; } // bit, null
        public bool? S15 { get; set; } // bit, null
        public bool? S16 { get; set; } // bit, null
        public bool? S17 { get; set; } // bit, null
        public bool? S18 { get; set; } // bit, null
        public bool? S19 { get; set; } // bit, null
        public bool? S20 { get; set; } // bit, null
        public bool? S21 { get; set; } // bit, null
        public bool? S22 { get; set; } // bit, null
        public bool? S23 { get; set; } // bit, null
        public bool? S24 { get; set; } // bit, null
        public bool? S25 { get; set; } // bit, null
        public bool? S26 { get; set; } // bit, null
        public bool? S27 { get; set; } // bit, null
        public bool? S28 { get; set; } // bit, null
        public bool? S29 { get; set; } // bit, null
        public bool? S30 { get; set; } // bit, null
        public bool? S31 { get; set; } // bit, null
        public bool? S32 { get; set; } // bit, null
        public bool? S33 { get; set; } // bit, null
        public bool? S34 { get; set; } // bit, null
        public bool? S35 { get; set; } // bit, null
        public bool? S36 { get; set; } // bit, null
        public bool? S37 { get; set; } // bit, null
        public bool? S38 { get; set; } // bit, null
        public bool? S39 { get; set; } // bit, null
        public bool? S40 { get; set; } // bit, null
        public bool? S41 { get; set; } // bit, null
        public bool? S42 { get; set; } // bit, null
        public bool? S43 { get; set; } // bit, null
        public bool? S44 { get; set; } // bit, null
        public bool? S45 { get; set; } // bit, null
        public bool? S46 { get; set; } // bit, null
        public bool? S47 { get; set; } // bit, null
        public bool? S48 { get; set; } // bit, null
        public bool? S49 { get; set; } // bit, null
        public bool? S50 { get; set; } // bit, null
        public bool? S51 { get; set; } // bit, null
        public bool? S52 { get; set; } // bit, null
        public bool? S53 { get; set; } // bit, null
        public bool? S54 { get; set; } // bit, null
        public bool? S55 { get; set; } // bit, null
        public bool? S56 { get; set; } // bit, null
        public bool? S57 { get; set; } // bit, null
        public bool? S58 { get; set; } // bit, null
        public bool? S59 { get; set; } // bit, null
        public bool? S60 { get; set; } // bit, null
        public bool? S61 { get; set; } // bit, null
        public bool? S62 { get; set; } // bit, null
        public bool? S63 { get; set; } // bit, null
        public bool? S64 { get; set; } // bit, null
        public bool? S65 { get; set; } // bit, null
        public bool? S66 { get; set; } // bit, null
        public bool? S67 { get; set; } // bit, null
        public bool? S68 { get; set; } // bit, null
        public bool? S69 { get; set; } // bit, null
        public bool? S70 { get; set; } // bit, null
        public bool? S71 { get; set; } // bit, null
        public bool? S72 { get; set; } // bit, null
        public bool? S73 { get; set; } // bit, null
        public bool? S74 { get; set; } // bit, null
        public bool? S75 { get; set; } // bit, null
        public bool? S76 { get; set; } // bit, null
        public bool? S77 { get; set; } // bit, null
        public bool? S78 { get; set; } // bit, null
        public bool? S79 { get; set; } // bit, null
        public bool? S80 { get; set; } // bit, null
        public bool? S81 { get; set; } // bit, null
        public bool? S82 { get; set; } // bit, null
        public bool? S83 { get; set; } // bit, null
        public bool? S84 { get; set; } // bit, null
        public bool? S85 { get; set; } // bit, null
        public bool? S86 { get; set; } // bit, null
        public bool? S87 { get; set; } // bit, null
        public bool? S88 { get; set; } // bit, null
        public bool? S89 { get; set; } // bit, null
        public bool? S90 { get; set; } // bit, null
        public bool? S91 { get; set; } // bit, null
        public bool? S92 { get; set; } // bit, null
        public bool? S93 { get; set; } // bit, null
        public bool? S94 { get; set; } // bit, null
        public bool? S95 { get; set; } // bit, null
        public bool? S96 { get; set; } // bit, null
        public bool? S97 { get; set; } // bit, null
        public bool? s98 { get; set; } // bit, null
        public bool? s99 { get; set; } // bit, null
        public bool? s100 { get; set; } // bit, null
        public bool? s101 { get; set; } // bit, null
        public bool? s102 { get; set; } // bit, null
        public bool? s103 { get; set; } // bit, null
        public bool? s104 { get; set; } // bit, null
        public bool? s105 { get; set; } // bit, null
        public bool? s106 { get; set; } // bit, null
        public bool? s107 { get; set; } // bit, null
        public bool? s108 { get; set; } // bit, null
        public bool? S109 { get; set; } // bit, null
        public bool? s110 { get; set; } // bit, null
        public bool? s111 { get; set; } // bit, null
        public bool? s112 { get; set; } // bit, null
        public bool? s113 { get; set; } // bit, null
        public bool? s114 { get; set; } // bit, null
        public bool? s115 { get; set; } // bit, null
        public bool? s116 { get; set; } // bit, null
        public bool? s117 { get; set; } // bit, null
        public bool? s118 { get; set; } // bit, null
        public bool? S119 { get; set; } // bit, null
        public bool? S120 { get; set; } // bit, null
        public bool? S121 { get; set; } // bit, null
        public bool? S122 { get; set; } // bit, null
        public bool? S123 { get; set; } // bit, null
        public bool? S134 { get; set; } // bit, null
        public bool? S135 { get; set; } // bit, null
        public bool? S124 { get; set; } // bit, null
        public bool? S125 { get; set; } // bit, null
        public bool? S126 { get; set; } // bit, null
        public bool? S127 { get; set; } // bit, null
        public bool? S128 { get; set; } // bit, null
        public bool? S129 { get; set; } // bit, null
        public bool? S130 { get; set; } // bit, null
        public bool? S131 { get; set; } // bit, null
        public bool? S132 { get; set; } // bit, null
        public bool? S133 { get; set; } // bit, null
        public bool? S136 { get; set; } // bit, null
        public bool? S137 { get; set; } // bit, null
        public bool? s138 { get; set; } // bit, null

        [Computed]
        public bool CanViewLeads { get => S126 ?? false; set => S126 = value; }
        [Computed]
        public bool CanAddLeads { get => S135 ?? false; set => S135 = value; }
        [Computed]
        public bool CanRunTranunionUpdates { get => s117 ?? false; set => s117 = value; }
        [Computed]
        public bool CanEditPurchasePrice { get => S123 ?? false; set => S123 = value; }

    }
}
