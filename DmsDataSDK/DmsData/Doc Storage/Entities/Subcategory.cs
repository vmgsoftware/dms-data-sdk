﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Doc_Storage.Entities
{
    [Dapper.Contrib.Extensions.Table("tblDocStorageSubcategories")]

    public class Subcategory
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Subcategory Id is required")]
        public int SubcategoryId { get; set; } // int, not null
        [MaxLength(500)]
        [StringLength(500)]
        [Required(ErrorMessage = "Subcategory Name is required")]
        public string SubcategoryName { get; set; } // nvarchar(500), not null
        [MaxLength(1000)]
        [StringLength(1000)]
        [Required(ErrorMessage = "Subcategory Description is required")]
        public string SubcategoryDescription { get; set; } // nvarchar(1000), not null
        [Required(ErrorMessage = "Category Id is required")]
        public int CategoryId { get; set; } // int, not null

    }
}
