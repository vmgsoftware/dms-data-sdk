﻿using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions;
using System.ComponentModel.DataAnnotations.Schema;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Stock;
using VMGSoftware.DataSDK.LDap.Attributes;

namespace VMGSoftware.DataSDK.DmsData.Stock.Entities
{
    [Dapper.Contrib.Extensions.Table("tblStockImages")]
    [System.ComponentModel.DataAnnotations.Schema.Table("stock_images")]
    public class StockImageUrls
    {
        [System.ComponentModel.DataAnnotations.Key]
        [ForeignKey("StockData")]
        public int stock_data_id { get; set; }
        [Column("company_id")]
        public int CompanyID { get; set; }
        public virtual StockData StockData { get; set; }
        [Column("stock_id")]
        public int? StockID { get; set; } // int, null
        [Column("image_url1")]
        public string ImageURL1 { get; set; } // varchar(250), null
        [Column("image_url2")]
        public string ImageURL2 { get; set; } // varchar(250), null
        [Column("image_url3")]
        public string ImageURL3 { get; set; } // varchar(250), null
        [Column("image_url4")]
        public string ImageURL4 { get; set; } // varchar(250), null
        [Column("image_url5")]
        public string ImageURL5 { get; set; } // varchar(250), null
        [Column("image_url6")]
        public string ImageURL6 { get; set; } // varchar(250), null
        [Column("image_url7")]
        public string ImageURL7 { get; set; } // varchar(250), null
        [Column("image_url8")]
        public string ImageURL8 { get; set; } // varchar(250), null
        [Column("image_url9")]
        public string ImageURL9 { get; set; } // varchar(250), null
        [Column("image_url10")]
        public string ImageURL10 { get; set; } // varchar(250), null
        [Column("image_url11")]
        public string ImageURL11 { get; set; } // varchar(250), null
        [Column("image_url12")]
        public string ImageURL12 { get; set; } // varchar(250), null
        [Column("image_url13")]
        public string ImageURL13 { get; set; } // varchar(250), null
        [Column("image_url14")]
        public string ImageURL14 { get; set; } // varchar(250), null
        [Column("image_url15")]
        public string ImageURL15 { get; set; } // varchar(250), null
        [Column("image_url16")]
        public string ImageURL16 { get; set; } // varchar(250), null.
        [Column("image_url17")]
        public string ImageURL17 { get; set; } // varchar(250), null
        [Column("image_url18")]
        public string ImageURL18 { get; set; } // varchar(250), null
        [Column("image_url19")]
        public string ImageURL19 { get; set; } // varchar(250), null
        [Column("image_url20")]
        public string ImageURL20 { get; set; } // varchar(250), null
    }
}
