﻿using Dapper.Contrib.Extensions;
using System;
using System.ComponentModel.DataAnnotations;
using VMGSoftware.DataSDK.LDap.Attributes;

namespace VMGSoftware.DataSDK.DmsData.Stock.Entities
{
    [TableInfo("tblStockImageProcessing")]
    [Table("tblStockImageProcessing")]
    public class StockImageProcessing
    {
        [ColumnInfo(true)]
        [Dapper.Contrib.Extensions.Key]
        [Required]
        public int id { get; set; }
        [Required]
        public int stockId { get; set; }
        public DateTime LastUpdated { get; set; }
        [MaxLength(100)]
        [Required]
        public string ProcessingStatus { get; set; }
        public bool DeleteImages { get; set; }
        [MaxLength(255)]
        public string Image1Status { get; set; }
        [MaxLength(255)]
        public string Image2Status { get; set; }
        [MaxLength(255)]
        public string Image3Status { get; set; }
        [MaxLength(255)]
        public string Image4Status { get; set; }
        [MaxLength(255)]
        public string Image5Status { get; set; }
        [MaxLength(255)]
        public string Image6Status { get; set; }
        [MaxLength(255)]
        public string Image7Status { get; set; }
        [MaxLength(255)]
        public string Image8Status { get; set; }
        [MaxLength(255)]
        public string Image9Status { get; set; }
        [MaxLength(255)]
        public string Image10Status { get; set; }
        [MaxLength(255)]
        public string Image11Status { get; set; }
        [MaxLength(255)]
        public string Image12Status { get; set; }
        [MaxLength(255)]
        public string Image13Status { get; set; }
        [MaxLength(255)]
        public string Image14Status { get; set; }
        [MaxLength(255)]
        public string Image15Status { get; set; }
        [MaxLength(255)]
        public string Image16Status { get; set; }
        [MaxLength(255)]
        public string Image17Status { get; set; }
        [MaxLength(255)]
        public string Image18Status { get; set; }
        [MaxLength(255)]
        public string Image19Status { get; set; }
        [MaxLength(255)]
        public string Image20Status { get; set; }
    }
}
