﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using VMGSoftware.DataSDK.DmsData.User.Entities;

namespace VMGSoftware.DataSDK.DmsData.User.Repositories
{
    public static class SecurityRepository
    {
        public static Entities.Security Load(int userLevelId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                return sqlConnection.Get<Entities.Security>(userLevelId);
            }
        }

        public static Entities.Security LoadSecurityForEmployee(string emailAddress, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                const string sqlString = "SELECT se.* FROM dbo.tblSysUsers AS s, dbo.tblEmployees AS e, dbo.tblSecurity AS se WHERE s.EmployeeID = e.EmployeeID and e.Email = @EmailAddress AND se.UserLevelID = s.UserLevelID";

                var results = sqlConnection.Query<Security>(sqlString, new {EmailAddress = emailAddress}).ToList().First();

                return results;
            }
        }

        public static Dictionary<string, string> GetUsersEmailAddressesInSecurityLevel(int userLevelId, SqlConnection sqlConnection)
        {

            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
             
                const string sqlString = "SELECT e.Email, (e.FirstName + ' ' + e.Surname) As EmployeeName FROM dbo.tblEmployees AS e, tblSysUsers AS s, dbo.tblSecurity AS se WHERE (e.Email is not null and e.Email <> '') and s.EmployeeID = e.EmployeeID  AND se.UserLevelID = s.UserLevelID and s.UserLevelID = @UserLevelID";

                var results = sqlConnection
                    .Query<EmployeeNameAndEmailAddress>(sqlString, new {UserLevelID = userLevelId}).ToList();

                var d = new Dictionary<string, string>();

                if (!results.Any()) return d;

                results.ForEach(p => d.Add(p.Email, p.EmployeeName));

                return d;
            }
        }

        public static List<string> GetUserEmailAddressesThatAreRegisteredForMobile(SqlConnection sqlConnection)
        {

            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                const string sqlString = "  SELECT e.Email FROM dbo.tblEmployees AS e, tblSysUsers AS s WHERE s.EmployeeID = e.EmployeeID AND s.VmgMobileRegistered = 1 AND s.Deleted = 0 AND e.Deleted = 0 AND (e.Email IS NOT NULL AND e.Email <> '')";

                var results = sqlConnection
                    .Query<string>(sqlString).ToList();

                return results;
            }
        }

        public static string GetUserEmailAddress(int userId, SqlConnection sqlConnection)
        {

            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                const string sqlString = "SELECT e.Email FROM dbo.tblEmployees AS e, tblSysUsers AS s WHERE s.EmployeeID = e.EmployeeID AND s.SysUserID = @UserId";

                var results = sqlConnection
                    .Query<string>(sqlString, new {UserId = userId}).ToList();

                

                return results.FirstOrDefault();
            }
        }

    }

    internal struct EmployeeNameAndEmailAddress
    {
        public string Email { get; set; }
        public string EmployeeName { get; set; }
    }
}


