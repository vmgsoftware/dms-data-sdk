﻿using System.Data.SqlClient;
using System.Linq;
using Dapper;
using VMGAuto.Data;

namespace VMGSoftware.DataSDK.DmsData.Client.Repositories
{
    public class ClientRepository : GenericRepository
    {
        public static string GetMobileLeadId(int clientId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                return sqlConnection.ExecuteScalar<string>(
                    "SELECT MobileLeadId FROM dbo.tblClients WHERE ClientID = @ClientId", new {ClientId = clientId});
            }
        }

        public static void UpdateMobileLeadId(int clientId, string mobileLeadId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                sqlConnection.Execute("update dbo.tblClients set MobileLeadId = @MobileLeadId WHERE ClientID = @ClientId", new {MobileLeadId = mobileLeadId, ClientId = clientId});
            }
        }

        public static Entities.Client GetClientByClientId(int leadId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                var client = sqlConnection.Query<Entities.Client>("select * from dbo.tblClients where ClientId = @ClientId",
                    new {ClientId = leadId}).Single();

                return client;
            }
        }

        public static int GetClientIdByClientCode(string clientCode, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                var client = sqlConnection.Query<int>("select ClientId from dbo.tblClients where ClientCode = @ClientCode",
                    new {ClientCode = clientCode}).Single();

                return client;
            }
        }
    }
}
