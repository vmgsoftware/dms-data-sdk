﻿using System;
using System.Data.SqlClient;
using Dapper;
using VMGAuto.Data;
using VMGSoftware.DataSDK.DmsData.Error_Handling.Entities;

namespace VMGSoftware.DataSDK.DmsData.Error_Handling.Repositories
{
    public class ErrorMessageRepository : GenericRepository
    {
        public static ErrorMessage LoadByType(Exception exception, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                return sqlConnection.QueryFirstOrDefault<ErrorMessage>(
                    $"SELECT * FROM tblErrorMessages where errorType = '{exception.GetType()}'");
            }
        }
    }
}
