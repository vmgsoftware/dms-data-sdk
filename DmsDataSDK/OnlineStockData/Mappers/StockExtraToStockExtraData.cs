﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using VMGSoftware.DataSDK.DmsData.Stock.Entities;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Stock;

namespace VMGSoftware.DataSDK.OnlineStockData.Mappers
{
    public static class StockExtraToStockExtraData
    {
        public static StockExtraData Map(int stockId,StockData stockData,string connectionString)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                var localStockExtras = sqlConnection.Get<StockExtras>(stockId);
                if (localStockExtras == null)
                {
                    return null;
                }
                var onlineStockExtra = new StockExtraData
                {
                    body_type = localStockExtras.BodyType,
                    extra_list_string = localStockExtras.ExtraListString,
                    StockData = stockData
                };

                return onlineStockExtra;

            }
        }
    }
}
