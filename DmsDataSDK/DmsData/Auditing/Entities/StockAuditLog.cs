﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Auditing.Entities
{
    [Dapper.Contrib.Extensions.Table("tblStockAuditLog")]
    public class StockAuditLog
    {
        public StockAuditLog()
        {
            this.Timestamp = DateTime.Now;
        }

        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Action Id is required")]
        public int ActionId { get; set; } // int, not null
        [Required(ErrorMessage = "Stock Id is required")]
        public int StockId { get; set; } // int, not null
        public DateTime? Timestamp { get; set; } // datetime, null
        [Required(ErrorMessage = "Sys User Id is required")]
        public int SysUserId { get; set; } // int, not null
        [MaxLength(100)]
        [StringLength(100)]
        public string Workstation { get; set; } // varchar(100), null
        [Required(ErrorMessage = "Action Type Id is required")]
        public int ActionTypeId { get; set; } // int, not null
    }

}
