﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using VMGAuto.Data;
using VMGSoftware.DataSDK.DmsData.Accounting.Entities;

namespace VMGSoftware.DataSDK.DmsData.Accounting.Repositories
{
    public  class GlAccountRepository : GenericRepository
    {
        public static List<GlAccount> GetSundryAccounts(SqlConnection sqlConnection)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                sqlConnection.Open();
            }
            return sqlConnection.Query<GlAccount>(
                "select Code,code + \' : \' + description as Description,code as account,Locked,BranchId,Deleted from tblglaccounts where fincatcode IN (\'I20\',\'I10\') and show =1  order by code ").ToList();
        }
    }
}
