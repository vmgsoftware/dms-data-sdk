﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using VMGSoftware.DataSDK.OnlineStockData.Mappers;
using VMGSoftware.DataSDK.OnlineStockData.Migrations;
using VMGSoftware.DataSDK.OnlineStockData.Repositories;

namespace VMGSoftware.DataSdk.Tests.OnlineStockTests
{
    
    public class UploadStock
    {
        const string conString =
            "Data Source=MYSTIQUE\\LMS,54082;Initial Catalog=amgauto;Persist Security Info=True;User ID=amguser;Password=autoAUTO2275!";

        [Test]
        public void UploadStockItem()
        {
            using (StockFeedDataContext dbContext = new StockFeedDataContext(conString))
            {
                StockDataRepo.UpsertStockData(1321,conString,dbContext);

                //var stockData = StockToStockData.Map(1320, conString, dbContext);
                //dbContext.StockData.Add(stockData);
                //dbContext.SaveChanges();
            }
        }

        [Test]
        public void UploadAllStock()
        {
            using (StockFeedDataContext dbContext = new StockFeedDataContext(conString))
            {
                StockDataRepo.UploadAllActiveStock(conString,dbContext);
            }
        }

    }
}
