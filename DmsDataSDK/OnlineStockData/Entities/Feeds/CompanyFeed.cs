﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.OnlineStockData.Entities.Feeds
{
    [System.ComponentModel.DataAnnotations.Schema.Table("company_feeds")]
    public class CompanyFeed
    {
        [Key]
        public int id { get; set; }

        public int company_id { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime date_of_change { get; set; }
        public bool enabled { get; set; }
        public string changed_by_user { get; set; }
        public string changed_by_computer { get; set; }
        public Feed feed { get; set; }
    }
}
