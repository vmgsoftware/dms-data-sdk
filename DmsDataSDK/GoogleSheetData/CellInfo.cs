﻿namespace VMGSoftware.DataSDK.GoogleSheetData
{
    public class CellInfo : System.Attribute
    {
        public char ColumnLetter { get; }
        public int RowNumber { get; set; }
        public bool IsIdColumn { get; set; }

        public CellInfo(char columnLetter, int rowNumber,bool isIdColumn = false)
        {
            ColumnLetter = columnLetter;
            RowNumber = rowNumber;
            IsIdColumn = isIdColumn;
        }

        public CellInfo(char columnLetter,bool isIdColumn = false)
        {
            ColumnLetter = columnLetter;
            IsIdColumn = isIdColumn;
        }
    }
}
