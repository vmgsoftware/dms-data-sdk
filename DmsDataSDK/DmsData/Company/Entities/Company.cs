﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Company.Entities
{
    [Dapper.Contrib.Extensions.Table("tblCompanies")]
    public class Company
    {
        public Company()
        {
            //this.Deleted = false;
            //this.DateCreated = DateTime.Now;
            //this.UploadToWeb = false;
            //this.accounting = false;
            //this.FontSizeTax = 9;
            //this.FontSizeOTP = 9;
            //this.CanAllocateInv = false;
            //this.CanTransferCosts = false;
            //this.JournalDelay = false;
            //this.Dealer2Dealer = false;
            //this.TaxSignature = false;
            //this.OTPSignature = false;
            //this.FloorplanTotal = 0M;
            //this.FontSizeSundry = 9;
            //this.SundrySignature = false;
            //this.RMITax = false;
            //this.RMIOTP = false;
            //this.LockOut = false;
            //this.ReqSalesSource = false;
            //this.MIOTAX = false;
            //this.MIOOTP = false;
            //this.SeritiInUse = false;
            //this.SignioInUse = false;
            //this.PromptInvoiceAppendix = false;
            //this.enablecarscoza = false;
            //this.enablecars4sa = false;
            //this.enablechase = false;
            //this.enableemanners = false;
            //this.enablesurf4cars = false;
            //this.PromptIDADisclosure = false;
            //this.enableixonline = false;
            //this.enablecarfind = false;
            //this.enableautodealer = false;
            //this.enableolx = false;
            //this.enableautomart = false;
            //this.enablebluechip = false;
            //this.AdditionalInvoiceExtras = false;
            //this.WebAccessTest = false;
            //this.BetaVersion = false;
            //this.CRMFeedServer = false;
            //this.BulkTransfer = false;
            //this.NewWebTransfer = false;
            //this.enabletransunion = false;
            //this.d2dnewsletter = false;
            //this.suppIncomeOutstBal = false;
            //this.enablegumtree = false;
            //this.SoldNoWebRemoval = false;
            //this.NewExtrasMode = false;
            //this.enableAllAuto = false;
            //this.enableHippoCoZa = false;
            //this.enablecapitec = false;
            //this.enableredfeed = false;
            //this.BankAccountBranchCode = "";
            //this.BankAccountBankName = "";
            //this.BankAccountBranchName = "";
            //this.BankAccountType = "";
            //this.BankAccountNumber = "";
            //this.postgresqlweb = false;
            //this.websiteurl = "";
            //this.contactusurl = "";
            //this.bluechip_db_address = "";
            //this.bluechip_db_port = 0;
            //this.bluechip_db_name = "";
            //this.bluechip_db_username = "";
            //this.bluechip_db_password = "";
            //this.bluechip_db_server_type = false;
            //this.carplace_db_address = "";
            //this.carplace_db_port = 0;
            //this.carplace_db_name = "";
            //this.carplace_db_username = "";
            //this.carplace_db_password = "";
            //this.carplace_db_server_type = false;
            //this.WebDbServerType = false;
            //this.d2d_db_address = "";
            //this.d2d_db_port = 0;
            //this.d2d_db_name = "";
            //this.d2d_db_username = "";
            //this.d2d_db_password = "";
            //this.d2d_db_server_type = false;
            //this.finance_url = "";
            //this.websiteEmailAddresses = "";
            //this.websiteTelephoneNumber = "";
            //this.defaultRoundingAccount = "3800/999";
            //this.displayBankingDetails = true;
            //this.enableCarborator = false;
            //this.WebSales = false;
            //this.fontWE = 9;
            //this.fontWI = 9;
            //this.fontCQ = 9;
            //this.fontCI = 9;
            //this.enablePriceCheck = false;
            //this.stockProfitsDefaultWithVat = false;
            //this.CompCity = "";
            //this.PostCity = "";
        }

        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Company ID is required")]
        public int CompanyID { get; set; } // int, not null
        [MaxLength(50)]
        public string CompanyName { get; set; } // nvarchar(50), null
        [MaxLength(50)]
        public string TradingName { get; set; } // nvarchar(50), null
        [MaxLength(20)]
        public string CompRegNo { get; set; } // nvarchar(20), null
        [MaxLength(20)]
        public string VatRegNo { get; set; } // nvarchar(20), null
        [MaxLength(50)]
        public string MainContactPerson { get; set; } // nvarchar(50), null
        [MaxLength(6)]
        public string WorkTelCode { get; set; } // nvarchar(6), null
        [MaxLength(10)]
        public string WorkTelNo { get; set; } // nvarchar(10), null
        [MaxLength(13)]
        public string Cell { get; set; } // nvarchar(13), null
        [MaxLength(30)]
        public string CompSuburb { get; set; } // nvarchar(30), null
        [MaxLength(30)]
        public string PostSuburb { get; set; } // nvarchar(30), null
        [MaxLength(3)]
        public string StockCodePrefix { get; set; } // nvarchar(3), null
        public int? LastCodeValue { get; set; } // int, null
        [MaxLength(3)]
        public string StockCodePostfix { get; set; } // nvarchar(3), null
        public byte[] Logo { get; set; } // image, null
        [MaxLength(100)]
        public string Email { get; set; } // varchar(100), null
        public decimal? DealerStock { get; set; } // money, null
        public decimal? Valet { get; set; } // money, null
        public decimal? Roadworthy { get; set; } // money, null
        public decimal? NumberPlates { get; set; } // money, null
        public decimal? License { get; set; } // money, null
        public decimal? Registration { get; set; } // money, null
        public decimal? Petrol { get; set; } // money, null
        public decimal? AfterSales { get; set; } // money, null
        public decimal? Telephone { get; set; } // money, null
        public decimal? Workshop { get; set; } // money, null
        [MaxLength(10)]
        public string SoftwareVersion { get; set; } // varchar(10), null
        public int? LastUpdateID { get; set; } // int, null
        public bool? Deleted { get; set; } // bit, null
        public DateTime? DateCreated { get; set; } // datetime, null
        public DateTime? DateUpdated { get; set; } // datetime, null
        public int? LastUserID { get; set; } // int, null
        [MaxLength(10)]
        public string SPostCode { get; set; } // nvarchar(10), null
        [MaxLength(50)]
        public string PPostCode { get; set; } // nvarchar(50), null
        [MaxLength(6)]
        public string WorkFaxCode { get; set; } // nvarchar(6), null
        [MaxLength(10)]
        public string WorkFaxNo { get; set; } // nvarchar(10), null
        public int? StockSupplierTypeID { get; set; } // int, null
        public int? CostsSupplierTypeID { get; set; } // int, null
        public int? StockCodeGenerationType { get; set; } // int, null
        public int? LastDBInstruction { get; set; } // int, null
        [MaxLength(5)]
        public string DatabaseVersion { get; set; } // varchar(5), null
        public DateTime? finstartdate { get; set; } // datetime, null
        public DateTime? finenddate { get; set; } // datetime, null
        [MaxLength(3)]
        public string SundryInvoicepostfix { get; set; } // nvarchar(3), null
        [MaxLength(3)]
        public string SundryInvoicePrefix { get; set; } // nvarchar(3), null
        public int? LastSundryValue { get; set; } // int, null
        public int? SundryGenerationType { get; set; } // int, null
        [MaxLength(30)]
        public string WebDB { get; set; } // varchar(30), null
        [MaxLength(6)]
        public string WebPort { get; set; } // varchar(6), null
        [MaxLength(3)]
        public string InvoicePrefix { get; set; } // nvarchar(3), null
        [MaxLength(3)]
        public string InvoicePostfix { get; set; } // nvarchar(3), null
        public long? LastInvoiceNumber { get; set; } // bigint, null
        public bool? UploadToWeb { get; set; } // bit, null
        [MaxLength(20)]
        public string FSPNo { get; set; } // nvarchar(20), null
        public decimal? InvoiceExtraAmt1 { get; set; } // money, null
        public decimal? InvoiceExtraAmt2 { get; set; } // money, null
        public decimal? InvoiceExtraAmt3 { get; set; } // money, null
        public decimal? InvoiceExtraAmt4 { get; set; } // money, null
        public decimal? InvoiceExtraAmt5 { get; set; } // money, null
        public decimal? InvoiceExtraAmt6 { get; set; } // money, null
        public decimal? LicRegAmt { get; set; } // money, null
        public decimal? Fuelingfee { get; set; } // money, null
        [MaxLength(40)]
        public string WebDbName { get; set; } // varchar(40), null
        [MaxLength(40)]
        public string webdbusername { get; set; } // varchar(40), null
        [MaxLength(40)]
        public string webdbpassword { get; set; } // varchar(40), null
        [MaxLength(50)]
        public string compaddr1 { get; set; } // nvarchar(50), null
        [MaxLength(50)]
        public string compaddr2 { get; set; } // nvarchar(50), null
        [MaxLength(50)]
        public string postaddr1 { get; set; } // nvarchar(50), null
        [MaxLength(50)]
        public string postaddr2 { get; set; } // nvarchar(50), null
        public bool? accounting { get; set; } // bit, null
        public int? PurchaseOrders { get; set; } // int, null
        public byte[] LogoWide { get; set; } // image, null
        [MaxLength(1400)]
        public string TermsInvoice { get; set; } // varchar(1400), null
        [MaxLength(20)]
        public string TrafficRegNo { get; set; } // nvarchar(20), null
        public short? CompanyTypeID { get; set; } // smallint, null
        [MaxLength(1400)]
        public string TermsInvoiceOTP { get; set; } // varchar(1400), null
        public short? FontSizeTax { get; set; } // smallint, null
        public short? FontSizeOTP { get; set; } // smallint, null
        [MaxLength(30)]
        public string InvoiceExtraNoVATDesc1 { get; set; } // varchar(30), null
        [MaxLength(30)]
        public string InvoiceExtraNoVATDesc2 { get; set; } // varchar(30), null
        public int? InvoiceExtraTypeID1 { get; set; } // int, null
        public int? InvoiceExtraTypeID2 { get; set; } // int, null
        public int? InvoiceExtraTypeID3 { get; set; } // int, null
        public int? InvoiceExtraTypeID4 { get; set; } // int, null
        public int? InvoiceExtraTypeID5 { get; set; } // int, null
        public int? InvoiceExtraTypeID6 { get; set; } // int, null
        public bool? CanAllocateInv { get; set; } // bit, null
        public bool? CanTransferCosts { get; set; } // bit, null
        public int? BankID { get; set; } // int, null
        [MaxLength(10)]
        public string BankBranchNo { get; set; } // nvarchar(10), null
        [MaxLength(30)]
        public string BankBranchName { get; set; } // nvarchar(30), null
        public int? BankAccountTypeID { get; set; } // int, null
        [MaxLength(16)]
        public string BankAccountNo { get; set; } // nvarchar(16), null
        [MaxLength(1000)]
        public string TermsQuote { get; set; } // varchar(1000), null
        [MaxLength(3)]
        public string WSInvPrefix { get; set; } // nvarchar(3), null
        public int? WSInvStart { get; set; } // int, null
        [MaxLength(3)]
        public string WSCrNotePrefix { get; set; } // nvarchar(3), null
        public int? WSCrStart { get; set; } // int, null
        [MaxLength(100)]
        public string DatabaseUpdLocation { get; set; } // nvarchar(100), null
        public int? StockSort { get; set; } // int, null
        public bool? JournalDelay { get; set; } // bit, null
        public bool? Dealer2Dealer { get; set; } // bit, null
        [MaxLength(8)]
        public string defaultCostTypeAccount { get; set; } // varchar(8), null
        public int? ProvinceID { get; set; } // int, null
        public bool? TaxSignature { get; set; } // bit, null
        public bool? OTPSignature { get; set; } // bit, null
        public decimal? FloorplanTotal { get; set; } // money, null
        [MaxLength(1400)]
        public string TermsSundry { get; set; } // varchar(1400), null
        public short? FontSizeSundry { get; set; } // smallint, null
        public bool? SundrySignature { get; set; } // bit, null
        public bool? RMITax { get; set; } // bit, null
        public bool? RMIOTP { get; set; } // bit, null
        public bool? LockOut { get; set; } // bit, null
        [MaxLength(10)]
        public string InvoiceSMS { get; set; } // varchar(10), null
        [MaxLength(20)]
        public string TUCode { get; set; } // varchar(20), null
        [MaxLength(30)]
        public string SeritiCompanyCode { get; set; } // varchar(30), null
        [MaxLength(30)]
        public string SeritiCompanyPassword { get; set; } // varchar(30), null
        [MaxLength(30)]
        public string SeritiBranchCode { get; set; } // varchar(30), null
        [MaxLength(50)]
        public string SeritiFIUsername { get; set; } // varchar(50), null
        [MaxLength(200)]
        public string SeritiFIEmail { get; set; } // varchar(200), null
        public bool? ReqSalesSource { get; set; } // bit, null
        [MaxLength(5)]
        public string CurrencySymbol { get; set; } // varchar(5), null
        public bool? ReqClientDetails { get; set; } // bit, null
        public bool? PromptInvoiceTC { get; set; } // bit, null
        public bool? PromptOTPTC { get; set; } // bit, null
        public bool? PromptInvoiceCPA { get; set; } // bit, null
        public bool? PromptOTPCPA { get; set; } // bit, null
        public bool? MIOTAX { get; set; } // bit, null
        public bool? MIOOTP { get; set; } // bit, null
        [MaxLength(1400)]
        public string termsinvoiceprivatedealer { get; set; } // varchar(1400), null
        public int? SellersPurchase { get; set; } // int, null
        [MaxLength(50)]
        public string ContactName1 { get; set; } // varchar(50), null
        [MaxLength(50)]
        public string ContactSurname1 { get; set; } // varchar(50), null
        [MaxLength(50)]
        public string ContactName2 { get; set; } // varchar(50), null
        [MaxLength(50)]
        public string ContactSurname2 { get; set; } // varchar(50), null
        [MaxLength(50)]
        public string ContactCell2 { get; set; } // varchar(50), null
        [MaxLength(50)]
        public string SignioDealerCode { get; set; } // varchar(50), null
        [MaxLength(50)]
        public string SignioUsername { get; set; } // varchar(50), null
        [MaxLength(50)]
        public string SignioPassword { get; set; } // varchar(50), null
        public bool? SeritiInUse { get; set; } // bit, null
        public bool? SignioInUse { get; set; } // bit, null
        public bool? PromptInvoiceAppendix { get; set; } // bit, null
        public bool? enablecarscoza { get; set; } // bit, null
        public bool? enablecars4sa { get; set; } // bit, null
        public bool? enablechase { get; set; } // bit, null
        public bool? enableemanners { get; set; } // bit, null
        public bool? enablesurf4cars { get; set; } // bit, null
        public bool? PromptIDADisclosure { get; set; } // bit, null
        public bool? enableixonline { get; set; } // bit, null
        public bool? enablecarfind { get; set; } // bit, null
        public bool? enableautodealer { get; set; } // bit, null
        public bool? enableolx { get; set; } // bit, null
        public bool? enableautomart { get; set; } // bit, null
        public bool? enablebluechip { get; set; } // bit, null
        public decimal? InvoiceExtraAmt7 { get; set; } // money, null
        public decimal? InvoiceExtraAmt8 { get; set; } // money, null
        public decimal? InvoiceExtraAmt9 { get; set; } // money, null
        public decimal? InvoiceExtraAmt10 { get; set; } // money, null
        public int? InvoiceExtraTypeID7 { get; set; } // int, null
        public int? InvoiceExtraTypeID8 { get; set; } // int, null
        public int? InvoiceExtraTypeID9 { get; set; } // int, null
        public int? InvoiceExtraTypeID10 { get; set; } // int, null
        public bool? AdditionalInvoiceExtras { get; set; } // bit, null
        public bool? WebAccessTest { get; set; } // bit, null
        public bool? BetaVersion { get; set; } // bit, null
        public bool? CRMFeedServer { get; set; } // bit, null
        public bool? BulkTransfer { get; set; } // bit, null
        [MaxLength(1400)]
        public string termsinvoiceprivate { get; set; } // varchar(1400), null
        [MaxLength(9)]
        public string AdvertColour { get; set; } // varchar(9), null
        public int? LastPurchaseInvNr { get; set; } // int, null
        public bool? NewWebTransfer { get; set; } // bit, null
        [MaxLength(50)]
        public string BranchName { get; set; } // nvarchar(50), null
        public bool? enabletransunion { get; set; } // bit, null
        [MaxLength(50)]
        public string TransUnionUsername { get; set; } // varchar(50), null
        [MaxLength(50)]
        public string TransUnionPassword { get; set; } // varchar(50), null
        public bool? d2dnewsletter { get; set; } // bit, null
        public bool? suppIncomeOutstBal { get; set; } // bit, null
        public bool? enablegumtree { get; set; } // bit, null
        public bool? SoldNoWebRemoval { get; set; } // bit, null
        public bool? NewExtrasMode { get; set; } // bit, null
        public bool? enableAllAuto { get; set; } // bit, null
        [MaxLength(10)]
        public string InvoiceSMSSecondary { get; set; } // varchar(10), null
        public bool? enableHippoCoZa { get; set; } // bit, null
        public bool? enablecapitec { get; set; } // bit, null
        public bool? enableredfeed { get; set; } // bit, null
        [MaxLength(50)]
        public string BankAccountBranchCode { get; set; } // varchar(50), null
        [MaxLength(50)]
        public string BankAccountBankName { get; set; } // varchar(50), null
        [MaxLength(50)]
        public string BankAccountBranchName { get; set; } // varchar(50), null
        [MaxLength(50)]
        public string BankAccountType { get; set; } // varchar(50), null
        [MaxLength(50)]
        public string BankAccountNumber { get; set; } // varchar(50), null
        public bool? postgresqlweb { get; set; } // bit, null
        [MaxLength(300)]
        public string websiteurl { get; set; } // varchar(300), null
        [MaxLength(300)]
        public string contactusurl { get; set; } // varchar(300), null
        [MaxLength(200)]
        public string bluechip_db_address { get; set; } // varchar(200), null
        public int? bluechip_db_port { get; set; } // int, null
        [MaxLength(50)]
        public string bluechip_db_name { get; set; } // varchar(50), null
        [MaxLength(200)]
        public string bluechip_db_username { get; set; } // varchar(200), null
        [MaxLength(200)]
        public string bluechip_db_password { get; set; } // varchar(200), null
        public bool? bluechip_db_server_type { get; set; } // bit, null
        [MaxLength(200)]
        public string carplace_db_address { get; set; } // varchar(200), null
        public int? carplace_db_port { get; set; } // int, null
        [MaxLength(50)]
        public string carplace_db_name { get; set; } // varchar(50), null
        [MaxLength(200)]
        public string carplace_db_username { get; set; } // varchar(200), null
        [MaxLength(200)]
        public string carplace_db_password { get; set; } // varchar(200), null
        public bool? carplace_db_server_type { get; set; } // bit, null
        public bool? WebDbServerType { get; set; } // bit, null
        [MaxLength(200)]
        public string d2d_db_address { get; set; } // varchar(200), null
        public int? d2d_db_port { get; set; } // int, null
        [MaxLength(50)]
        public string d2d_db_name { get; set; } // varchar(50), null
        [MaxLength(200)]
        public string d2d_db_username { get; set; } // varchar(200), null
        [MaxLength(200)]
        public string d2d_db_password { get; set; } // varchar(200), null
        public bool? d2d_db_server_type { get; set; } // bit, null
        [MaxLength(300)]
        public string finance_url { get; set; } // varchar(300), null
        [MaxLength(300)]
        public string websiteEmailAddresses { get; set; } // varchar(300), null
        [MaxLength(12)]
        public string websiteTelephoneNumber { get; set; } // varchar(12), null
        [MaxLength(8)]
        public string defaultRoundingAccount { get; set; } // varchar(8), null
        public bool? displayBankingDetails { get; set; } // bit, null
        public bool? enableCarborator { get; set; } // bit, null
        public bool? WebSales { get; set; } // bit, null
        [MaxLength(1000)]
        public string TermsWSEst { get; set; } // nvarchar(1000), null
        [MaxLength(1000)]
        public string TermsWSInv { get; set; } // nvarchar(1000), null
        [MaxLength(1000)]
        public string TermsCSInv { get; set; } // nvarchar(1000), null
        public int? fontWE { get; set; } // int, null
        public int? fontWI { get; set; } // int, null
        public int? fontCQ { get; set; } // int, null
        public int? fontCI { get; set; } // int, null
        [MaxLength(50)]
        public string AccHolder { get; set; } // nvarchar(50), null
        public bool? enablePriceCheck { get; set; } // bit, null
        [Required(ErrorMessage = "stock Profits Default With Vat is required")]
        public bool stockProfitsDefaultWithVat { get; set; } // bit, not null
        [MaxLength(25)]
        public string CompCity { get; set; } // nvarchar(25), null
        [MaxLength(25)]
        public string PostCity { get; set; } // nvarchar(25), null
    }

}
