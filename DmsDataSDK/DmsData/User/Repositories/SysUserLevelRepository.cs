﻿using System.Data.SqlClient;
using Dapper.Contrib.Extensions;

namespace VMGSoftware.DataSDK.DmsData.User.Repositories
{
    public static class SysUserLevelRepository
    {
        public static Entities.SysUserLevel Load(int sysUserLevelID, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                return sqlConnection.Get<Entities.SysUserLevel>(sysUserLevelID);
            }
        }
    }
}
