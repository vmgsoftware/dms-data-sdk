﻿using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.ServiceQueue.Entities
{
    [Dapper.Contrib.Extensions.Table("tblExternalDb")]
    public class ExternalDb
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; } // int, not null
        public string Name { get; set; } // varchar(255), not null
        public string ConnectionString { get; set; } // varchar(1000), not null
        public string Type { get; set; } // varchar(255), not null
    }

}
