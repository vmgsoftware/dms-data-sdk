﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using VMGAuto.Data;

namespace VMGSoftware.DataSDK.DmsData.Doc_Storage.Repositories
{
    public class StorageIndexRepository : GenericRepository
    {
 
        public static List<Entities.StorageIndex> LoadAllForASpecificStockItem(int stockId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                return sqlConnection.Query<Entities.StorageIndex>($"SELECT * FROM tblDocStorageIndexes WHERE StockId = {stockId}").ToList();
            }
        }

        public static Entities.StorageIndex GetExistingIndexElseCreate(int stockId, int subCategoryId, bool IsFreeformCategory, string filename, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {

                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                Entities.StorageIndex storageIndex = null;

                if (IsFreeformCategory)
                {
                    storageIndex = sqlConnection.QueryFirstOrDefault<Entities.StorageIndex>($"SELECT * FROM [tblDocStorageIndexes] WHERE StockId = {stockId} AND tblDocStorageIndexes.SubcategoryId = {subCategoryId} AND FileName = '{filename}'");

                }
                else
                {
                    storageIndex = sqlConnection.QueryFirstOrDefault<Entities.StorageIndex>($"SELECT * FROM [tblDocStorageIndexes] WHERE StockId = {stockId} AND tblDocStorageIndexes.SubcategoryId = {subCategoryId}");
                }


                if (storageIndex == null)
                {
                    storageIndex = new Entities.StorageIndex();
                }
                return storageIndex;
            }

        }
    }
}
