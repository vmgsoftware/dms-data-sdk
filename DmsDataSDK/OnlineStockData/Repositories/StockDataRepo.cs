﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using VMGSoftware.DataSDK.DmsData.Company.Repositories;
using VMGSoftware.DataSDK.DmsData.Stock.Repositories;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Stock;
using VMGSoftware.DataSDK.OnlineStockData.Mappers;
using VMGSoftware.DataSDK.OnlineStockData.Migrations;
using VMGSoftware.DataSDK.Temp_Logging;

namespace VMGSoftware.DataSDK.OnlineStockData.Repositories
{
    public static class StockDataRepo
    {

        public static void UploadAllActiveStock(string localConnectionString, StockFeedDataContext dbContext)
        {
            try
            {
                //Get Company Id
                var companyId = StandardVariablesRepository.GetCompanyId(localConnectionString);

                var activeStockIds = StockRepository.GetAllActiveStockIds(localConnectionString);
                foreach (var activeStockId in activeStockIds)
                {
                    //Remove existing data
                    RemoveStockData(activeStockId, localConnectionString, companyId, dbContext);

                    var stockData = StockToStockData.Map(activeStockId, localConnectionString, dbContext);
                    dbContext.StockData.Add(stockData);
                    dbContext.SaveChanges();
                    using (var sqlConnection = new SqlConnection(localConnectionString))
                    {
                        sqlConnection.Open();

                        sqlConnection.Execute(
                            $"UPDATE tblstock SET IsStockDataOnline = 1 where stockId = {activeStockId}");
                    }

                }
            }
            catch (Exception e)
            {
                Logger.LogExceptionToGraylog(e);
                throw;
            }
        }

        public static void UpsertStockData(int stockId, string localConnectionString, StockFeedDataContext dbContext)
        {
            try
            {
                //Get Company Id
                var companyId = StandardVariablesRepository.GetCompanyId(localConnectionString);
                if (companyId == 0)
                    throw new Exception("Company ID can not be null or 0");

                //Remove existing data
                RemoveStockData(stockId, localConnectionString, companyId, dbContext);

                //Get updated stockData entity
                var stockData = StockToStockData.Map(stockId, localConnectionString, dbContext);
                dbContext.StockData.Add(stockData);

                dbContext.SaveChanges();

                using (var sqlConnection = new SqlConnection(localConnectionString))
                {
                    sqlConnection.Open();

                    sqlConnection.Execute($"UPDATE tblstock SET IsStockDataOnline = 1 where stockId = {stockId}");
                }
            }
            catch (Exception e)
            {
                Logger.LogExceptionToGraylog(e);
                throw;
            }
           
        }

        public static void RemoveAllDeletedStock(string localConnectionString, StockFeedDataContext dbContext)
        {
            var companyId = StandardVariablesRepository.GetCompanyId(localConnectionString);
            foreach (var stockId in StockRepository.GetStockIdsToRemoveFromOnline(localConnectionString))
            {
                RemoveStockData(stockId,localConnectionString,companyId,dbContext);
            }
        }

        public static void RemoveStockData(int stockId, string localConnectionString,int companyId, StockFeedDataContext dbContext)
        {
            try
            {
                if (companyId == 0)
                    throw new Exception("Company ID can not be null or 0");

                var stockDataToRemove = dbContext.StockData.Where(s => s.stock_id == stockId && s.company_id == companyId);
                if (!stockDataToRemove.Any())
                    return;

                var stockDataIds = stockDataToRemove.Select(s => s.stock_data_id);

                dbContext.StockImageUrls.RemoveRange(dbContext.StockImageUrls.Where(s => stockDataIds.Contains(s.stock_data_id)));
                dbContext.StockExtras.RemoveRange(dbContext.StockExtras.Where(s => stockDataIds.Contains(s.stock_data_id)));
                dbContext.StockFeeds.RemoveRange(dbContext.StockFeeds.Where(s => stockDataIds.Contains(s.stock_data_id)));
                dbContext.StockData.RemoveRange(stockDataToRemove);
                dbContext.SaveChanges();
                using (var sqlConnection = new SqlConnection(localConnectionString))
                {
                    sqlConnection.Open();
                    sqlConnection.Execute($"UPDATE tblstock SET IsStockDataOnline = 0 where stockId = {stockId}");
                }

                
            }
            catch (Exception e)
            {
                Logger.LogExceptionToGraylog(e);
                throw;
            }

        }
    }
}