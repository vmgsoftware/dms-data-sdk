﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.ServiceQueue.Entities
{
    [Dapper.Contrib.Extensions.Table("tblServiceQueue")]
    public class QueuedCommand
    {
        public QueuedCommand()
        {
            this.DateQueued = DateTime.Now;
            this.IsPriority = false;
            this.Status = "QUEUED";
        }

        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; } // int, not null
        public string Command { get; set; } // varchar(255), not null
        public DateTime DateQueued { get; set; } // datetime, not null
        public bool IsPriority { get; set; } // bit, not null
        public string Status { get; set; } // varchar(3000), not null
        public string Parameters { get; set; } // varchar(4000), null
        public DateTime? DateProcessed { get; set; } 
    }
}
