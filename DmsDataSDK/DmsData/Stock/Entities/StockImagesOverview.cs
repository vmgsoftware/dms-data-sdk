﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMGSoftware.DataSDK.LDap.Attributes;

namespace VMGSoftware.DataSDK.DmsData.Stock.Entities
{
    public class StockImagesOverview
    {
        [ColumnInfo(true)]
        [Dapper.Contrib.Extensions.Key]
        public int StockID { get; set; } // int, not null

        public bool MainImage { get; set; } // bool, null
        public bool Image1 { get; set; } // bool, null
        public bool Image2 { get; set; } // bool, null
        public bool Image3 { get; set; } // bool, null
        public bool Image4 { get; set; } // bool, null
        public bool Image5 { get; set; } // bool, null
        public bool Image6 { get; set; } // bool, null
        public bool Image7 { get; set; } // bool, null
        public bool Image8 { get; set; } // bool, null
        public bool Image9 { get; set; } // bool, null
        public bool Image10 { get; set; } // bool, null
        public bool Image11 { get; set; } // bool, null
        public bool Image12 { get; set; } // bool, null
        public bool Image13 { get; set; } // bool, null
        public bool Image14 { get; set; } // bool, null
        public bool Image15 { get; set; } // bool, null
        public bool Image16 { get; set; } // bool, null
        public bool Image17 { get; set; } // bool, null
        public bool Image18 { get; set; } // bool, null
        public bool Image19 { get; set; } // bool, null
    }
}
