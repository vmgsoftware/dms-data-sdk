﻿using System.ComponentModel.DataAnnotations;

namespace VMGSoftware.DataSDK.DmsData.Company.Entities
{
    [Dapper.Contrib.Extensions.Table("tblCompanyRegistrations")]
    public class CompanyReg
    {
        
        public CompanyReg()
        {
            //this.Deleted = false;
            //this.CompanyInUse = false;
            //this.DateStamp = DateTime.Now;
            //this.Demo = false;
            //this.DemoDays = 0;
        }

        [Dapper.Contrib.Extensions.Key]
        [Required(ErrorMessage = "Company ID is required")]
        public int CompanyID { get; set; } // int, not null
        [MaxLength(50)]
        [StringLength(50)]
        public string RegCode { get; set; } // varchar(50), null
        public bool? Deleted { get; set; } // bit, null
        public bool? CompanyInUse { get; set; } // bit, null
        //public DateTime? SynchDate { get; set; } // datetime, null
        //public DateTime? ExpiryDate { get; set; } // datetime, null
        //public DateTime? DateStamp { get; set; } // datetime, null
        //public bool? Demo { get; set; } // bit, null
        //public int? DemoDays { get; set; } // int, null

    }
}
