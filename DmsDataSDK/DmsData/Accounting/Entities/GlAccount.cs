﻿using System.ComponentModel.DataAnnotations;

namespace VMGSoftware.DataSDK.DmsData.Accounting.Entities
{
    [Dapper.Contrib.Extensions.Table("tblglaccounts")]
    public class GlAccount
    {
        public GlAccount()
        {
            this.Show = false;
            this.Locked = false;
            this.Deleted = false;
            this.BranchId = null;
        }

        [Dapper.Contrib.Extensions.Key]
        [MaxLength(8)]
        [StringLength(8)]
        [Required(ErrorMessage = "Code is required")]
        public string Code { get; set; } // varchar(8), not null
        [MaxLength(3)]
        [StringLength(3)]
        public string FinCatCode { get; set; } // varchar(3), null
        [MaxLength(300)]
        [StringLength(300)]
        public string Description { get; set; } // varchar(300), null
        public bool? Show { get; set; } // bit, null
        public bool? Locked { get; set; } // bit, null
        public decimal? StatementBalance { get; set; } // money, null
        public bool? Deleted { get; set; } // bit, null
        public int? BranchId { get; set; } // int, null
    }
}
