﻿using System;
using VMGSoftware.DataSDK.LDap.Attributes;
using Dapper.Contrib.Extensions;

namespace VMGSoftware.DataSDK.LDap.Logic
{
    public static class AttributeHelper
    {

        public static string GetTableName(object entity)
        {
            var classType = entity.GetType();
            var attributeType = typeof(TableInfo);
            if (classType.IsDefined(attributeType,false))
            {
                return ((TableInfo)classType.GetCustomAttributes(attributeType, false)[0]).TableName;
            }
            throw new Exception("Entity does not have the TableInfo attribute");
        }

        public static string GetTableNameDapper(Type entityType)
        {
            var attributeType = typeof(Dapper.Contrib.Extensions.TableAttribute);
            if (entityType.IsDefined(attributeType,false))
            {
                return ((TableAttribute)entityType.GetCustomAttributes(attributeType, false)[0]).Name;
            }
            throw new Exception("Entity does not have the TableInfo attribute");
        }

        public static string GetIdColumnName(object entity)
        {
            var attributeType = typeof(ColumnInfo);
            foreach (var propertyInfo in entity.GetType().GetProperties())
            {
                if (propertyInfo.IsDefined(attributeType,false))
                {
                    if (((ColumnInfo)propertyInfo.GetCustomAttributes(attributeType, false)[0]).IsIdColumn)
                    {
                        return propertyInfo.Name;
                    }
                }
            }
            throw new Exception("Entity does not have a member with the ColumnInfo attribute and IsIdColumn set to true");
        }
    }
}
