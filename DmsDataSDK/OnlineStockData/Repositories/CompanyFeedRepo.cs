﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Feeds;
using VMGSoftware.DataSDK.OnlineStockData.Mappers;
using VMGSoftware.DataSDK.OnlineStockData.Migrations;

namespace VMGSoftware.DataSDK.OnlineStockData.Repositories
{
    public static class CompanyFeedRepo
    {
        public static void UploadActivatedFeeds(int companyId,string localConnectionString,StockFeedDataContext dbContext)
        {
            var activeFeeds = CompanyFeedsMapper.Map(companyId, localConnectionString, dbContext);
            dbContext.CompanyFeeds.AddRange(activeFeeds);
            dbContext.SaveChanges();
        }

        public static void UploadFeedChange(string feedName,string computerName,string username,int companyId,bool activation,StockFeedDataContext dbContext)
        {
            CompanyFeed feedChange = new CompanyFeed();
            feedChange.feed = dbContext.Feeds.FirstOrDefault(x => x.feed_name == feedName);
            
            if (feedChange.feed == null) throw new Exception($"Feed {feedName} not found in online db");
            
            feedChange.company_id = companyId;
            feedChange.changed_by_computer = computerName;
            feedChange.changed_by_user = username;
            feedChange.enabled = activation;
            dbContext.CompanyFeeds.Add(feedChange);
            dbContext.SaveChanges();
        }


    }
}
