﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using VMGSoftware.DataSDK;

namespace VMGAuto.Data.SupportRepositories
{
    public class SupportRepository : GenericRepository
    {
        #region Colours
        public static string GetColourNameFromId(int colourId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                return sqlConnection.Query<string>("SELECT description FROM dbo.tblColors WHERE ColorID = @ColourId", new {ColourId = colourId}).FirstOrDefault();
            }
        }
        #endregion

        #region "Leads Source"
        public static string GetLeadSourceStringById(int leadSourceId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                return sqlConnection
                    .Query<string>("SELECT LeadSource FROM dbo.tblLeadSource WHERE SourceID = @SourceId",
                        new {SourceId = leadSourceId}).FirstOrDefault();
            }
        }
        #endregion

        #region "titles"
        public static string GetTitleStringFromId(int titleId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                return sqlConnection.Query<string>("SELECT Title FROM dbo.tblTitles WHERE TitleID = @TitleId",
                    new {TitleID = titleId}).FirstOrDefault();
            }
        }
        #endregion

        #region "Makes and Models"
        public static MakeModelVariant GetMakeModelVariantFrom(int mmcode, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                const string query =
                    "SELECT tblmakes.Make, ModelDescription AS Model, Variant FROM dbo.tblModels INNER JOIN dbo.tblMakes ON tblMakes.MakeID = tblModels.MakeID AND tblMakes.companyID = tblModels.companyID AND tblModels.MMCode = @mmcode";
                var result = sqlConnection.Query<dynamic>(query, new {mmcode = mmcode}).Single();
                return new MakeModelVariant
                {
                    Make = result.Make,
                    Model = result.Model,
                    Variant = result.Variant
                };
            }
        }

        public static string GetMakeFromMakeId(string makeId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                const string query = "SELECT Make FROM dbo.tblMakes WHERE MakeID = @makeId";
                var result = sqlConnection.Query<string>(query, new {makeId = makeId}).Single();
                return result;
            }
        }
        #endregion
        
    }


    public class MakeModelVariant
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public string Variant { get; set; }
    }
}
