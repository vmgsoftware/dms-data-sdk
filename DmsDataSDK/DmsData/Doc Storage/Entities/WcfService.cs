﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Doc_Storage.Entities
{
    [Dapper.Contrib.Extensions.Table("tblWcfService")]

    public class WcfService
    {


        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Service Id is required")]
        public int ServiceId { get; set; } // int, not null
        [MaxLength(200)]
        [StringLength(200)]
        [Required(ErrorMessage = "Service Name is required")]
        public string ServiceName { get; set; } // nvarchar(200), not null
        [MaxLength(200)]
        [StringLength(200)]
        [Required(ErrorMessage = "Local Address is required")]
        public string LocalAddress { get; set; } // nvarchar(200), not null

    }

}

