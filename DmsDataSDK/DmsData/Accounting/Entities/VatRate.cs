﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VMGSoftware.DataSDK.DmsData.Accounting.Entities
{
    [Dapper.Contrib.Extensions.Table("tblVat")]
    public class Vat
    {
        public Vat()
        {

        }
        [Dapper.Contrib.Extensions.Key]
        [Required(ErrorMessage = "Vat Id is required")]
        public int VatId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime UptoDate { get; set; }
        public decimal VatRate { get; set; }
    }
}
