﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VMGSoftware.DataSDK.LDap.Attributes;

namespace VMGSoftware.DataSDK.DmsData.Stock.Entities
{
    [TableInfo("tblStock")]
    [Dapper.Contrib.Extensions.Table("tblStock")]
    public class StockImages
    {
        [ColumnInfo(true)]
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Stock ID is required")]
        public int StockID { get; set; } // int, not null

        public byte[] MainImage { get; set; } // image, null
        public byte[] Image1 { get; set; } // image, null
        public byte[] Image2 { get; set; } // image, null
        public byte[] Image3 { get; set; } // image, null
        public byte[] Image4 { get; set; } // image, null
        public byte[] MainThumb { get; set; } // image, null
        public byte[] Image1Thumb { get; set; } // image, null
        public byte[] Image2Thumb { get; set; } // image, null
        public byte[] Image3Thumb { get; set; } // image, null
        public byte[] Image4Thumb { get; set; } // image, null
        public byte[] Image5 { get; set; } // image, null
        public byte[] Image6 { get; set; } // image, null
        public byte[] Image7 { get; set; } // image, null
        public byte[] Image8 { get; set; } // image, null
        public byte[] Image9 { get; set; } // image, null
        public byte[] Image10 { get; set; } // image, null
        public byte[] Image11 { get; set; } // image, null
        public byte[] Image12 { get; set; } // image, null
        public byte[] Image13 { get; set; } // image, null
        public byte[] Image14 { get; set; } // image, null
        public byte[] Image15 { get; set; } // image, null
        public byte[] Image16 { get; set; } // image, null
        public byte[] Image17 { get; set; } // image, null
        public byte[] Image18 { get; set; } // image, null
        public byte[] Image19 { get; set; } // image, null
        public byte[] Image5Thumb { get; set; } // image, null
        public byte[] Image6Thumb { get; set; } // image, null
        public byte[] Image7Thumb { get; set; } // image, null
        public byte[] Image8Thumb { get; set; } // image, null
        public byte[] Image9Thumb { get; set; } // image, null
        public byte[] Image10Thumb { get; set; } // image, null
        public byte[] Image11Thumb { get; set; } // image, null
        public byte[] Image12Thumb { get; set; } // image, null
        public byte[] Image13Thumb { get; set; } // image, null
        public byte[] Image14Thumb { get; set; } // image, null
        public byte[] Image15Thumb { get; set; } // image, null
        public byte[] Image16Thumb { get; set; } // image, null
        public byte[] Image17Thumb { get; set; } // image, null
        public byte[] Image18Thumb { get; set; } // image, null
        public byte[] Image19Thumb { get; set; } // image, null

        public int? companyID { get; set; } // int, null
    }
}
