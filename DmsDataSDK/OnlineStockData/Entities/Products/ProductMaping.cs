﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMGSoftware.DataSDK.OnlineStockData.Entities.Products
{
    [Table("product_mappings")]
    public class ProductMaping
    {
        [Key]
        [ForeignKey("Product")]
        public int id { get; set; }

        public virtual Product Product { get; set; }
        public string local_sql_query { get; set; }
    }
}
