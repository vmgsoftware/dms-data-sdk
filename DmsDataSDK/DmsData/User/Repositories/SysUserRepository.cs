﻿using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;

namespace VMGSoftware.DataSDK.DmsData.User.Repositories
{
    public static class SysUserRepository
    {
        public static Entities.SysUser Load(int sysUserID, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                return sqlConnection.Get<Entities.SysUser>(sysUserID);
            }
        }

        public static string GetEmailAddressForSysUser(int sysUserId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                var query =
                    "SELECT e.Email FROM dbo.tblEmployees AS e, dbo.tblSysUsers AS s WHERE s.SysUserID = @SysUserId AND e.EmployeeID = s.EmployeeID";

                var result = sqlConnection.Query<string>(query, new {SysUserId = sysUserId}).FirstOrDefault();

                return result;
            }
        }

        public static Entities.SysUser GetUserByEmployeeEmailAddress(string emailAddress, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                var query =
                    "SELECT s.* FROM dbo.tblSysUsers as s, dbo.tblEmployees WHERE s.EmployeeID = dbo.tblEmployees.EmployeeID AND dbo.tblEmployees.Email = @EmailAddress";

                var result = sqlConnection.Query<Entities.SysUser>(query, new {EmailAddress = emailAddress})
                    .SingleOrDefault();
                return result;
            }
        }

        // Upsert, <= 0 SysUserId indicates that it is new and is inserted, otherwise it is updated
        public static void Save(Entities.SysUser user, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                if (user.SysUserID <= 0)
                {
                    sqlConnection.Insert(user);
                }
                else
                {
                    sqlConnection.Update(user);
                }
            }
        }

        public static long GetUserIdByEmployeeEmailAddress(string emailAddress, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                var query =
                    "SELECT SysUserID FROM dbo.tblSysUsers as s, dbo.tblEmployees WHERE s.EmployeeID = dbo.tblEmployees.EmployeeID AND dbo.tblEmployees.Email = @EmailAddress";

                var result = sqlConnection.Query<long>(query, new {EmailAddress = emailAddress})
                    .SingleOrDefault();
                return result;
            }
        }

        public static void RegisterUserWithVmgMobile(long userId, string mobileUserId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                var query =
                    "update dbo.tblSysUsers set MobileUserId = @MobileUserId, VmgMobileRegistered = 1 where SysUserId = @UserId";
                sqlConnection.Execute(query, new {UserId = userId, MobileUserId = mobileUserId});
            }
        }

        public static void DeRegisterUserWithVmgMobile(long userId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                var query =
                    "update dbo.tblSysUsers set MobileUserId = '', VmgMobileRegistered = 0 where SysUserId = @UserId";
                sqlConnection.Execute(query, new {UserId = userId});
            }
        }
    }
}
