﻿using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using VMGSoftware.DataSDK.DmsData.MMCode.Entities;

namespace VMGSoftware.DataSDK.DmsData.MMCode.Repositories
{
    public class MMCodeDataRepository : GenericRepository
    {
        public static MMCodeData[] GetBasicMMCodeDataArray(string connectionString)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                return sqlConnection.Query<MMCodeData>("SELECT MMCode,VehicleType,Make,Model,Variant,Master_Model,RegYears FROM dbo.tblMMCodes_All").ToArray();
            }
        }

        public static string[] GetMakesFromArray(MMCodeData[] mMCodeData)
        {
            List<string> makes = new List<string>(300);
            for (int i = 0; i < mMCodeData.Length; i++)
            {
                if (!makes.Contains(mMCodeData[i].Make))
                {
                    makes.Add(mMCodeData[i].Make);
                }
            }
            return makes.OrderBy(x=> x).ToArray();
        }



        public static string[] GetMakesForCategory(MMCodeData[] mMCodeData,string vehicleType,string year = "")
        {
            List<string> makes = new List<string>(300);
            for (int i = 0; i < mMCodeData.Length; i++)
            {
                if (!makes.Contains(mMCodeData[i].Make) && mMCodeData[i].VehicleType == vehicleType && (year == "" || mMCodeData[i].RegYears == null || mMCodeData[i].RegYears.Contains(year)))
                {
                    makes.Add(mMCodeData[i].Make);
                }
            }
            return makes.OrderBy(x=> x).ToArray();
        }

        public static string[] GetModelsForMake(MMCodeData[] mMCodeData,string make,string vehicleType = "", string year = "")
        {
            List<string> master_Models = new List<string>(300);
            for (int i = 0; i < mMCodeData.Length; i++)
            {
                if (!master_Models.Contains(mMCodeData[i].Master_Model) && mMCodeData[i].Make == make && (vehicleType == "" || mMCodeData[i].VehicleType == vehicleType) && (year == "" || mMCodeData[i].RegYears == null || mMCodeData[i].RegYears.Contains(year)))
                {
                    master_Models.Add(mMCodeData[i].Master_Model);
                }
            }
            return master_Models.OrderBy(x=> x).ToArray();
        }

        public static Dictionary<string,string> GetVarientsForModel(MMCodeData[] mMCodeData,string masterModel,string year = "")
        {
            Dictionary<string,string> variants = new Dictionary<string,string>(300);
            var sortedMMCodeData = mMCodeData.OrderBy(x => x.Variant).ToArray();
            for (int i = 0; i < sortedMMCodeData.Length; i++)
            {

                if (!variants.Values.Contains(sortedMMCodeData[i].Variant) && sortedMMCodeData[i].Master_Model == masterModel && (year == "" || sortedMMCodeData[i].RegYears == null || sortedMMCodeData[i].RegYears.Contains(year)))
                {
                    variants.Add(sortedMMCodeData[i].MMCode.ToString().PadLeft(8,'0')  ,$"{sortedMMCodeData[i].Variant} ({sortedMMCodeData[i].MMCode})");
                }
            }
            return variants;
        }

        public static List<MMCodeData> GetBasicMMCodeDataList(string connectionString)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                return sqlConnection.Query<MMCodeData>("SELECT MMCode,VehicleType,Make,Model,Variant,Master_Model,RegYears FROM dbo.tblMMCodes_All").ToList();
            }
        }

    }
}
