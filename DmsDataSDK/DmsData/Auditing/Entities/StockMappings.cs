﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Auditing.Entities
{

    [Dapper.Contrib.Extensions.Table("tblStockMappings")]
    public class StockMappings
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Mapping Id is required")]
        public int MappingId { get; set; } // int, not null
        [MaxLength(100)]
        [StringLength(100)]
        [Required(ErrorMessage = "Stock Column Name is required")]
        public string StockColumnName { get; set; } // nvarchar(100), not null
        [MaxLength(100)]
        [StringLength(100)]
        [Required(ErrorMessage = "Display Name is required")]
        public string DisplayName { get; set; } // nvarchar(100), not null
        [MaxLength(100)]
        [StringLength(100)]
        public string MapsToTableName { get; set; } // nvarchar(100), null
        [MaxLength(100)]
        [StringLength(100)]
        public string MapsToTablePK { get; set; } // nvarchar(100), null
        [MaxLength(100)]
        [StringLength(100)]
        public string MapsToTableDescriptor { get; set; } // nvarchar(100), null
    }

}
