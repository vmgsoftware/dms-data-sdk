﻿using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions;
using System.ComponentModel.DataAnnotations.Schema;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Stock;
using VMGSoftware.DataSDK.LDap.Attributes;

namespace VMGSoftware.DataSDK.DmsData.Stock.Entities
{
    [TableInfo("tblStockImages")]
    [Dapper.Contrib.Extensions.Table("tblStockImages")]
    public class StockImageUrlsLite
    {
        [ColumnInfo(true)]
        [Dapper.Contrib.Extensions.Key]
        [Required(ErrorMessage = "Stock ID is required")]
        public int? StockID { get; set; } // int, null
        public string ImageURL1 { get; set; } // varchar(250), null
        public string ImageURL2 { get; set; } // varchar(250), null
        public string ImageURL3 { get; set; } // varchar(250), null
        public string ImageURL4 { get; set; } // varchar(250), null
        public string ImageURL5 { get; set; } // varchar(250), null
        public string ImageURL6 { get; set; } // varchar(250), null
        public string ImageURL7 { get; set; } // varchar(250), null
        public string ImageURL8 { get; set; } // varchar(250), null
        public string ImageURL9 { get; set; } // varchar(250), null
        public string ImageURL10 { get; set; } // varchar(250), null
        public string ImageURL11 { get; set; } // varchar(250), null
        public string ImageURL12 { get; set; } // varchar(250), null
        public string ImageURL13 { get; set; } // varchar(250), null
        public string ImageURL14 { get; set; } // varchar(250), null
        public string ImageURL15 { get; set; } // varchar(250), null
        public string ImageURL16 { get; set; } // varchar(250), null.
        public string ImageURL17 { get; set; } // varchar(250), null
        public string ImageURL18 { get; set; } // varchar(250), null
        public string ImageURL19 { get; set; } // varchar(250), null
        public string ImageURL20 { get; set; } // varchar(250), null
    }
}
