﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using VMGAuto.Data;
using VMGSoftware.DataSDK.DmsData.Stock.Entities;
using VMGSoftware.DataSDK.LDap.Logic;

namespace VMGSoftware.DataSDK.DmsData.Stock.Repositories
{
    public class StockRepository : GenericRepository
    {
        public static List<StockBasic> LoadNonDeletedStockList(SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                return sqlConnection.Query<StockBasic>("SELECT StockCode FROM tblStock where Deleted = 0 and StockCode NOT LIKE '%DEL%'").ToList();
            }
        }

        public static StockLite FastLoadStockLite(int id, SqlConnection sqlConnection)
        {
            var loadedStockItem = new StockLite();
            using (sqlConnection)
            {
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                EntityQueryBridge<StockLite> builder = new EntityQueryBridge<StockLite>(loadedStockItem,id);
                builder.PopulateEntityReader(sqlConnection);
                return loadedStockItem;
            }
        }
        
        public static bool IsPurchasePriceAndDatePresent(string stockCode, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                var result = sqlConnection.Query($"SELECT PurchaseDate, PurchasePrice FROM dbo.tblStock WHERE StockCode = '{stockCode}' and deleted = 0 ").First();
                if (result.PurchaseDate == null || result.PurchasePrice == null)
                {
                    return false;
                }

                return true;
            }
        }

        public static StockBasic LoadStockPurchasePriceAndDate(string stockCode, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                var result = sqlConnection.Query<StockBasic>($"SELECT StockId, PurchaseDate, PurchasePrice FROM dbo.tblStock WHERE StockCode = '{stockCode}' and deleted = 0 ").First();
                return result;
            }
        }

        public static StockBasic LoadStockDetailsForCrmByStockId(int stockId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                var result = sqlConnection.Query<StockBasic>($"SELECT stockid, stockcode, ModelID, MakeId, Year, SellingPrice, ColourID FROM dbo.tblStock WHERE stockid = '{stockId}' and deleted = 0 ").First();
                return result;
            }
        }

        public static string GetStockCodeForStockId(int stockId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                var result = sqlConnection.ExecuteScalar<string>(
                    "SELECT stockcode from dbo.tblStock WHERE stockid = @StockId", new {StockId = stockId});
                return result;
            }
        }

        public static decimal? GetMileageForStockId(int stockId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                var result = sqlConnection.ExecuteScalar<decimal?>(
                    "SELECT [km-in] as KmIn from dbo.tblStock WHERE stockid = @StockId", new {StockId = stockId});
                return result;
            }
        }

        public static List<int> GetStockIdsWithImages(string sqlConnectionString)
        {
            using (var sqlConnection = new SqlConnection(sqlConnectionString))
            {
                sqlConnection.Open();

                return (List<int>)sqlConnection.Query<int>("SELECT tblStock.StockID FROM dbo.tblStock LEFT JOIN dbo.tblStockImageProcessing ON tblStockImageProcessing.stockId = tblStock.StockID WHERE tblStockImageProcessing.stockId IS NULL AND  (MainImage IS NOT NULL OR Image1 IS NOT NULL OR Image2 IS NOT NULL OR Image3 IS NOT NULL OR Image4 IS NOT NULL OR Image5 IS NOT NULL OR Image6 IS NOT NULL OR Image7 IS NOT NULL OR Image8 IS NOT NULL OR Image9 IS NOT NULL OR Image10 IS NOT NULL OR Image11 IS NOT NULL OR Image12 IS NOT NULL OR Image13 IS NOT NULL OR Image14 IS NOT NULL OR Image15 IS NOT NULL OR Image16 IS NOT NULL OR Image17 IS NOT NULL OR Image18 IS NOT NULL OR Image19 IS NOT NULL)");
            }

        }

        public static List<StockImagesOverview> GetStockImagesOverview(string sqlConnectionString)
        {
            using (var sqlConnection = new SqlConnection(sqlConnectionString))
            {
                sqlConnection.Open();

                return (List<StockImagesOverview>)sqlConnection.Query<StockImagesOverview>("SELECT StockID,CASE WHEN MainImage IS NOT NULL THEN 1 ELSE 0 END AS MainImage, CASE WHEN Image1 IS NOT NULL THEN 1 ELSE 0 END AS Image1,CASE WHEN Image2 IS NOT NULL THEN 1 ELSE 0 END AS Image2,CASE WHEN Image3 IS NOT NULL THEN 1 ELSE 0 END AS Image3,CASE WHEN Image4 IS NOT NULL THEN 1 ELSE 0 END AS Image4,CASE WHEN Image5 IS NOT NULL THEN 1 ELSE 0 END AS Image5,CASE WHEN Image6 IS NOT NULL THEN 1 ELSE 0 END AS Image6,CASE WHEN Image7 IS NOT NULL THEN 1 ELSE 0 END AS Image7,CASE WHEN Image8 IS NOT NULL THEN 1 ELSE 0 END AS Image8,CASE WHEN Image9 IS NOT NULL THEN 1 ELSE 0 END AS Image9,CASE WHEN Image10 IS NOT NULL THEN 1 ELSE 0 END AS Image10,CASE WHEN Image11 IS NOT NULL THEN 1 ELSE 0 END AS Image11,CASE WHEN Image12 IS NOT NULL THEN 1 ELSE 0 END AS Image12,CASE WHEN Image13 IS NOT NULL THEN 1 ELSE 0 END AS Image13,CASE WHEN Image14 IS NOT NULL THEN 1 ELSE 0 END AS Image14,CASE WHEN Image15 IS NOT NULL THEN 1 ELSE 0 END AS Image15,CASE WHEN Image16 IS NOT NULL THEN 1 ELSE 0 END AS Image16,CASE WHEN Image17 IS NOT NULL THEN 1 ELSE 0 END AS Image17,CASE WHEN Image18 IS NOT NULL THEN 1 ELSE 0 END AS Image18,CASE WHEN Image19 IS NOT NULL THEN 1 ELSE 0 END AS Image19 FROM dbo.tblStock");
            }
        }

        public static List<int> GetAllActiveStockIds(string localConnectionString)
        {
            using (var sqlConnection = new SqlConnection(localConnectionString))
            {
                sqlConnection.Open();
                return (List<int>) sqlConnection.Query<int>(
                    "SELECT StockID FROM tblstock WHERE Deleted = 0 AND Archived = 0 AND IsStockDataOnline = 0");
            }
        }

        public static List<int> GetStockIdsToRemoveFromOnline(string localConnectionString)
        {
            using (var sqlConnection = new SqlConnection(localConnectionString))
            {
                sqlConnection.Open();
                return (List<int>) sqlConnection.Query<int>(
                    "SELECT StockID FROM tblstock WHERE (Deleted = 1 OR Archived = 1) AND IsStockDataOnline = 1");
            }
        }

        public static void OpenConnectionIfClosed(SqlConnection sqlConnection)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                sqlConnection.Open();
            }
        }
    }
}
