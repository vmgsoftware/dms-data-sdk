﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Feeds;
using VMGSoftware.DataSDK.OnlineStockData.Migrations;

namespace VMGSoftware.DataSDK.OnlineStockData.Mappers
{
    public static class CompanyFeedsMapper
    {
        public static List<CompanyFeed> Map(int companyId,string connectionString,StockFeedDataContext dbContext)
        {
            List<CompanyFeed> companyFeeds = new List<CompanyFeed>();
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                var activeFeeds = sqlConnection.Get<ActiveFeeds>(companyId);


                companyFeeds.Add(GetCompanyFeed(companyId,"Swarm",activeFeeds.enableswarm,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"AllAuto",activeFeeds.enableAllAuto,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"Carborator",activeFeeds.enableCarborator,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"Hippo.co.za",activeFeeds.enableHippoCoZa,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"AutoDealer",activeFeeds.enableautodealer,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"PriceCheck",activeFeeds.enablePriceCheck,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"Automart",activeFeeds.enableautomart,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"BlueChip",activeFeeds.enablebluechip,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"Capitec",activeFeeds.enablecapitec,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"CarFind",activeFeeds.enablecarfind,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"Cars4Sa",activeFeeds.enablecars4sa,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"Cars.co.za",activeFeeds.enablecarscoza,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"e-GoodManners",activeFeeds.enableemanners,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"Gumtree",activeFeeds.enablegumtree,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"Surf4Cars",activeFeeds.enablesurf4cars,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"RedFeed",activeFeeds.enableredfeed,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"Olx",activeFeeds.enableolx,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"Chase",activeFeeds.enablechase,dbContext));
                companyFeeds.Add(GetCompanyFeed(companyId,"IXOnline",activeFeeds.enableixonline,dbContext));
                
            }

            companyFeeds.RemoveAll(cf => cf == null);
            return companyFeeds;
        }

        private static CompanyFeed GetCompanyFeed(int companyId, string feedName,bool? activated,StockFeedDataContext dbContext)
        {
            if (activated == null) activated = false;
            var companyFeed = new CompanyFeed();
            companyFeed.company_id = companyId;
            companyFeed.enabled = (bool)activated;
            companyFeed.feed = dbContext.Feeds.FirstOrDefault(x => x.feed_name == feedName);
            var existingFeedActivation = dbContext.CompanyFeeds.Where(f => f.company_id == companyId && f.feed.id == companyFeed.feed.id && f.enabled == activated)
                                                            .OrderByDescending(f => f.date_of_change).FirstOrDefault();
            
            return existingFeedActivation == null ? companyFeed : null;
        }
    }
}
