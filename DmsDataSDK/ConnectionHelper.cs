﻿using System;
using System.Data;

namespace VMGSoftware.DataSDK
{
    //WIP
    public static class ConnectionHelper
    {
        public static TReturn Connect<TReturn>(IDbConnection dbConnection,
            Func<IDbConnection, TReturn> function)
        {
            return UsingWrapper.Using(dbConnection, connection =>
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                return function(connection);
            });
        }
    }

    public static class UsingWrapper
    {
        public static TReturn Using<TDisposable, TReturn>(TDisposable disposable, Func<TDisposable, TReturn> f)
            where TDisposable : IDisposable
        {
            using (disposable)
            {
                return f(disposable);
            }
        }
    }
}