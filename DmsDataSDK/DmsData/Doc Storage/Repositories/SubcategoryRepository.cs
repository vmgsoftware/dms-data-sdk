﻿using System.Data.SqlClient;
using Dapper.Contrib.Extensions;
using VMGAuto.Data;

namespace VMGSoftware.DataSDK.DmsData.Doc_Storage.Repositories
{
    public class SubcategoryRepository : GenericRepository
    {
        public static Entities.Subcategory Load(int subcategoryId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                return sqlConnection.Get<Entities.Subcategory>(subcategoryId);
            }
        }
    }
}
