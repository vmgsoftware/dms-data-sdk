﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using VMGAuto.Data;
using VMGSoftware.DataSDK.DmsData.User.Entities;

namespace VMGSoftware.DataSDK.DmsData.User.Repositories
{
    public class UserLevelSettingRepository : GenericRepository
    {
        public static Entities.UserLevelSetting LoadByUserLevelAndKeyElseCreate(int userLevelId, string key, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                var loadedSetting = sqlConnection.QueryFirstOrDefault<Entities.UserLevelSetting>($"SELECT * FROM tblUserLevelSettings WHERE [Key] = '{key}' AND [UserLevelId] = '{userLevelId}'");
                if (loadedSetting == null)
                {
                    loadedSetting = new UserLevelSetting();
                    loadedSetting.Key = key;
                    loadedSetting.UserLevelId = userLevelId;
                    loadedSetting.value = "";
                }

                return loadedSetting;
            }
        }

        public static Dictionary<string,string> LoadAllForUserLevel(int userLevelId, SqlConnection sqlConnection)
        {
            var userLevelKeyValueDictionary = new Dictionary<string,string>();
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                var userLevelSettings = sqlConnection.Query<Entities.UserLevelSetting>($"SELECT * FROM tblUserLevelSettings WHERE [UserLevelId] = '{userLevelId}'");

                foreach (Entities.UserLevelSetting Setting in userLevelSettings)
                {
                    userLevelKeyValueDictionary.Add(Setting.Key,Setting.value);
                }
            }
            return userLevelKeyValueDictionary;
        }


    }
}