﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Doc_Storage.Entities
{
    [Dapper.Contrib.Extensions.Table("tblDocStorageAuditLog")]
    public class AuditLog
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Log Id is required")]
        public int LogId { get; set; } // int, not null
        [MaxLength(100)]
        [StringLength(100)]
        [Required(ErrorMessage = "Action Type is required")]
        public string ActionType { get; set; } // nvarchar(100), not null
        [Required(ErrorMessage = "User Id is required")]
        public int UserId { get; set; } // int, not null
        [Required(ErrorMessage = "Stock Id is required")]
        public int StockId { get; set; } // int, not null
        [Required(ErrorMessage = "Size is required")]
        public double Size { get; set; } // float, not null
        [MaxLength(1000)]
        [StringLength(1000)]
        [Required(ErrorMessage = "Full File Path is required")]
        public string FullFilePath { get; set; } // nvarchar(1000), not null
        [Required(ErrorMessage = "Time Stamp is required")]
        public DateTime TimeStamp { get; set; } // datetime, not null

    }
}
