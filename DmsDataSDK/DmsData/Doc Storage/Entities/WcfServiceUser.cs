﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Doc_Storage.Entities
{
    [Dapper.Contrib.Extensions.Table("tblWcfServiceUsers")]

    public class WcfServiceUser
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Service User Id is required")]
        public int ServiceUserId { get; set; } // int, not null
        [Required(ErrorMessage = "Service Id is required")]
        public int ServiceId { get; set; } // int, not null
        [Required(ErrorMessage = "User Id is required")]
        public int UserId { get; set; } // int, not null
    }
}
