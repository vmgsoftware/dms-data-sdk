﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;

namespace VMGSoftware.DataSDK.GoogleSheetData.Repositories
{
    public class AppClient
    {
        private const string SecretFileName = "client_secret.json";
        private static string[] Scopes = { SheetsService.Scope.Spreadsheets };
        private static string ApplicationName = "VMG DMS Service Dashboard Client";

        internal static SheetsService AuthorizeGoogleApp()
        {
            UserCredential credential;
            using (var stream =
                new FileStream($"{System.AppDomain.CurrentDomain.BaseDirectory}\\{SecretFileName}", FileMode.Open, FileAccess.Read))
            {
                string credPath = $"{System.AppDomain.CurrentDomain.BaseDirectory}";
                credPath = Path.Combine(credPath, ".credentials/sheets.googleapis.com-dotnet-quickstart.json");
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }
            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            return service;
        }

        public static SheetsService AuthorizeGoogleApp(string clientId, string clientSecret,string refreshToken = "1/T5ThazpHn2G5mJs1grclyVIJDzA-DKHvRnicESxhvBe2cdt6ifJ3OtDYbkFPAiOF")
        {
            ClientSecrets secrets = new ClientSecrets()
            {
                ClientId = clientId,
                ClientSecret = clientSecret
            };

            var token = new TokenResponse { RefreshToken = refreshToken }; 
            var credentials = new UserCredential(new GoogleAuthorizationCodeFlow(
                    new GoogleAuthorizationCodeFlow.Initializer 
                    {
                        ClientSecrets = secrets
                    }), 
                "user", 
                token);

            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credentials,
                ApplicationName = "TestProject"
            });
            return service;

        }

        internal static void AppendGoogleSheetinBatch(IList<IList<Object>> values, string spreadsheetId, string range, SheetsService service)
        {
            SpreadsheetsResource.ValuesResource.AppendRequest request =
                service.Spreadsheets.Values.Append(new ValueRange() { Values = values }, spreadsheetId, range);
            request.InsertDataOption = SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum.INSERTROWS;
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
            var response = request.Execute();
        }

        internal static void UpdateGoogleSheetinBatch(IList<IList<Object>> values, string spreadsheetId, string range, SheetsService service)
        {
            SpreadsheetsResource.ValuesResource.UpdateRequest request =
                service.Spreadsheets.Values.Update(new ValueRange() { Values = values }, spreadsheetId, range);
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.USERENTERED;
            var response = request.Execute();
        }

        internal static IList<IList<Object>> GetValuesFromSheet(string spreadsheetId, string range, SheetsService service)
        {
            SpreadsheetsResource.ValuesResource.GetRequest getRequest =
                service.Spreadsheets.Values.Get(spreadsheetId, range);
            ValueRange getResponse = getRequest.Execute();
            return getResponse.Values;
        }
    }
}
