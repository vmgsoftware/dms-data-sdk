﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Doc_Storage.Entities
{
    [Dapper.Contrib.Extensions.Table("tblDocStorageIndexes")]
    public class StorageIndex
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Index Id is required")]
        public int IndexId { get; set; } // int, not null
        [Required(ErrorMessage = "Stock Id is required")]
        public int StockId { get; set; } // int, not null
        [MaxLength(500)]
        [StringLength(500)]
        [Required(ErrorMessage = "File Name is required")]
        public string FileName { get; set; } // nvarchar(500), not null
        [Required(ErrorMessage = "Sub Category Id is required")]
        public int SubCategoryId { get; set; } // int, not null
        [Required(ErrorMessage = "File Size is required")]
        public double FileSize { get; set; } // float, not null
        [Required(ErrorMessage = "Date Uploaded is required")]
        public DateTime DateUploaded { get; set; } // datetime, not null
        [Required(ErrorMessage = "Is Pending is required")]
        public bool IsPending { get; set; } // bit, not null

    }
}
