﻿using System.Data.SqlClient;
using Dapper.Contrib.Extensions;
using VMGAuto.Data;

namespace VMGSoftware.DataSDK.DmsData.Stock.Repositories
{
    public class zpnlndsRepository : GenericRepository
    {
        public static Entities.zpnInds Load(int stockId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                return sqlConnection.Get<Entities.zpnInds>(stockId);
            }
        }

        public static void Save(Entities.zpnInds zpnInds, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                sqlConnection.Update(zpnInds);
            }
        }
    }
}
