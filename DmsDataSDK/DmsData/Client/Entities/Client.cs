﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Client.Entities
{
    [Dapper.Contrib.Extensions.Table("tblClients")]
    public class Client
    {
        public Client()
        {
            this.Deleted = false;
            this.DateCreated = DateTime.Now;
            this.NonVAT = false;
            this.ActivatedAcc = false;
            this.WarrantyClient = false;
            this.AltInvoice = false;
            this.RandTrust = false;
            this.CashSale = false;
            this.OrderNoReq = false;
            this.BlockClient = false;
            this.LookingForNotification = false;
            this.wasLead = false;
            this.isLead = false;
            this.SendBirthday = false;
            this.SendLicence = false;
            this.SendCommunications = false;
            this.date_downloaded = DateTime.Now;
            this.not_interested = false;
            this.fuel_client = false;
            this.T20Taxi = false;
            this.ClientSuburb = "";
            this.ClientCity = "";
            this.ClientPostalSuburb = "";
            this.ClientPostalCity = "";
        }

        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Client ID is required")]
        public int ClientID { get; set; } // int, not null
        public int? ClientTitleID { get; set; } // int, null
        [MaxLength(50)]
        public string ClientName { get; set; } // varchar(50), null
        [MaxLength(30)]
        public string ClientSurname { get; set; } // varchar(30), null
        [MaxLength(20)]
        public string ClientContactNr { get; set; } // varchar(20), null
        [MaxLength(100)]
        public string ClientAddress1 { get; set; } // varchar(100), null
        [MaxLength(100)]
        public string ClientAddress2 { get; set; } // varchar(100), null
        [MaxLength(20)]
        public string ClientVatNr { get; set; } // varchar(20), null
        [MaxLength(100)]
        public string ClientAddress3 { get; set; } // varchar(100), null
        public bool? Deleted { get; set; } // bit, null
        public DateTime? DateCreated { get; set; } // datetime, null
        [MaxLength(20)]
        public string Reference { get; set; } // varchar(20), null
        [MaxLength(20)]
        public string ClientIDNr { get; set; } // varchar(20), null
        [MaxLength(50)]
        public string FileAs { get; set; } // varchar(50), null
        [MaxLength(15)]
        public string OldID { get; set; } // varchar(15), null
        [MaxLength(8)]
        public string ClientCode { get; set; } // varchar(8), null
        [MaxLength(50)]
        public string CompanyName { get; set; } // varchar(50), null
        [MaxLength(20)]
        public string CompRegNo { get; set; } // varchar(20), null
        [MaxLength(6)]
        public string TelCode { get; set; } // varchar(6), null
        [MaxLength(20)]
        public string TelNo { get; set; } // varchar(20), null
        [MaxLength(6)]
        public string FaxCode { get; set; } // varchar(6), null
        [MaxLength(20)]
        public string FaxNo { get; set; } // varchar(20), null
        [MaxLength(15)]
        public string Cell { get; set; } // varchar(15), null
        [MaxLength(50)]
        public string Email { get; set; } // varchar(50), null
        [MaxLength(6)]
        public string StreetPcode { get; set; } // varchar(6), null
        [MaxLength(40)]
        public string PostAddr1 { get; set; } // varchar(40), null
        [MaxLength(40)]
        public string PostAddr2 { get; set; } // nvarchar(40), null
        [MaxLength(40)]
        public string PostAddr3 { get; set; } // nvarchar(40), null
        [MaxLength(6)]
        public string PostPcode { get; set; } // varchar(6), null
        public int? PaymentTermID { get; set; } // int, null
        public int? PaymentTypeID { get; set; } // int, null
        public int? RepID { get; set; } // int, null
        public decimal? ClientDiscount { get; set; } // decimal(18,0), null
        [MaxLength(1000)]
        public string Notes { get; set; } // varchar(1000), null
        [MaxLength(6)]
        public string OtherCode { get; set; } // varchar(6), null
        [MaxLength(20)]
        public string OtherNo { get; set; } // varchar(20), null
        [MaxLength(6)]
        public string Other1Code { get; set; } // varchar(6), null
        [MaxLength(20)]
        public string Other1No { get; set; } // varchar(20), null
        public int? ClientRel1 { get; set; } // int, null
        public int? ClientRel2 { get; set; } // int, null
        public int? ClientRel3 { get; set; } // int, null
        public int? ClientRel4 { get; set; } // int, null
        public bool? NonVAT { get; set; } // bit, null
        [MaxLength(100)]
        public string ClientEmail { get; set; } // varchar(100), null
        [MaxLength(20)]
        public string ClientContactNr2 { get; set; } // varchar(20), null
        public bool? ActivatedAcc { get; set; } // bit, null
        [MaxLength(20)]
        public string Username { get; set; } // nvarchar(20), null
        [MaxLength(20)]
        public string Password { get; set; } // nvarchar(20), null
        [MaxLength(20)]
        public string IDNumber { get; set; } // nvarchar(20), null
        [MaxLength(30)]
        public string StreetCountry { get; set; } // nvarchar(30), null
        [MaxLength(30)]
        public string PostCountry { get; set; } // nvarchar(30), null
        public int? WebClientID { get; set; } // int, null
        public bool? WarrantyClient { get; set; } // bit, null
        public bool? AltInvoice { get; set; } // bit, null
        [MaxLength(20)]
        public string Branch { get; set; } // nvarchar(20), null
        public bool? RandTrust { get; set; } // bit, null
        public decimal? CreditLimit { get; set; } // money, null
        [MaxLength(200)]
        public string EmailCC { get; set; } // nvarchar(200), null
        [MaxLength(200)]
        public string EmailCC2 { get; set; } // nvarchar(200), null
        public int? SalesSourceID { get; set; } // int, null
        public bool? CashSale { get; set; } // bit, null
        public int? BranchID { get; set; } // int, null
        public bool? OrderNoReq { get; set; } // bit, null
        public bool? BlockClient { get; set; } // bit, null
        public int? leadSourceID { get; set; } // int, null
        [MaxLength(6)]
        public string LookingForMakeID { get; set; } // nvarchar(6), null
        public int? LookingForMMcode { get; set; } // int, null
        public int? LookingForYear { get; set; } // int, null
        public decimal? LookingForPrice { get; set; } // money, null
        public bool? LookingForNotification { get; set; } // bit, null
        public int? AssignedToID { get; set; } // int, null
        public bool? wasLead { get; set; } // bit, null
        public bool? isLead { get; set; } // bit, null
        public bool? SendBirthday { get; set; } // bit, null
        public bool? SendLicence { get; set; } // bit, null
        public bool? SendCommunications { get; set; } // bit, null
        public int? lookingForColourID { get; set; } // int, null
        [MaxLength(100)]
        public string ModelDescription { get; set; } // varchar(100), null
        public int? hosted_lead_id { get; set; } // int, null
        public int? stockid { get; set; } // int, null
        public DateTime? date_downloaded { get; set; } // datetime, null
        public int? first_contact { get; set; } // int, null
        public bool? not_interested { get; set; } // bit, null
        public bool? fuel_client { get; set; } // bit, null
        public bool? T20Taxi { get; set; } // bit, null
        public int? OtherSourceID { get; set; } // int, null
        [MaxLength(150)]
        public string FinAppRejectReason { get; set; } // nvarchar(150), null
        public int? FinAppStatusID { get; set; } // int, null
        public int? RecycleBin_ID { get; set; } // int, null
        [MaxLength(25)]
        public string ClientSuburb { get; set; } // nvarchar(25), null
        [MaxLength(25)]
        public string ClientCity { get; set; } // nvarchar(25), null
        [MaxLength(25)]
        public string ClientPostalSuburb { get; set; } // nvarchar(25), null
        [MaxLength(25)]
        public string ClientPostalCity { get; set; } // nvarchar(25), null
        [MaxLength(200)]
        public string MobileLeadId {get;set;} // nvarchar(200), null
    }
}
