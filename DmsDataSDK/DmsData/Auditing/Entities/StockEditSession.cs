﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Auditing.Entities
{
    [Dapper.Contrib.Extensions.Table("tblStockEditSession")]
    public class StockEditSession
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Change Id is required")]
        public int ChangeId { get; set; } // int, not null
        [MaxLength(100)]
        [StringLength(100)]
        [Required(ErrorMessage = "Column Changed is required")]
        public string ColumnChanged { get; set; } // nvarchar(100), not null
        [MaxLength(100)]
        [StringLength(100)]
        public string DisplayName { get; set; } // nvarchar(100), not null
        public string OldValue { get; set; } // text, null
        public string NewValue { get; set; } // text, null
        public string DescriptiveOldValue { get; set; } // text, null
        public string DescriptiveNewValue { get; set; } // text, null
        [Required(ErrorMessage = "Action Id is required")]
        public int ActionId { get; set; } // int, not null
    }

}
