﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using VMGSoftware.DataSDK.DmsData.Stock.Entities;

namespace VMGSoftware.DataSDK.DmsData.Stock.Repositories
{
    public class DiscountCreditNoteRepository : GenericRepository
    {
        public static DiscountCreditNote LoadByStockId(string connectionString,int stockId )
        {
            using (var sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                return sqlCon.QueryFirstOrDefault<DiscountCreditNote>($"SELECT * FROM tblDiscountCreditNote where StockId = {stockId}");
            }
        }
    }
}
