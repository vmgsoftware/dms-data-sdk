﻿namespace VMGSoftware.DataSDK.LDap.Attributes
{
    public class ColumnInfo : System.Attribute
    {
        public bool IsIdColumn;

        public ColumnInfo(bool isIdColumn)
        {
            IsIdColumn = isIdColumn;
        }
    }
}
