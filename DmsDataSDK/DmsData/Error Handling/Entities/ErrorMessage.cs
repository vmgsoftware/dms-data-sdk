﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Error_Handling.Entities
{
    [Table("tblErrorMessages")]

    public class ErrorMessage
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "error Message Id is required")]
        public int errorMessageId { get; set; } // int, not null
        [MaxLength(500)]
        [StringLength(500)]
        [Required(ErrorMessage = "error Type is required")]
        public string errorType { get; set; } // nvarchar(500), not null
        [MaxLength(500)]
        [StringLength(500)]
        [Required(ErrorMessage = "error Header is required")]
        public string errorHeader { get; set; } // nvarchar(500), not null
        [MaxLength(2000)]
        [StringLength(2000)]
        [Required(ErrorMessage = "error Subheader is required")]
        public string errorSubheader { get; set; } // nvarchar(2000), not null
        [MaxLength(4000)]
        [StringLength(4000)]
        [Required(ErrorMessage = "error Message is required")]
        public string errorMessage { get; set; } // nvarchar(max), not null
        [Required(ErrorMessage = "close Dms After Error is required")]
        public bool closeDmsAfterError { get; set; } // bit, not null

    }
}
