﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Auditing.Entities
{
    [Dapper.Contrib.Extensions.Table("tblAuditColumnsToIgnore")]
    public class AuditColumnToIgnore
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Column Id is required")]
        public int ColumnId { get; set; } // int, not null
        [MaxLength(100)]
        [StringLength(100)]
        [Required(ErrorMessage = "Column Name is required")]
        public string ColumnName { get; set; } // nvarchar(100), not null
        [MaxLength(100)]
        [StringLength(100)]
        [Required(ErrorMessage = "Table Name is required")]
        public string TableName { get; set; } // nvarchar(100), not null
    }

}
