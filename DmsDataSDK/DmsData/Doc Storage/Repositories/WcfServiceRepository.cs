﻿using System.Data;
using System.Data.SqlClient;
using Dapper;
using VMGAuto.Data;
using VMGSoftware.DataSDK.DmsData.Doc_Storage.Entities;

namespace VMGSoftware.DataSDK.DmsData.Doc_Storage.Repositories
{
    public class WcfServiceRepository : GenericRepository
    {
        public static WcfService GetServiceByAddres(string address, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != ConnectionState.Open) sqlConnection.Open();

                return sqlConnection.QueryFirstOrDefault<WcfService>(
                    $"SELECT * FROM tblWcfService WHERE LocalAddress = '{address}'");
            }
        }

        public static WcfService GetSetviceForUserId(int userId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != ConnectionState.Open) sqlConnection.Open();

                WcfServiceUser user = WcfServiceUserRepository.GetServiceUserByUserId(userId, sqlConnection);
                return WcfServiceRepository.Load<WcfService>(user.ServiceId, sqlConnection);
            }
        }
    }
}