﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Company.Entities
{
    [Dapper.Contrib.Extensions.Table("tblCompanyBranches")]
    public class Branch: System.ComponentModel.DataAnnotations.IValidatableObject
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "BranchId is required")]
        public int BranchId { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [Required]
        [MaxLength(3)]
        public string Code { get; set; }

        [MaxLength(8)]
        public string Purchase_account { get; set; }

        [MaxLength(8)]
        public string Purchase_Account_Non { get; set; }

        [MaxLength(8)]
        public string New_Purchase_Account { get; set; }

        [MaxLength(8)]
        public string New_Purchase_Account_Non { get; set; }

        [MaxLength(8)]
        public string Sales_Account { get; set; }

        [MaxLength(8)]
        public string Sales_Account_Non { get; set; }

        [MaxLength(8)]
        public string New_Sales_Account { get; set; }

        [MaxLength(8)]
        public string New_Sales_Account_Non { get; set; }

        [MaxLength(8)]
        public string Supplementary_Account { get; set; }

        [MaxLength(8)]
        public string Supplementary_Account_Non { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            Validator.TryValidateProperty(Name, new ValidationContext(this) {MemberName = "Name"}, results);
            Validator.TryValidateProperty(Code, new ValidationContext(this) {MemberName = "Code"}, results);
            Validator.TryValidateProperty(Purchase_account, new ValidationContext(this) {MemberName = "Purchase_account" }, results);
            Validator.TryValidateProperty(Purchase_Account_Non, new ValidationContext(this) { MemberName = "Purchase_Account_Non" }, results);
            Validator.TryValidateProperty(New_Purchase_Account, new ValidationContext(this) { MemberName = "New_Purchase_Account" }, results);
            Validator.TryValidateProperty(New_Purchase_Account_Non, new ValidationContext(this) { MemberName = "New_Purchase_Account_Non" }, results);

            Validator.TryValidateProperty(Sales_Account, new ValidationContext(this) { MemberName = "Sales_Account" }, results);
            Validator.TryValidateProperty(Sales_Account_Non, new ValidationContext(this) { MemberName = "Sales_Account_Non" }, results);
            Validator.TryValidateProperty(New_Sales_Account, new ValidationContext(this) { MemberName = "New_Sales_Account" }, results);
            Validator.TryValidateProperty(New_Sales_Account_Non, new ValidationContext(this) { MemberName = "New_Sales_Account_Non" }, results);

            Validator.TryValidateProperty(Supplementary_Account, new ValidationContext(this) { MemberName = "Supplementary_Account" }, results);
            Validator.TryValidateProperty(Supplementary_Account_Non, new ValidationContext(this) { MemberName = "Supplementary_Account_Non" }, results);

            return results;
        }
    }
}
