﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Stock.Entities
{
    [Dapper.Contrib.Extensions.Table("tblDiscountCreditNote")]
    public class DiscountCreditNote
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CreditNoteNr { get; set; }
        [Required]
        public int StockId { get; set; }
        public decimal? IncorrectAmount { get; set; }
        public decimal? CorrectAmount { get; set; }
        public decimal? NettAmount { get; set; }
    }

}
