﻿using System.ComponentModel.DataAnnotations;

namespace VMGSoftware.DataSDK.DmsData.Accounting.Entities
{
    [Dapper.Contrib.Extensions.Table("tblTaxTypes")]
    public class TaxType
    {
        [Dapper.Contrib.Extensions.Key]
        [Required(ErrorMessage = "TaxTypeId is Required")]
        public string TaxTypeId { get; set; }
        
        public string Description { get; set; }
        public bool UpdateableVatRate { get; set; }
    }
}
