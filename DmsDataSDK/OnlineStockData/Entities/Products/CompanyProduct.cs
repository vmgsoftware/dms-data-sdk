﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMGSoftware.DataSDK.OnlineStockData.Entities.Products
{
    [Table("company_products")]
    public class CompanyProduct
    {
        [Key] 
        public int id { get; set; }

        public Product product { get; set; }
        public bool activated { get; set; }
        public int company_id { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime date_of_change { get; set; }
        public string changed_by_user { get; set; }
        public string changed_by_computer { get; set; }
    }
}
