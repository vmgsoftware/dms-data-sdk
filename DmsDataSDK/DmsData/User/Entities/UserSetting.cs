﻿using System.ComponentModel.DataAnnotations;

namespace VMGSoftware.DataSDK.DmsData.User.Entities
{
    [Dapper.Contrib.Extensions.Table("tblUserSettings")]
    public class UserSettings
    {
        [Dapper.Contrib.Extensions.Key]
        [Required(ErrorMessage = "User Setting Id is required")]
        public int UserSettingId { get; set; } // int, not null
        [Required(ErrorMessage = "User Id is required")]
        public int UserId { get; set; } // int, not null
        [MaxLength(500)]
        [StringLength(500)]
        [Required(ErrorMessage = "Key is required")]
        public string Key { get; set; } // nvarchar(500), not null
        [Required(ErrorMessage = "Value is required")]
        public string Value { get; set; } // text, not null
    }
}
