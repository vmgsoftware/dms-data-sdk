﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Company.Entities
{
    [Dapper.Contrib.Extensions.Table("tblStdVar")]
    public class StandardVariables
    {
        public StandardVariables()
        {
            this.allocationNumber = 0;
            this.Uploading2HQ = false;
            this.dbUpdatesRunning = false;
            this.feedFTPUrl = "";
            this.feedFTPUsername = "";
            this.feedFTPPassword = "";
            this.enableBPFMC = false;
            this.ScriptBusy = false;
            this.subletMarkUp = false;
            this.ShowStockTakeOn = false;
            this.ShowStockReceipt = false;
            this.LoadStockList = false;
            this.MonthEndReminder = false;
            this.ForceVin = false;
            this.CreditLimitCheck = false;
            this.SHOWVINPO = false;
            this.DiscountCheck = false;
            this.ShowPONotes = false;
            this.UseLastPurchasePrice = false;
            this.JCCheckList = false;
            this.VehicleDetailsSublet = false;
            this.DontAllowNegativeStock = false;
            this.ShowJCDate = false;
            this.LabourRequired = false;
            this.EditLabourDesc = false;
            this.CompanyFirst = false;
            this.DisplayRMILogo = false;
            this.PaymentTypeReminder = false;
            this.BarcodeScanner = false;
            this.MultiBranch = false;
            this.WorkshopLite = false;
            this.CRM = false;
            this.new_selling_price_required = false;
            this.calculate_New_Selling_Price = false;
            this.useSuppInvRefGL = false;
            this.enableTBImport = false;
            this.T20Taxi = false;
            this.daily_task_email = false;
            this.reqKMout = false;
            this.VMGStatisticsDBAddress = "";
            this.VMGStatisticsDBPort = "";
            this.VMGStatisticsDBUsername = "";
            this.VMGStatisticsDBPassword = "";
            this.VMGStatisticsDBName = "";
            this.VMGStatisticsEnabled = false;
            this.TransunionServicesEndpoint = "";
            this.TUVehicleDataDBAddress = "";
            this.TUVehicleDataDBPort = "";
            this.TUVehicleDataDBUsername = "";
            this.TUVehicleDataDBPassword = "";
            this.TUVehicleDataDBName = "";
            this.TUVehicleDataEnabled = false;
            this.TUVehicleDataUpdateInProgress = false;
            this.RMISundry = false;
            this.OrderPrint = false;
            this.P2POTPPack = false;
            this.TUVehicleDataUpdateInProgressTime = new DateTime(2017, 1, 1);
            this.MobileServiceUrl = "";
            this.MobileUsername = "";
            this.MobilePassword = "";
            this.MobileBranchGuid = "";
            this.VmgMobileEnabled = false;
            this.sendSMSToAutoAssignedEmployee = false;
            this.TransunionVehicleInformationEnabled = false;
            this.use_last_selling_price = false;
        }

        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Std Var ID is required")]
        public int StdVarID { get; set; } // int, not null
        public byte[] NoImage { get; set; } // image, null
        public int? allocationNumber { get; set; } // int, null
        public int? AccountingYear { get; set; } // int, null
        public int? AccountingPeriod { get; set; } // int, null
        public int? AccountingMonth { get; set; } // int, null
        public int? StockPeriod { get; set; } // int, null
        public int? LedgerPeriod { get; set; } // int, null
        public int? PurchasesOrderNumber { get; set; } // int, null
        public int? creditNoteNR { get; set; } // int, null
        public int? companyID { get; set; } // int, null
        public int? StockYear { get; set; } // int, null
        public int? AccountingYearTB { get; set; } // int, null
        public int? NextPOWorkID { get; set; } // int, null
        public int? NextPOID { get; set; } // int, null
        public int? NextInvNo { get; set; } // int, null
        public int? WIPInUser { get; set; } // int, null
        public int? LastWSInvoice { get; set; } // int, null
        public int? LastWSCreditNote { get; set; } // int, null
        public DateTime? VerExpDate { get; set; } // datetime, null
        [MaxLength(20)]
        [StringLength(20)]
        public string CurVer { get; set; } // nvarchar(20), null
        public int? LastJobCardNo { get; set; } // int, null
        public int? ScriptID { get; set; } // int, null
        public DateTime? Uploading2HQDate { get; set; } // datetime, null
        public bool? Uploading2HQ { get; set; } // bit, null
        [MaxLength(100)]
        [StringLength(100)]
        public string PostDaysEmail { get; set; } // varchar(100), null
        public int? PostNrDays { get; set; } // int, null
        public bool? dbUpdatesRunning { get; set; } // bit, null
        [MaxLength(200)]
        [StringLength(200)]
        public string feedFTPUrl { get; set; } // varchar(200), null
        [MaxLength(200)]
        [StringLength(200)]
        public string feedFTPUsername { get; set; } // varchar(200), null
        [MaxLength(200)]
        [StringLength(200)]
        public string feedFTPPassword { get; set; } // varchar(200), null
        public bool? enableBPFMC { get; set; } // bit, null
        public int? LastOrderNo { get; set; } // int, null
        public bool? ScriptBusy { get; set; } // bit, null
        public DateTime? VerReleaseDate { get; set; } // datetime, null
        public bool? subletMarkUp { get; set; } // bit, null
        public decimal? subletMarkupPerc { get; set; } // decimal(18,2), null
        public bool? ShowStockTakeOn { get; set; } // bit, null
        public bool? ShowStockReceipt { get; set; } // bit, null
        public bool? LoadStockList { get; set; } // bit, null
        public int? internalSupplierID { get; set; } // int, null
        public int? ServiceDateActionUser { get; set; } // int, null
        public bool? MonthEndReminder { get; set; } // bit, null
        public bool? ForceVin { get; set; } // bit, null
        public bool? CreditLimitCheck { get; set; } // bit, null
        public bool? SHOWVINPO { get; set; } // bit, null
        public bool? DiscountCheck { get; set; } // bit, null
        public int? lastCashReceiptNo { get; set; } // int, null
        public bool? ShowPONotes { get; set; } // bit, null
        public bool? UseLastPurchasePrice { get; set; } // bit, null
        public bool? JCCheckList { get; set; } // bit, null
        public bool? VehicleDetailsSublet { get; set; } // bit, null
        public bool? DontAllowNegativeStock { get; set; } // bit, null
        public decimal? RandTrustPerc { get; set; } // decimal(5,2), null
        public bool? ShowJCDate { get; set; } // bit, null
        public int? InternalCostTypeID { get; set; } // int, null
        public bool? LabourRequired { get; set; } // bit, null
        [MaxLength(8)]
        [StringLength(8)]
        public string GLSundryAccount { get; set; } // nvarchar(8), null
        public bool? EditLabourDesc { get; set; } // bit, null
        public bool? CompanyFirst { get; set; } // bit, null
        public bool? DisplayRMILogo { get; set; } // bit, null
        public bool? PaymentTypeReminder { get; set; } // bit, null
        [MaxLength(8)]
        [StringLength(8)]
        public string TimerStartStop { get; set; } // nvarchar(8), null
        public bool? BarcodeScanner { get; set; } // bit, null
        public bool? MultiBranch { get; set; } // bit, null
        public bool? WorkshopLite { get; set; } // bit, null
        public decimal? Consumable_Perc { get; set; } // decimal(4,1), null
        [MaxLength(40)]
        [StringLength(40)]
        public string server_address_auto { get; set; } // nvarchar(40), null
        [MaxLength(40)]
        [StringLength(40)]
        public string auto_clientid { get; set; } // nvarchar(40), null
        [MaxLength(8)]
        [StringLength(8)]
        public string Fuel_gl_account { get; set; } // nvarchar(8), null
        public bool? CRM { get; set; } // bit, null
        public bool? new_selling_price_required { get; set; } // bit, null
        public decimal? Consumable_fixed_price { get; set; } // decimal(18,2), null
        public bool? calculate_New_Selling_Price { get; set; } // bit, null
        public bool? useSuppInvRefGL { get; set; } // bit, null
        public bool? enableTBImport { get; set; } // bit, null
        public bool? T20Taxi { get; set; } // bit, null
        public int? SANTACONationalID { get; set; } // int, null
        public int? NationalTaxiAssociationID { get; set; } // int, null
        public bool? daily_task_email { get; set; } // bit, null
        public bool? reqKMout { get; set; } // bit, null
        [MaxLength(100)]
        [StringLength(100)]
        public string defaultAdvertEmail { get; set; } // varchar(100), null
        [MaxLength(200)]
        [StringLength(200)]
        public string VMGStatisticsDBAddress { get; set; } // varchar(200), null
        [MaxLength(5)]
        [StringLength(5)]
        public string VMGStatisticsDBPort { get; set; } // varchar(5), null
        [MaxLength(50)]
        [StringLength(50)]
        public string VMGStatisticsDBUsername { get; set; } // varchar(50), null
        [MaxLength(50)]
        [StringLength(50)]
        public string VMGStatisticsDBPassword { get; set; } // varchar(50), null
        [MaxLength(50)]
        [StringLength(50)]
        public string VMGStatisticsDBName { get; set; } // varchar(50), null
        public bool? VMGStatisticsEnabled { get; set; } // bit, null
        [MaxLength(200)]
        [StringLength(200)]
        public string TransunionServicesEndpoint { get; set; } // varchar(200), null
        [MaxLength(200)]
        [StringLength(200)]
        public string TUVehicleDataDBAddress { get; set; } // varchar(200), null
        [MaxLength(5)]
        [StringLength(5)]
        public string TUVehicleDataDBPort { get; set; } // varchar(5), null
        [MaxLength(50)]
        [StringLength(50)]
        public string TUVehicleDataDBUsername { get; set; } // varchar(50), null
        [MaxLength(50)]
        [StringLength(50)]
        public string TUVehicleDataDBPassword { get; set; } // varchar(50), null
        [MaxLength(50)]
        [StringLength(50)]
        public string TUVehicleDataDBName { get; set; } // varchar(50), null
        public bool? TUVehicleDataEnabled { get; set; } // bit, null
        public DateTime? TUVehicleDataLastUpdated { get; set; } // datetime, null
        public bool? TUVehicleDataUpdateInProgress { get; set; } // bit, null
        public bool? RMISundry { get; set; } // bit, null
        public bool? OrderPrint { get; set; } // bit, null
        public bool? P2POTPPack { get; set; } // bit, null
        public DateTime? TUVehicleDataUpdateInProgressTime { get; set; } // datetime, null
        public int? last_client_no { get; set; } // int, null
        [MaxLength(200)]
        [StringLength(200)]
        public string MobileServiceUrl { get; set; } // varchar(200), null
        [MaxLength(100)]
        [StringLength(100)]
        public string MobileUsername { get; set; } // varchar(100), null
        [MaxLength(100)]
        [StringLength(100)]
        public string MobilePassword { get; set; } // varchar(100), null
        [MaxLength(100)]
        [StringLength(100)]
        public string MobileBranchGuid { get; set; } // varchar(100), null
        public bool? VmgMobileEnabled { get; set; } // bit, null
        [MaxLength(10)]
        [StringLength(10)]
        public string DMS_Version { get; set; } // nvarchar(10), null
        public bool? sendSMSToAutoAssignedEmployee { get; set; } // bit, null
        [MaxLength(200)]
        [StringLength(200)]
        public string TransunionVehicleInformationUrl { get; set; } // nvarchar(200), null
        public bool? TransunionVehicleInformationEnabled { get; set; } // bit, null
        public bool? use_last_selling_price { get; set; } // bit, null
    }
}
