﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMGSoftware.DataSDK.OnlineStockData.Entities.Service
{
    [Table("service_checkins")]
    public class ServiceCheckin
    {
        [Key]
        public int id { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime date_checked_in{ get; set;}
        public string computer_name { get; set; }
        public string service_version { get; set; }

        public CompanyConfig company_config { get; set; }
    }
}
