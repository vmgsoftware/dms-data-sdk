﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMGSoftware.DataSDK.DmsData.Stock.Entities
{
    [Dapper.Contrib.Extensions.Table("tblStockExtras")]
    public class StockExtras
    {
        public StockExtras()
        {
            this.Deleted = false;
        }
        [Dapper.Contrib.Extensions.Key]
        public int StockId { get; set; } // int, not null
        public string ExtraListString { get; set; } // varchar(2000), null
        public string ExtraIdString { get; set; } // varchar(1000), null
        public int? CompanyId { get; set; } // int, null
        public bool? Deleted { get; set; } // bit, null
        public string BodyType { get; set; } // varchar(50), null
    }
}
