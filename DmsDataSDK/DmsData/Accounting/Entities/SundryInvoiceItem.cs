﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Accounting.Entities
{
    public class SundryInvoiceItem
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "SundryItemID is required")]
        public int SundryItemID { get; set; }
        [MaxLength(3)]
        public string MakeId { get; set; }
        public int ModelId { get; set; }
        [MaxLength(50)]
        public string RegNr { get; set; }
        public decimal Amount { get; set; }
        public decimal VAT { get; set; }
        public decimal Total { get; set; }
        public DateTime DateCreated { get; set; }
        public int SundryInvoiceID { get; set; }
        public int SundryItemType { get; set; }
        public bool Deleted { get; set; }
        public bool GL { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        [MaxLength(30)]
        public string EngineNo { get; set; }
        [MaxLength(30)]
        public string ChassisNo { get; set; }
    }
}
