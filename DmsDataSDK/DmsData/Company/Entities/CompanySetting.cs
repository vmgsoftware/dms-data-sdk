﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Company.Entities
{
    [Dapper.Contrib.Extensions.Table("tblCompanySettings")]
    public class CompanySetting
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Settings Id is required")]
        public int SettingsId { get; set; } // int, not null
        [MaxLength(300)]
        [Required(ErrorMessage = "Key is required")]
        public string Key { get; set; } // nvarchar(300), not null
        [MaxLength(255)]
        [Required(ErrorMessage = "Value is required")]
        public string Value { get; set; } // nvarchar(255), not null
        [MaxLength(50)]
        public string Linked { get; set; } // nvarchar(50), null
    }
}
