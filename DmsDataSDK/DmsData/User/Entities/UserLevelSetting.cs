﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.User.Entities
{
    [Dapper.Contrib.Extensions.Table("tblUserLevelSettings")]
    public class UserLevelSetting
    {
        

        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Setting Id is required")]
        public int SettingId { get; set; } // int, not null
        [Required(ErrorMessage = "User Level Id is required")]
        public int UserLevelId { get; set; } // int, not null
        [MaxLength(1000)]
        [StringLength(1000)]
        [Required(ErrorMessage = "Key is required")]
        public string Key { get; set; } // nvarchar(1000), not null
        [MaxLength]
        [Required(ErrorMessage = "value is required")]
        public string value { get; set; } // nvarchar(max), not null

    }
}