﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using VMGAuto.Data;

namespace VMGSoftware.DataSDK.DmsData.Company.Repositories
{
    public class CompanySettingsRepository : GenericRepository
    {
        public static Entities.CompanySetting LoadByKey(string key, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                return sqlConnection.QueryFirstOrDefault<Entities.CompanySetting>($"SELECT * FROM tblCompanySettings WHERE [Key] = '{key}'");
            }
        }

        public static Dictionary<string, Entities.CompanySetting> LoadByLinked(string linkedName,SqlConnection sqlConnection)
        {
            Dictionary<string, Entities.CompanySetting> settingKeyValueDictionary = new Dictionary<string, Entities.CompanySetting>();

            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                var userSettingList = sqlConnection.Query<Entities.CompanySetting>($"SELECT * FROM tblCompanySettings WHERE [Linked] = '{linkedName}'");
                foreach (Entities.CompanySetting setting in userSettingList)
                {
                    settingKeyValueDictionary.Add(setting.Key, setting);
                }
            }
            return settingKeyValueDictionary;
        }

        public static void SaveDictionarySettings(Dictionary<string, Entities.CompanySetting> settingsDictionary, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                for (int i = 0; i < settingsDictionary.Count; i++)
                {
                    GenericRepository.Update(settingsDictionary.Values.ElementAt(i), sqlConnection);
                }
            }

        }
    }
}
