﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Stock.Entities
{
    [Dapper.Contrib.Extensions.Table("tblIdentifiedBy")]
    public class IdentityType
    {
        public IdentityType()
        {
            this.TblStocks = new List<StockBasic>();
        }

        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "id is required")]
        public int id { get; set; } // int, not null
        [MaxLength(50)]
        public string IdentifiedBy { get; set; } // nchar(50), null

        public List<StockBasic> TblStocks { get; set; }
    }
}