﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arebis.Logging.GrayLog;

namespace VMGSoftware.DataSDK.Temp_Logging
{
    public static class Logger
    {
        public static int GetLineNumber(Exception ex)
        {
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch);
            if (index != -1)
            {
                var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber))
                {
                }
            }
            return lineNumber;
        }

        public static void LogExceptionToGraylog(Exception exception)
        {
            try
            {
                var st = new StackTrace(exception, true);
                var debugInfo = "";
                try
                {
                    debugInfo += "Line: " + st.GetFrame(0).GetFileLineNumber() + "\n";
                }
                catch (Exception )
                { 
                }
                try
                {
                    debugInfo += "Method: " + st.GetFrame(0).GetMethod() + "\n";
                }
                catch (Exception)
                {
                }
                try
                {
                    debugInfo += "file: " + st.GetFrame(0).GetFileName() + "\n";
                }
                catch (Exception)
                {
                }

                using (var logger = new GrayLogUdpClient("DmsDataSdk", "logging.vmgdms.com"))
                {
                    logger.Send(exception.Message, exception.ToString(),
                        new
                        {
                            computer = Environment.MachineName,
                            debugInfo,
                            lineNumber = GetLineNumber(exception),
                            stack = exception.StackTrace,
                            innerException = exception.InnerException?.ToString()

                        });
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
