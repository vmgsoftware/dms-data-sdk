﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Stock.Entities
{
    [Dapper.Contrib.Extensions.Table("tblStock")]
    public class StockBasic
    {

        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Stock ID is required")]
        public int StockID { get; set; } // int, not null
        [MaxLength(14)]
        public string StockCode { get; set; } // nvarchar(14), null
        [MaxLength(3)]
        public string MakeID { get; set; } // nvarchar(3), null
        public int? ModelID { get; set; } // int, null
        public DateTime? PurchaseDate { get; set; } // smalldatetime, null
        public decimal? PurchasePrice { get; set; } // money, null
        public decimal? SellingPrice { get; set; } // money, null
        public decimal? Year { get; set; } // numeric(4,0), null
       
        public int? ColourID { get; set; } // int, null

        //0 - Nothing
        //1 - Upload
        //2 - On Web
        //3 - Remove
        //4 - On Web + Upload
        //5 - On Web + Remove
        //6 - Hide in web management

        public int? WebFlag { get; set; } // int, null
    
        public int? companyID { get; set; } // int, null
     
        public DateTime? LicenseDiscExpiryDate { get; set; }
    }
}
