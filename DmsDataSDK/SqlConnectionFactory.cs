﻿using System.Data.SqlClient;

namespace VMGSoftware.DataSDK
{
    public class SqlConnectionFactory
    {
        public string ConnectionString { get; }

        public SqlConnectionFactory(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public SqlConnection CreateAndOpenSqlConnection()
        {
            SqlConnection sqlConnection = new SqlConnection(ConnectionString);
            sqlConnection.Open();
            return sqlConnection;
        }
    }
}
