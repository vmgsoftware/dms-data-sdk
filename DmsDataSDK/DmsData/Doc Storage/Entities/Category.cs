﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Doc_Storage.Entities
{
    [Dapper.Contrib.Extensions.Table("tblDocStorageCategories")]
   public class Category
   {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Category Id is required")]
        public int CategoryId { get; set; } // int, not null
        [MaxLength(500)]
        [StringLength(500)]
        [Required(ErrorMessage = "Category Name is required")]
        public string CategoryName { get; set; } // nvarchar(500), not null
        [MaxLength(1000)]
        [StringLength(1000)]
        [Required(ErrorMessage = "Category Description is required")]
        public string CategoryDescription { get; set; } // nvarchar(1000), not null
        [Required(ErrorMessage = "Is Free Form Category is required")]
        public bool IsFreeFormCategory { get; set; } // bit, not null

    }
}
