﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Products;
using VMGSoftware.DataSDK.OnlineStockData.Migrations;

namespace VMGSoftware.DataSDK.OnlineStockData.Repositories
{
    public class CompanyProductRepo
    {
        public static void UploadProducts(int companyId,string localConnectionString,StockFeedDataContext dbContext)
        {
            using (var sqlConnection = new SqlConnection(localConnectionString))
            {
                sqlConnection.Open();
                var productMapings = dbContext.ProductMappings.ToArray();
                foreach (var productMapping in productMapings)
                {
                    var currentValue = sqlConnection.QueryFirstOrDefault<string>(productMapping.local_sql_query);
                    bool isActive;
                    if (currentValue == null)
                    {
                        isActive = false;
                    }
                    else
                    {
                        if (currentValue == "1")
                            currentValue = "true";
                        if (currentValue == "0")
                            currentValue = "false";
                        try
                        {
                            isActive = bool.Parse(currentValue);
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                    }

                    var lastProductAction = dbContext.CompanyProducts.Where(p => p.product.id == productMapping.Product.id && p.company_id == companyId).OrderByDescending(x => x.date_of_change).FirstOrDefault();
                    
                    if (lastProductAction == null || lastProductAction.activated != isActive)
                    {
                        var companyProduct = new CompanyProduct();
                        companyProduct.activated = isActive;
                        companyProduct.product = productMapping.Product;
                        companyProduct.company_id = companyId;
                        dbContext.CompanyProducts.Add(companyProduct);
                    }
                }

                dbContext.SaveChanges();
            }
        }
    }
}
