﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using VMGSoftware.DataSDK.DmsData.Stock.Entities;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Feeds;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Stock;
using VMGSoftware.DataSDK.OnlineStockData.Migrations;

namespace VMGSoftware.DataSDK.OnlineStockData.Mappers
{
    public class StockToStockData
    {
        public static StockData Map(int stockId, string connectionString, StockFeedDataContext dbContext)
        {
            var onlineStockItem = new StockData();
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                var localStockBasicItem = sqlConnection.QueryFirstOrDefault<StockDataToUpload>(
                        $"select * from tblstock where stockid = {stockId}");
                onlineStockItem.stock_id = stockId;
                onlineStockItem.company_id = localStockBasicItem.companyID;

                onlineStockItem.archived = localStockBasicItem.Archived;
                onlineStockItem.date_created = localStockBasicItem.DateCreated;
                onlineStockItem.date_updated = localStockBasicItem.DateUpdated;
                onlineStockItem.last_user = localStockBasicItem.LastUser;

                onlineStockItem.advert = localStockBasicItem.advert;
                onlineStockItem.advert_colour = localStockBasicItem.AdvertColour;
                onlineStockItem.advert_email = localStockBasicItem.advertEmail;
                onlineStockItem.description = localStockBasicItem.Description;

                onlineStockItem.chassis_no = localStockBasicItem.ChassisNo;
                onlineStockItem.engine_no = localStockBasicItem.EngineNo;

                if (localStockBasicItem.ConditionID != null)
                    onlineStockItem.condition = sqlConnection.QueryFirstOrDefault<string>(
                        $"select Description from tblConditions where ConditionID = {localStockBasicItem.ConditionID}");

                if (localStockBasicItem.ColourID != null)
                    onlineStockItem.colour = sqlConnection.QueryFirstOrDefault<string>(
                        $"select Description from tblColors where ColorID = {localStockBasicItem.ColourID}");

                onlineStockItem.company_car = localStockBasicItem.CompanyCar;
                onlineStockItem.enatis_docs = localStockBasicItem.enatisDocs;
                onlineStockItem.consignment = localStockBasicItem.Consignment;
                onlineStockItem.floorplan = localStockBasicItem.floorplan;
                onlineStockItem.spare_key = localStockBasicItem.SpareKey;
                onlineStockItem.service_plan = localStockBasicItem.ServicePlan;
                onlineStockItem.warranty = localStockBasicItem.Warranty;

                onlineStockItem.warranty_date = localStockBasicItem.WarrantyDate;
                onlineStockItem.warranty_km = localStockBasicItem.WarrantyKM;

                onlineStockItem.delivered = localStockBasicItem.Delivered;
                onlineStockItem.deposit_amount = localStockBasicItem.DepositAmount;
                onlineStockItem.deposit_date = localStockBasicItem.DepositDate;

                onlineStockItem.enatis_docs = localStockBasicItem.enatisDocs;

                onlineStockItem.first_payment_date = localStockBasicItem.FirstPaymentDate;
                onlineStockItem.first_reg_date = localStockBasicItem.FirstRegDate;
                onlineStockItem.floor_plan_invoice = localStockBasicItem.FloorPlanInvoice;
                onlineStockItem.floorplan_date = localStockBasicItem.floorPlanDateSettled;
                onlineStockItem.floorplan_date_settled = localStockBasicItem.floorPlanDateSettled;
                onlineStockItem.floorplan_price = localStockBasicItem.floorplanprice;
                onlineStockItem.fueling_fee = localStockBasicItem.Fuelingfee;

                if (localStockBasicItem.floorplanbank != null)
                    onlineStockItem.floorplan_bank_interest = sqlConnection.QueryFirstOrDefault<decimal>(
                        $"select Rate from tblBankInterests where BankInterestID = {localStockBasicItem.floorplanbank}");

                if (localStockBasicItem.fiID != null)
                    onlineStockItem.fi = sqlConnection.QueryFirstOrDefault<string>(
                        $"select FirstName + ' ' + Surname from tblEmployees where EmployeeID = {localStockBasicItem.fiID}");

                onlineStockItem.invoice_custom_note = localStockBasicItem.InvoiceCustomNote;
                if (localStockBasicItem.InvoiceExtraTypeID1 != null)
                    onlineStockItem.invoice_extra1 = sqlConnection.QueryFirstOrDefault<string>(
                        $"select InvoiceExtraType from tblInvoiceExtraTypes where InvoiceExtraTypeID = {localStockBasicItem.InvoiceExtraTypeID1}");
                if (localStockBasicItem.InvoiceExtraTypeID2 != null)
                    onlineStockItem.invoice_extra2 = sqlConnection.QueryFirstOrDefault<string>(
                        $"select InvoiceExtraType from tblInvoiceExtraTypes where InvoiceExtraTypeID = {localStockBasicItem.InvoiceExtraTypeID2}");
                if (localStockBasicItem.InvoiceExtraTypeID3 != null)
                    onlineStockItem.invoice_extra3 = sqlConnection.QueryFirstOrDefault<string>(
                        $"select InvoiceExtraType from tblInvoiceExtraTypes where InvoiceExtraTypeID = {localStockBasicItem.InvoiceExtraTypeID3}");
                if (localStockBasicItem.InvoiceExtraTypeID4 != null)
                    onlineStockItem.invoice_extra4 = sqlConnection.QueryFirstOrDefault<string>(
                        $"select InvoiceExtraType from tblInvoiceExtraTypes where InvoiceExtraTypeID = {localStockBasicItem.InvoiceExtraTypeID4}");
                if (localStockBasicItem.InvoiceExtraTypeID5 != null)
                    onlineStockItem.invoice_extra5 = sqlConnection.QueryFirstOrDefault<string>(
                        $"select InvoiceExtraType from tblInvoiceExtraTypes where InvoiceExtraTypeID = {localStockBasicItem.InvoiceExtraTypeID5}");
                if (localStockBasicItem.InvoiceExtraTypeID6 != null)
                    onlineStockItem.invoice_extra6 = sqlConnection.QueryFirstOrDefault<string>(
                        $"select InvoiceExtraType from tblInvoiceExtraTypes where InvoiceExtraTypeID = {localStockBasicItem.InvoiceExtraTypeID6}");
                if (localStockBasicItem.InvoiceExtraTypeID7 != null)
                    onlineStockItem.invoice_extra7 = sqlConnection.QueryFirstOrDefault<string>(
                        $"select InvoiceExtraType from tblInvoiceExtraTypes where InvoiceExtraTypeID = {localStockBasicItem.InvoiceExtraTypeID7}");
                if (localStockBasicItem.InvoiceExtraTypeID8 != null)
                    onlineStockItem.invoice_extra8 = sqlConnection.QueryFirstOrDefault<string>(
                        $"select InvoiceExtraType from tblInvoiceExtraTypes where InvoiceExtraTypeID = {localStockBasicItem.InvoiceExtraTypeID8}");
                if (localStockBasicItem.InvoiceExtraTypeID9 != null)
                    onlineStockItem.invoice_extra9 = sqlConnection.QueryFirstOrDefault<string>(
                        $"select InvoiceExtraType from tblInvoiceExtraTypes where InvoiceExtraTypeID = {localStockBasicItem.InvoiceExtraTypeID9}");
                if (localStockBasicItem.InvoiceExtraTypeID10 != null)
                    onlineStockItem.invoice_extra10 = sqlConnection.QueryFirstOrDefault<string>(
                        $"select InvoiceExtraType from tblInvoiceExtraTypes where InvoiceExtraTypeID = {localStockBasicItem.InvoiceExtraTypeID10}");

                onlineStockItem.invoice_extra_amt1 = localStockBasicItem.InvoiceExtraAmt1;
                onlineStockItem.invoice_extra_amt2 = localStockBasicItem.InvoiceExtraAmt2;
                onlineStockItem.invoice_extra_amt3 = localStockBasicItem.InvoiceExtraAmt3;
                onlineStockItem.invoice_extra_amt4 = localStockBasicItem.InvoiceExtraAmt4;
                onlineStockItem.invoice_extra_amt5 = localStockBasicItem.InvoiceExtraAmt5;
                onlineStockItem.invoice_extra_amt6 = localStockBasicItem.InvoiceExtraAmt6;
                onlineStockItem.invoice_extra_amt7 = localStockBasicItem.InvoiceExtraAmt7;
                onlineStockItem.invoice_extra_amt8 = localStockBasicItem.InvoiceExtraAmt8;
                onlineStockItem.invoice_extra_amt9 = localStockBasicItem.InvoiceExtraAmt9;
                onlineStockItem.invoice_extra_amt10 = localStockBasicItem.InvoiceExtraAmt10;

                onlineStockItem.invoice_number = localStockBasicItem.InvoiceNumber;
                if (localStockBasicItem.IdentifiedById != null)
                    onlineStockItem.identified_by = sqlConnection.QueryFirstOrDefault<string>(
                        $"select IdentifiedBy from tblIdentifiedBy where id = {localStockBasicItem.IdentifiedById}");
                onlineStockItem.invoice_printed = localStockBasicItem.InvoicePrinted;
                onlineStockItem.is_dealer_sale = localStockBasicItem.isDealerSale;
                onlineStockItem.is_new = localStockBasicItem.New;
                onlineStockItem.is_private_sale = localStockBasicItem.IsPrivateSale;

                onlineStockItem.km_in = sqlConnection.QueryFirstOrDefault<decimal?>(
                    $"select [KM-in] from tblstock where stockId = {stockId}");
                onlineStockItem.km_out = localStockBasicItem.KMout;

                onlineStockItem.last_interest_run = localStockBasicItem.LastInterestRun;
                onlineStockItem.lic_reg_amt = localStockBasicItem.LicRegAmt;
                onlineStockItem.licence_nr = localStockBasicItem.licenceNr;
                onlineStockItem.license_disc_expiry_date = localStockBasicItem.LicenseDiscExpiryDate;
                if (localStockBasicItem.LocationID != null)
                    onlineStockItem.location = sqlConnection.QueryFirstOrDefault<string>(
                        $"select Description from tblLocations where LocationID = {localStockBasicItem.LocationID}");
                if (localStockBasicItem.MakeID != null)
                    onlineStockItem.make =
                        sqlConnection.QueryFirstOrDefault<string>(
                            $"select Make from tblMakes where MakeID = '{localStockBasicItem.MakeID}'");
                if (localStockBasicItem.ModelID != null)
                    onlineStockItem.model =
                        sqlConnection.QueryFirstOrDefault<string>(
                            $"select Variant from tblModels where MMCode = {localStockBasicItem.ModelID}");
                onlineStockItem.mm_code = localStockBasicItem.ModelID;
                onlineStockItem.motor_plan_date = localStockBasicItem.MotorPlanDate;
                onlineStockItem.motor_plan_km = localStockBasicItem.MotorPlanKM;

                onlineStockItem.next_service_date = localStockBasicItem.NextServiceDate;
                onlineStockItem.next_service_km = localStockBasicItem.NextServiceKM;

                onlineStockItem.notes = localStockBasicItem.Notes;
                onlineStockItem.on_hold = localStockBasicItem.OnHold;
                onlineStockItem.on_hold_person = localStockBasicItem.OnHoldPerson;

                onlineStockItem.paid_to_seller = localStockBasicItem.paid_to_seller;
                onlineStockItem.payment_made = localStockBasicItem.PaymentMade;
                onlineStockItem.payment_ref = localStockBasicItem.Paymentref;
                if (localStockBasicItem.PaymentTypeID != null)
                    onlineStockItem.payment_type = sqlConnection.QueryFirstOrDefault<string>(
                        $"select PaymentType from tblPaymentTypes where PaymentTypeID = {localStockBasicItem.PaymentTypeID}");
                if (localStockBasicItem.ProvinceID != null)
                    onlineStockItem.province = sqlConnection.QueryFirstOrDefault<string>(
                        $"select ProvinceName from tblProvinces where ProvinceID = {localStockBasicItem.ProvinceID}");
                onlineStockItem.purchase_date = localStockBasicItem.PurchaseDate;
                onlineStockItem.purchase_excludes_vat = localStockBasicItem.purchaseExcludesVat;
                onlineStockItem.purchase_inv_nr = localStockBasicItem.PurchaseInvNr;
                onlineStockItem.purchase_price = localStockBasicItem.PurchasePrice;
                if (localStockBasicItem.PurchasedByID != null)
                    onlineStockItem.purchased_by = sqlConnection.QueryFirstOrDefault<string>(
                        $"select FirstName + ' ' + Surname from tblEmployees where EmployeeID = {localStockBasicItem.PurchasedByID}");

                onlineStockItem.reconned = localStockBasicItem.Reconned;
                onlineStockItem.retail_price = localStockBasicItem.RetailPrice;
                onlineStockItem.reg_nr = localStockBasicItem.RegNr;

                if (localStockBasicItem.SalesPersonnelID != null)
                    onlineStockItem.sales_personnel = sqlConnection.QueryFirstOrDefault<string>(
                        $"select FirstName + ' ' + Surname from tblEmployees where EmployeeID = {localStockBasicItem.SalesPersonnelID}");
                onlineStockItem.second_gross1 = localStockBasicItem.SecondGross1;
                onlineStockItem.second_gross2 = localStockBasicItem.SecondGross2;
                onlineStockItem.second_gross3 = localStockBasicItem.SecondGross3;
                onlineStockItem.second_gross4 = localStockBasicItem.SecondGross4;
                onlineStockItem.second_gross5 = localStockBasicItem.SecondGross5;
                onlineStockItem.second_gross6 = localStockBasicItem.SecondGross6;
                onlineStockItem.second_gross7 = localStockBasicItem.SecondGross7;
                onlineStockItem.second_gross8 = localStockBasicItem.SecondGross8;
                onlineStockItem.second_gross9 = localStockBasicItem.SecondGross9;
                onlineStockItem.second_gross10 = localStockBasicItem.SecondGross10;
                onlineStockItem.second_gross1_otp = localStockBasicItem.SecondGross1OTP;
                onlineStockItem.second_gross2_otp = localStockBasicItem.SecondGross2OTP;
                onlineStockItem.second_gross3_otp = localStockBasicItem.SecondGross3OTP;
                onlineStockItem.second_gross4_otp = localStockBasicItem.SecondGross4OTP;
                onlineStockItem.second_gross5_otp = localStockBasicItem.SecondGross5OTP;
                onlineStockItem.second_gross6_otp = localStockBasicItem.SecondGross6OTP;
                onlineStockItem.second_gross7_otp = localStockBasicItem.SecondGross7OTP;
                onlineStockItem.second_gross8_otp = localStockBasicItem.SecondGross8OTP;
                if (localStockBasicItem.SalesSourceID != null)
                    onlineStockItem.sales_source = sqlConnection.QueryFirstOrDefault<string>(
                        $"select Description from tblSalesSources where SalesSourceID = {localStockBasicItem.SalesSourceID}");
                onlineStockItem.selling_price = localStockBasicItem.SellingPrice;
                onlineStockItem.selling_price_min = localStockBasicItem.SellingPriceMin;
                onlineStockItem.service_book = localStockBasicItem.ServiceBook;
                onlineStockItem.service_plan_date = localStockBasicItem.ServicePlanDate;
                onlineStockItem.service_plan_km = localStockBasicItem.ServicePlanKM;
                onlineStockItem.settlement_amt = localStockBasicItem.SettlementAmt;
                onlineStockItem.sold_as_used = localStockBasicItem.SoldAsUsed;
                onlineStockItem.sold_date = localStockBasicItem.SoldDate;
                onlineStockItem.sold_price = localStockBasicItem.SoldPrice;
                if (localStockBasicItem.DealerID != null)
                    onlineStockItem.sold_to_dealer = sqlConnection.QueryFirstOrDefault<string>(
                        $"select DealerName from tblDealers where DealerID = {localStockBasicItem.DealerID}");
                onlineStockItem.stock_code = localStockBasicItem.StockCode;
                onlineStockItem.spare_key = localStockBasicItem.SpareKey;
                if (localStockBasicItem.SupplierID != null)
                    onlineStockItem.supplier = sqlConnection.QueryFirstOrDefault<string>(
                        $"select SupplierName from tblSuppliers where SupplierID = {localStockBasicItem.SupplierID}");
                onlineStockItem.stock_report = localStockBasicItem.StockReport;
                onlineStockItem.supplier_invoice_nr = localStockBasicItem.SupplierInvoiceNr;

                onlineStockItem.trade_in_stockid = localStockBasicItem.TradeInStockID;
                onlineStockItem.trade_in_stockid2 = localStockBasicItem.TradeInStockID2;
                onlineStockItem.trade_price = localStockBasicItem.TradePrice;

                onlineStockItem.wholesale = localStockBasicItem.Wholesale;

                onlineStockItem.was_lead = localStockBasicItem.wasLead;

                onlineStockItem.client_name = localStockBasicItem.ClientName;
                onlineStockItem.client_address1 = localStockBasicItem.ClientAddress1;
                onlineStockItem.client_address2 = localStockBasicItem.ClientAddress2;
                onlineStockItem.client_address3 = localStockBasicItem.ClientAddress3;
                onlineStockItem.client_city = localStockBasicItem.ClientCity;
                onlineStockItem.client_code = localStockBasicItem.clientCode;
                onlineStockItem.client_contact_nr = localStockBasicItem.ClientContactNr;
                onlineStockItem.client_contact_nr2 = localStockBasicItem.ClientContactNr2;
                onlineStockItem.client_email = localStockBasicItem.ClientEmail;
                onlineStockItem.client_id_number = localStockBasicItem.ClientIDNr;
                onlineStockItem.client_postal_code = localStockBasicItem.ClientPostalCode;
                onlineStockItem.client_suburb = localStockBasicItem.ClientSuburb;
                onlineStockItem.client_surname = localStockBasicItem.ClientSurname;
                onlineStockItem.client_vat_nr = localStockBasicItem.ClientVATNr;
                if (localStockBasicItem.ClientTitleID != null)
                    onlineStockItem.client_title = sqlConnection.QueryFirstOrDefault<string>(
                        $"select Title from tblTitles where TitleID = {localStockBasicItem.ClientTitleID}");

                if (localStockBasicItem.BankRef != null)
                    onlineStockItem.bank =
                        sqlConnection.QueryFirstOrDefault<string>(
                            $"select BankName from tblBanks where BankID = {localStockBasicItem.BankRef}");
                //if (localStockBasicItem.BranchId != null)
                //    onlineStockItem.bank_branch = sqlConnection.QueryFirstOrDefault<string>(
                //        $"select BranchName from tblBankBranches where BankRef = {localStockBasicItem.BranchId}");

                onlineStockItem.zero_rated = localStockBasicItem.ZeroRated;

                onlineStockItem.floorplan_date = localStockBasicItem.floorplandate;
                onlineStockItem.web_flag = localStockBasicItem.WebFlag;
                onlineStockItem.gl_purchase = localStockBasicItem.glPurchase;
                onlineStockItem.gl_purchase_removed = localStockBasicItem.glPurchaseRemoved;
                onlineStockItem.gl_sale = localStockBasicItem.glSale;
                onlineStockItem.gl_sale_removed = localStockBasicItem.glSaleRemoved;
                onlineStockItem.bank_contract_number = localStockBasicItem.BankContractNumber;
                onlineStockItem.bank_discount_desc = localStockBasicItem.Bankdiscountdesc;
                onlineStockItem.bank_discount_amt = localStockBasicItem.bankdiscountamt;
                onlineStockItem.bank_receipt_date = localStockBasicItem.BankReceiptDate;
                onlineStockItem.selling_price_changed = localStockBasicItem.sellingPriceChanged;
                onlineStockItem.video_url = localStockBasicItem.VideoURL;
                onlineStockItem.mm_retail_price_estimated = localStockBasicItem.MMRetailPriceEstimated;
                onlineStockItem.key_number = localStockBasicItem.KeyNumber;
                onlineStockItem.mm_trade_price = localStockBasicItem.MMTradePrice;
                onlineStockItem.mm_trade_price_estimated = localStockBasicItem.MMTradePriceEstimated;
                onlineStockItem.second_gross11 = localStockBasicItem.SecondGross11;
                onlineStockItem.second_gross12 = localStockBasicItem.SecondGross12;
                onlineStockItem.tu_date_updated = localStockBasicItem.TUDateUpdated;
                onlineStockItem.year = localStockBasicItem.Year;

                if (localStockBasicItem.BranchId != null)
                    onlineStockItem.branch = sqlConnection.QueryFirstOrDefault<string>(
                        $"select Description from tblBranches where BranchId = {localStockBasicItem.BranchId}");

                if (localStockBasicItem.VehicleTypeID != null)
                    onlineStockItem.vehicle_type = sqlConnection.QueryFirstOrDefault<string>(
                        $"select Description from tblVehicleTypes where VehicleTypeID = {localStockBasicItem.VehicleTypeID}");

                onlineStockItem.StockExtraData = StockExtraToStockExtraData.Map(stockId, onlineStockItem, connectionString);
                onlineStockItem.StockImageUrls =
                    sqlConnection.QueryFirstOrDefault<StockImageUrls>(
                        $"select * from tblStockImages WHERE StockID = {stockId}");

                ICollection<StockFeed> stockfeeds = new List<StockFeed>();

                stockfeeds.Add(StockFeedMapper.Map("Swarm", localStockBasicItem.swarm, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("BPFMC", localStockBasicItem.BpfmcWeb, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("Hippo.co.za", localStockBasicItem.hippoCoZa, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("Capitec", localStockBasicItem.capitec, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("RedFeed", localStockBasicItem.redfeed, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("Gumtree", localStockBasicItem.gumtree, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("AllAuto", localStockBasicItem.allauto, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("BlueChip", localStockBasicItem.bluechip, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("Surf4Cars", localStockBasicItem.surf4cars, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("CarFind", localStockBasicItem.carfind, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("Olx", localStockBasicItem.olx, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("Automart", localStockBasicItem.automart, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("AutoDealer", localStockBasicItem.autodealer, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("PriceCheck", localStockBasicItem.pricecheck, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("Cars4Sa", localStockBasicItem.c2 == true ? 3:0, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("Chase", localStockBasicItem.c3 == true ? 3:0, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("e-GoodManners", localStockBasicItem.c4 == true ? 3:0, dbContext));
                stockfeeds.Add(StockFeedMapper.Map("IXOnline", localStockBasicItem.c5 == true ? 3:0, dbContext));

                onlineStockItem.StockFeed = stockfeeds;
            }


            return onlineStockItem;
        }
    }
}