﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper.Contrib.Extensions;

namespace VMGSoftware.DataSDK
{
    /// <summary>
    ///     A repository that works with all models
    /// </summary>
    public class FunctionalGenericRepository
    {
        /// <summary>
        ///     Loads a database row into a entity with the given Id from the database
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="Id">Primary key of row</param>
        /// <param name="sqlConnection">Connection object</param>
        /// <param name="timeout">timeout period in seconds</param>
        /// <returns></returns>
        public static T Load<T>(int Id, IDbConnection sqlConnection, int timeout = 60) where T : class
        {
            return ConnectionHelper.Connect(sqlConnection, sqlCon => sqlCon.Get<T>(Id, commandTimeout: timeout));
        }

        /// <summary>
        ///     Loads all rows from a database table into a list of entities
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="sqlConnection"></param>
        /// <param name="timeout">timeout period in seconds</param>
        /// <returns>List of entity</returns>
        public static List<T> LoadAll<T>(IDbConnection sqlConnection, int timeout = 120) where T : class
        {
            return ConnectionHelper.Connect(sqlConnection,
                sqlCon => sqlCon.GetAll<T>(commandTimeout: timeout).ToList());

        }

        /// <summary>
        ///     Inserts the source object into the database if it doesn't exist, updates the table if the source object already
        ///     exists in it
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="source"></param>
        /// <param name="sqlConnection">connection object</param>
        /// <param name="timeout">timeout period in seconds</param>
        public static void Upsert<T>(T source, IDbConnection sqlConnection, int timeout = 20) where T : class
        {
            var idValue = 0;
            try
            {
                //Dynamically get value of the id property
                var props = typeof(T).GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(KeyAttribute)))
                    .ToList();
                idValue = int.Parse(source.GetType().GetProperty(props[0].Name)?.GetValue(source).ToString());
            }
            catch (Exception ex)
            {
                throw new Exception(
                    "Source doesn't have a property with the Dapper.Contrib.Extensions.Key Attribute: " + ex.Message);
            }

            if (idValue <= 0)
                ConnectionHelper.Connect(sqlConnection, sqlCon => sqlCon.Insert(source, commandTimeout: timeout));
            else
                ConnectionHelper.Connect(sqlConnection, sqlCon => sqlCon.Update(source, commandTimeout: timeout));
        }

        /// <summary>
        ///     Updates the existing row in the database
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="source">source object</param>
        /// <param name="sqlConnection">connection object</param>
        /// <param name="timeout">timeout period in seconds</param>
        public static void Update<T>(T source, IDbConnection sqlConnection, int timeout = 20) where T : class
        {
            ConnectionHelper.Connect(sqlConnection, sqlCon => sqlCon.Update(source, commandTimeout: timeout));
        }

        /// <summary>
        ///     Updates the existing row in the database
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="source">source object</param>
        /// <param name="sqlConnection">connection object</param>
        /// <param name="timeout">timeout period in seconds</param>
        public static int Insert<T>(T source, IDbConnection sqlConnection, int timeout = 20) where T : class
        {
            return ConnectionHelper.Connect(sqlConnection,
                sqlCon => int.Parse(sqlCon.Insert(source, commandTimeout: timeout).ToString()));
        }

        /// <summary>
        ///     Inserts source object into the database
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="source">source object</param>
        /// <param name="sqlConnection">connection object</param>
        /// <param name="timeout">timeout period in seconds</param>
        public static void Delete<T>(T source, IDbConnection sqlConnection, int timeout = 20) where T : class
        {
            ConnectionHelper.Connect(sqlConnection, sqlCon => sqlCon.Delete(source, commandTimeout: timeout));
        }
    }
}