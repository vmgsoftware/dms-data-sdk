namespace VMGSoftware.DataSDK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDatabaseStructure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "company_data_v1.company_configs",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        connection_string = c.String(),
                    })
                .PrimaryKey(t => t.company_id);
            
            CreateTable(
                "company_data_v1.service_checkins",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        date_checked_in = c.DateTime(nullable: false,defaultValueSql:"now()"),
                        computer_name = c.String(),
                        service_version = c.String(),
                        company_config_company_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("company_data_v1.company_configs", t => t.company_config_company_id)
                .Index(t => t.company_config_company_id);
            
            CreateTable(
                "company_data_v1.company_feeds",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        company_id = c.Int(nullable: false),
                        date_of_change = c.DateTime(nullable: false,defaultValueSql:"now()"),
                        enabled = c.Boolean(nullable: false),
                        changed_by_user = c.String(),
                        changed_by_computer = c.String(),
                        feed_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("company_data_v1.feeds", t => t.feed_id)
                .Index(t => t.feed_id);
            
            CreateTable(
                "company_data_v1.feeds",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        feed_name = c.String(),
                        date_added = c.DateTime(nullable: false,defaultValueSql:"now()"),
                        is_active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "company_data_v1.stock_feeds",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        stock_data_id = c.Int(nullable: false),
                        feed_id = c.Int(),
                        feed_upload_state_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("company_data_v1.feeds", t => t.feed_id)
                .ForeignKey("company_data_v1.upload_states", t => t.feed_upload_state_id)
                .ForeignKey("company_data_v1.company_stock_data", t => t.stock_data_id, cascadeDelete: true)
                .Index(t => t.stock_data_id)
                .Index(t => t.feed_id)
                .Index(t => t.feed_upload_state_id);
            
            CreateTable(
                "company_data_v1.upload_states",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        state = c.String(),
                        dms_web_fag = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "company_data_v1.company_products",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        activated = c.Boolean(nullable: false),
                        company_id = c.Int(nullable: false),
                        date_of_change = c.DateTime(nullable: false,defaultValueSql:"now()"),
                        changed_by_user = c.String(),
                        changed_by_computer = c.String(),
                        product_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("company_data_v1.products", t => t.product_id)
                .Index(t => t.product_id);
            
            CreateTable(
                "company_data_v1.products",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "company_data_v1.product_mappings",
                c => new
                    {
                        id = c.Int(nullable: false),
                        local_sql_query = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("company_data_v1.products", t => t.id)
                .Index(t => t.id);
            
            CreateTable(
                "company_data_v1.company_stock_data",
                c => new
                    {
                        stock_data_id = c.Int(nullable: false, identity: true),
                        date_of_upload = c.DateTime(nullable: false,defaultValueSql:"now()"),
                        company_id = c.Int(),
                        stock_id = c.Int(nullable: false),
                        stock_code = c.String(),
                        make = c.String(),
                        model = c.String(),
                        mm_code = c.Int(),
                        purchase_date = c.DateTime(),
                        purchase_price = c.Decimal(precision: 18, scale: 2),
                        selling_price = c.Decimal(precision: 18, scale: 2),
                        bank = c.String(),
                        bank_branch = c.String(),
                        payment_type = c.String(),
                        year = c.Decimal(precision: 18, scale: 2),
                        engine_no = c.String(),
                        chassis_no = c.String(),
                        km_in = c.Decimal(precision: 18, scale: 2),
                        sold_date = c.DateTime(),
                        sold_price = c.Decimal(precision: 18, scale: 2),
                        sales_personnel = c.String(),
                        colour = c.String(),
                        condition = c.String(),
                        reg_nr = c.String(),
                        description = c.String(),
                        notes = c.String(),
                        deposit_amount = c.Decimal(precision: 18, scale: 2),
                        deposit_date = c.DateTime(),
                        vehicle_type = c.String(),
                        client_title = c.String(),
                        client_name = c.String(),
                        client_surname = c.String(),
                        client_contact_nr = c.String(),
                        client_vat_nr = c.String(),
                        trade_in_stockid = c.Int(),
                        supplier = c.String(),
                        location = c.String(),
                        date_created = c.DateTime(),
                        date_updated = c.DateTime(),
                        last_user = c.String(),
                        invoice_extra_amt1 = c.Decimal(precision: 18, scale: 2),
                        invoice_extra_amt2 = c.Decimal(precision: 18, scale: 2),
                        invoice_extra_amt3 = c.Decimal(precision: 18, scale: 2),
                        invoice_extra_amt4 = c.Decimal(precision: 18, scale: 2),
                        invoice_printed = c.Boolean(),
                        lic_reg_amt = c.Decimal(precision: 18, scale: 2),
                        payment_ref = c.String(),
                        supplier_invoice_nr = c.String(),
                        is_private_sale = c.Boolean(),
                        is_dealer_sale = c.Boolean(),
                        sold_to_dealer = c.String(),
                        archived = c.Boolean(),
                        bank_discount_desc = c.String(),
                        bank_discount_amt = c.Decimal(precision: 18, scale: 2),
                        invoice_number = c.String(),
                        bank_receipt_date = c.DateTime(),
                        invoice_extra_amt5 = c.Decimal(precision: 18, scale: 2),
                        invoice_extra_amt6 = c.Decimal(precision: 18, scale: 2),
                        is_new = c.Boolean(),
                        web_flag = c.Int(),
                        fueling_fee = c.Decimal(precision: 18, scale: 2),
                        settlement_amt = c.Decimal(precision: 18, scale: 2),
                        consignment = c.Boolean(),
                        gl_purchase = c.Boolean(),
                        gl_sale = c.Boolean(),
                        trade_in_stockid2 = c.Int(),
                        floorplan_date = c.DateTime(),
                        floorplan_price = c.Decimal(precision: 18, scale: 2),
                        floorplan_bank_interest = c.Decimal(precision: 18, scale: 2),
                        floorplan = c.Boolean(),
                        floorplan_date_settled = c.DateTime(),
                        last_interest_run = c.DateTime(),
                        second_gross1 = c.Boolean(),
                        second_gross2 = c.Boolean(),
                        second_gross3 = c.Boolean(),
                        second_gross4 = c.Boolean(),
                        second_gross5 = c.Boolean(),
                        second_gross6 = c.Boolean(),
                        company_car = c.Boolean(),
                        client_id_number = c.String(),
                        spare_key = c.Boolean(),
                        service_book = c.Boolean(),
                        second_gross1_otp = c.Boolean(),
                        second_gross2_otp = c.Boolean(),
                        second_gross3_otp = c.Boolean(),
                        second_gross4_otp = c.Boolean(),
                        second_gross5_otp = c.Boolean(),
                        second_gross6_otp = c.Boolean(),
                        second_gross7 = c.Boolean(),
                        second_gross8 = c.Boolean(),
                        second_gross7_otp = c.Boolean(),
                        second_gross8_otp = c.Boolean(),
                        invoice_extra1 = c.String(),
                        invoice_extra2 = c.String(),
                        invoice_extra3 = c.String(),
                        invoice_extra4 = c.String(),
                        invoice_extra5 = c.String(),
                        invoice_extra6 = c.String(),
                        payment_made = c.Boolean(),
                        sales_source = c.String(),
                        client_code = c.String(),
                        wholesale = c.Boolean(),
                        gl_purchase_removed = c.Boolean(),
                        gl_sale_removed = c.Boolean(),
                        advert_colour = c.String(),
                        advert = c.String(),
                        advert_email = c.String(),
                        stock_report = c.Boolean(),
                        licence_nr = c.String(),
                        trade_price = c.Decimal(precision: 18, scale: 2),
                        selling_price_changed = c.Boolean(),
                        enatis_docs = c.Boolean(),
                        dealer_2_dealer = c.Boolean(),
                        province = c.String(),
                        selling_price_min = c.Decimal(precision: 18, scale: 2),
                        retail_price = c.Decimal(precision: 18, scale: 2),
                        client_contact_nr2 = c.String(),
                        client_email = c.String(),
                        video_url = c.String(),
                        first_payment_date = c.DateTime(),
                        client_address1 = c.String(),
                        client_address2 = c.String(),
                        client_address3 = c.String(),
                        km_out = c.Decimal(precision: 18, scale: 2),
                        zero_rated = c.Boolean(),
                        mm_trade_price = c.Decimal(precision: 18, scale: 2),
                        fi = c.String(),
                        key_number = c.String(),
                        purchased_by = c.String(),
                        first_reg_date = c.DateTime(),
                        on_hold = c.Boolean(),
                        on_hold_person = c.String(),
                        reconned = c.Boolean(),
                        motor_plan_km = c.Decimal(precision: 18, scale: 2),
                        motor_plan_date = c.DateTime(),
                        delivered = c.Boolean(),
                        invoice_extra_amt7 = c.Decimal(precision: 18, scale: 2),
                        invoice_extra_amt8 = c.Decimal(precision: 18, scale: 2),
                        invoice_extra_amt9 = c.Decimal(precision: 18, scale: 2),
                        invoice_extra_amt10 = c.Decimal(precision: 18, scale: 2),
                        invoice_extra7 = c.String(),
                        invoice_extra8 = c.String(),
                        invoice_extra9 = c.String(),
                        invoice_extra10 = c.String(),
                        second_gross9 = c.Boolean(),
                        second_gross10 = c.Boolean(),
                        second_gross11 = c.Boolean(),
                        second_gross12 = c.Boolean(),
                        purchase_inv_nr = c.String(),
                        tu_date_updated = c.DateTime(),
                        bank_contract_number = c.String(),
                        floor_plan_invoice = c.String(),
                        service_plan = c.Boolean(),
                        service_plan_km = c.Decimal(precision: 18, scale: 2),
                        service_plan_date = c.DateTime(),
                        warranty = c.Boolean(),
                        warranty_km = c.Decimal(precision: 18, scale: 2),
                        warranty_date = c.DateTime(),
                        mm_trade_price_estimated = c.Decimal(precision: 18, scale: 2),
                        mm_retail_price_estimated = c.Decimal(precision: 18, scale: 2),
                        invoice_custom_note = c.String(),
                        next_service_km = c.Decimal(precision: 18, scale: 2),
                        next_service_date = c.DateTime(),
                        identified_by = c.String(),
                        paid_to_seller = c.Decimal(precision: 18, scale: 2),
                        client_suburb = c.String(),
                        client_city = c.String(),
                        client_postal_code = c.String(),
                        purchase_excludes_vat = c.Boolean(),
                        license_disc_expiry_date = c.DateTime(),
                        was_lead = c.Boolean(),
                        branch = c.String(),
                        sold_as_used = c.Boolean(),
                    })
                .PrimaryKey(t => t.stock_data_id);
            
            CreateTable(
                "company_data_v1.stock_extras",
                c => new
                    {
                        stock_data_id = c.Int(nullable: false),
                        body_type = c.String(),
                        extra_list_string = c.String(),
                    })
                .PrimaryKey(t => t.stock_data_id)
                .ForeignKey("company_data_v1.company_stock_data", t => t.stock_data_id)
                .Index(t => t.stock_data_id);
            
            CreateTable(
                "company_data_v1.stock_images",
                c => new
                    {
                        stock_data_id = c.Int(nullable: false),
                        company_id = c.Int(nullable: false),
                        stock_id = c.Int(),
                        image_url1 = c.String(),
                        image_url2 = c.String(),
                        image_url3 = c.String(),
                        image_url4 = c.String(),
                        image_url5 = c.String(),
                        image_url6 = c.String(),
                        image_url7 = c.String(),
                        image_url8 = c.String(),
                        image_url9 = c.String(),
                        image_url10 = c.String(),
                        image_url11 = c.String(),
                        image_url12 = c.String(),
                        image_url13 = c.String(),
                        image_url14 = c.String(),
                        image_url15 = c.String(),
                        image_url16 = c.String(),
                        image_url17 = c.String(),
                        image_url18 = c.String(),
                        image_url19 = c.String(),
                        image_url20 = c.String(),
                    })
                .PrimaryKey(t => t.stock_data_id)
                .ForeignKey("company_data_v1.company_stock_data", t => t.stock_data_id)
                .Index(t => t.stock_data_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("company_data_v1.stock_images", "stock_data_id", "company_data_v1.company_stock_data");
            DropForeignKey("company_data_v1.stock_feeds", "stock_data_id", "company_data_v1.company_stock_data");
            DropForeignKey("company_data_v1.stock_extras", "stock_data_id", "company_data_v1.company_stock_data");
            DropForeignKey("company_data_v1.company_products", "product_id", "company_data_v1.products");
            DropForeignKey("company_data_v1.product_mappings", "id", "company_data_v1.products");
            DropForeignKey("company_data_v1.stock_feeds", "feed_upload_state_id", "company_data_v1.upload_states");
            DropForeignKey("company_data_v1.stock_feeds", "feed_id", "company_data_v1.feeds");
            DropForeignKey("company_data_v1.company_feeds", "feed_id", "company_data_v1.feeds");
            DropForeignKey("company_data_v1.service_checkins", "company_config_company_id", "company_data_v1.company_configs");
            DropIndex("company_data_v1.stock_images", new[] { "stock_data_id" });
            DropIndex("company_data_v1.stock_extras", new[] { "stock_data_id" });
            DropIndex("company_data_v1.product_mappings", new[] { "id" });
            DropIndex("company_data_v1.company_products", new[] { "product_id" });
            DropIndex("company_data_v1.stock_feeds", new[] { "feed_upload_state_id" });
            DropIndex("company_data_v1.stock_feeds", new[] { "feed_id" });
            DropIndex("company_data_v1.stock_feeds", new[] { "stock_data_id" });
            DropIndex("company_data_v1.company_feeds", new[] { "feed_id" });
            DropIndex("company_data_v1.service_checkins", new[] { "company_config_company_id" });
            DropTable("company_data_v1.stock_images");
            DropTable("company_data_v1.stock_extras");
            DropTable("company_data_v1.company_stock_data");
            DropTable("company_data_v1.product_mappings");
            DropTable("company_data_v1.products");
            DropTable("company_data_v1.company_products");
            DropTable("company_data_v1.upload_states");
            DropTable("company_data_v1.stock_feeds");
            DropTable("company_data_v1.feeds");
            DropTable("company_data_v1.company_feeds");
            DropTable("company_data_v1.service_checkins");
            DropTable("company_data_v1.company_configs");
        }
    }
}
