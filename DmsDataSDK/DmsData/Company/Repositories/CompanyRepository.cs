﻿using System.Data;
using System.Data.SqlClient;
using Dapper;
using VMGAuto.Data;

namespace VMGSoftware.DataSDK.DmsData.Company.Repositories
{
    public class CompanyRepository : GenericRepository
    {
        public static Entities.Company GetCurrentCompanyBasicInfo(SqlConnection sqlConnection)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                sqlConnection.Open();
            }
            return sqlConnection.QueryFirstOrDefault<Entities.Company>(
                "SELECT * FROM dbo.tblCompanies JOIN tblCompanyRegistrations ON tblCompanyRegistrations.CompanyID = tblCompanies.CompanyID WHERE tblCompanyRegistrations.CompanyInUse = 1");
        }
    }
}
