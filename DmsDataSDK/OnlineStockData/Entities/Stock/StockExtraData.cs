﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.OnlineStockData.Entities.Stock
{
    [Table("stock_extras")]
    public class StockExtraData
    {
        [Key]
        [ForeignKey("StockData")]
        public int stock_data_id { get; set; }

        public virtual StockData StockData { get; set; }
        public string body_type { get; set; }
        public string extra_list_string { get; set; }
    }
}