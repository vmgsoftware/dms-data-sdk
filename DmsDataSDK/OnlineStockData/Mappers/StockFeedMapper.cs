﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Feeds;
using VMGSoftware.DataSDK.OnlineStockData.Migrations;

namespace VMGSoftware.DataSDK.OnlineStockData.Mappers
{
    public static class StockFeedMapper
    {
        public static StockFeed Map(string feedName, int? dmsWebFlag, StockFeedDataContext dbContext )
        {
            var feed = new StockFeed
            {
                feed = dbContext.Feeds.FirstOrDefault(f => f.feed_name == feedName),
                feed_upload_state = dbContext.FeedUploadStates.FirstOrDefault(x => x.dms_web_fag == dmsWebFlag)

            };
            return feed;
        }
    }
}
