﻿using Dapper;
using System;
using System.Data.SqlClient;
using VMGSoftware.DataSDK.DmsData.Stock.Entities;

namespace VMGSoftware.DataSDK.DmsData.Stock.Repositories
{
    public class StockImageProcessingRepository : GenericRepository
    {
        public static int GetUprocessedStock(string sqlConnectionString)
        {
            using (var sqlConnection = new SqlConnection(sqlConnectionString))
            {
                sqlConnection.Open();

                return sqlConnection.QuerySingleOrDefault<int>("SELECT COUNT(1) FROM dbo.tblStockImageProcessing WHERE ProcessingStatus = 'QUEUED' OR ProcessingStatus = 'MIGRATE' OR (LastUpdated < GETDATE() - 0.1 AND ProcessingStatus = 'BUSY')");

            }
        }


        public static int GetProcessingStock(SqlConnection sqlConnection)
        {
            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                sqlConnection.Open();
            }

            return sqlConnection.QuerySingleOrDefault<int>("SELECT COUNT(1) FROM dbo.tblStockImageProcessing WHERE ProcessingStatus = 'BUSY' AND (LastUpdated > GETDATE() - 0.1)");


        }

        public static int GetUprocessedStock(SqlConnection sqlConnection)
        {
            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                sqlConnection.Open();
            }
            return sqlConnection.QuerySingleOrDefault<int>("SELECT COUNT(1) FROM dbo.tblStockImageProcessing WHERE ProcessingStatus = 'QUEUED' OR ProcessingStatus = 'MIGRATE' OR (LastUpdated < GETDATE() - 0.1 AND ProcessingStatus = 'BUSY')");


        }

        public static StockImageProcessing GetNextUprocessedStock(string sqlConnectionString)
        {
            using (var sqlConnection = new SqlConnection(sqlConnectionString))
            {
                sqlConnection.Open();
                var nextStockItem = sqlConnection.QueryFirstOrDefault<StockImageProcessing>("SELECT TOP 1 * FROM tblStockImageProcessing WHERE ProcessingStatus = 'QUEUED' OR ProcessingStatus = 'MIGRATE' OR (LastUpdated < GETDATE() - 0.1 AND ProcessingStatus = 'BUSY') ORDER BY LastUpdated ASC ");
                nextStockItem.ProcessingStatus = "BUSY";
                nextStockItem.LastUpdated = DateTime.Now;
                Update(nextStockItem, sqlConnection);
                return nextStockItem;
            }
        }

        public static StockImageProcessing GetNextUprocessedStock(SqlConnection sqlConnection)
        {
            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                sqlConnection.Open();
            }
            var nextStockItem = sqlConnection.QueryFirstOrDefault<StockImageProcessing>("SELECT TOP 1 * FROM tblStockImageProcessing WHERE ProcessingStatus = 'QUEUED' OR ProcessingStatus = 'MIGRATE' OR (LastUpdated < GETDATE() - 0.1 AND ProcessingStatus = 'BUSY') ORDER BY LastUpdated ASC ");
            nextStockItem.ProcessingStatus = "BUSY";
            nextStockItem.LastUpdated = DateTime.Now;
            Update(nextStockItem, sqlConnection);
            return nextStockItem;

        }
    }
}
