﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VMGSoftware.DataSDK.LDap.Attributes;

namespace VMGSoftware.DataSDK.DmsData.Stock.Entities
{
    [TableInfo("tblStock")]
    [Dapper.Contrib.Extensions.Table("tblStock")]
    public class StockLite
    {
        [ColumnInfo(true)]
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Stock ID is required")]
        public int StockID { get; set; } // int, not null
        public int? WebFlag { get; set; } // int, null
        public DateTime? LicenseDiscExpiryDate { get; set; }
        public DateTime? ReceivedDate { get; set; }
    }
}
