﻿using System.Data.SqlClient;
using Dapper;
using VMGAuto.Data;
using VMGSoftware.DataSDK.DmsData.Company.Entities;

namespace VMGSoftware.DataSDK.DmsData.Company.Repositories
{
    public class SettingsRegDocsRepository : GenericRepository
    {
        public static SettingsRegDocs LoadSettingsByCompanyId(int companyId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                SettingsRegDocs settingsRegDocs = sqlConnection.QueryFirstOrDefault<SettingsRegDocs>($"SELECT * FROM tblSettingsRegDocs where CompanyId = {companyId}");
                if (settingsRegDocs == null)
                {
                    return sqlConnection.QueryFirstOrDefault<SettingsRegDocs>($"SELECT TOP 1 * FROM tblSettingsRegDocs");
                } else
                {
                    return settingsRegDocs;
                }
            }
        }
    }
}
