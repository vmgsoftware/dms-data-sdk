﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.User.Entities
{
    [Dapper.Contrib.Extensions.Table("tblUserLevels")]
    public class SysUserLevel
    {
        public SysUserLevel()
        {
            this.deleted = false;
        }

        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "User Level ID is required")]
        public int UserLevelID { get; set; } // int, not null
        [MaxLength(30)]
        public string UserLevel { get; set; } // nvarchar(30), null
        public bool? deleted { get; set; } // bit, null
    }
}
