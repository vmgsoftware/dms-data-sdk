﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Auditing.Entities
{
    [Dapper.Contrib.Extensions.Table("tblAuditActionTypes")]
    public class AuditActionTypes
    {

        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Action Type Id is required")]
        public int ActionTypeId { get; set; } // int, not null
        [MaxLength(100)]
        [StringLength(100)]
        [Required(ErrorMessage = "Action is required")]
        public string Action { get; set; } // nvarchar(100), not null

    }

}
