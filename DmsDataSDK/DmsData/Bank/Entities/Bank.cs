﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Bank.Entities
{
    [Dapper.Contrib.Extensions.Table("tblBanks")]
    public class Bank
    {
        public Bank()
        {
            this.VATIncl = false;
            this.Deleted = false;
            this.BankSuburb = "";
            this.BankCity = "";
            this.BankPostalCode = "";
        }

        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Bank ID is required")]
        public int BankID { get; set; } // int, not null
        [MaxLength(100)]
        public string BankName { get; set; } // varchar(100), null
        [MaxLength(20)]
        public string VATNo { get; set; } // nvarchar(20), null
        public bool? VATIncl { get; set; } // bit, null
        public bool? Deleted { get; set; } // bit, null
        [MaxLength(20)]
        public string CompRegNo { get; set; } // nvarchar(20), null
        [MaxLength(50)]
        public string bankAddressLine1 { get; set; } // varchar(50), null
        [MaxLength(50)]
        public string bankAddressLine2 { get; set; } // varchar(50), null
        [MaxLength(50)]
        public string bankAddressLine3 { get; set; } // varchar(50), null
        [MaxLength(25)]
        public string BankSuburb { get; set; } // nvarchar(25), null
        [MaxLength(25)]
        public string BankCity { get; set; } // nvarchar(25), null
        [MaxLength(4)]
        public string BankPostalCode { get; set; } // nvarchar(4), null
    }

}
