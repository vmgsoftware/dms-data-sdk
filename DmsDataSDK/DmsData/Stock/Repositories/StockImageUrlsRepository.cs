﻿using System.Data.SqlClient;
using Dapper;
using VMGAuto.Data;

namespace VMGSoftware.DataSDK.DmsData.Stock.Repositories
{
   public class StockImageUrlsRepository : GenericRepository
    {
        public static Entities.StockImageUrls LoadByStockAndCompanyId(int stockId,int companyId, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                return sqlConnection.QueryFirstOrDefault<Entities.StockImageUrls>($"SELECT * FROM tblStockImages WHERE StockId = {stockId} AND CompanyID = {companyId}");
            }
        }

        public static void SaveByStockAndCompanyId(Entities.StockImageUrls imageUrls, SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                string sqlQuery = "UPDATE tblStockImages SET";
                for (int i = 1; i < 20; i++)
                {
                    sqlQuery += $"[ImageURL{i}] = '{imageUrls.GetType().GetProperty($"ImageURL{i}").GetValue(imageUrls)}', ";
                }
                sqlQuery += $"[ImageURL20] = '{imageUrls.ImageURL20}' WHERE StockId = {imageUrls.StockID} AND CompanyID = {imageUrls.CompanyID}";
                sqlConnection.Execute(sqlQuery);
            }
        }
    }
}
