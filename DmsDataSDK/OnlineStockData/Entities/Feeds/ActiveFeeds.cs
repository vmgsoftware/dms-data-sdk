﻿using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.OnlineStockData.Entities.Feeds
{
    [Dapper.Contrib.Extensions.Table("tblCompanies")]
    public class ActiveFeeds
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Dapper.Contrib.Extensions.Key]
        public int CompanyID { get; set; } // int, not null
        public bool? enablecarscoza { get; set; } // bit, null
        public bool? enablecars4sa { get; set; } // bit, null
        public bool? enablechase { get; set; } // bit, null
        public bool? enableemanners { get; set; } // bit, null
        public bool? enablesurf4cars { get; set; } // bit, null
        public bool? PromptIDADisclosure { get; set; } // bit, null
        public bool? enableixonline { get; set; } // bit, null
        public bool? enablecarfind { get; set; } // bit, null
        public bool? enableautodealer { get; set; } // bit, null
        public bool? enableolx { get; set; } // bit, null
        public bool? enableautomart { get; set; } // bit, null
        public bool? enablebluechip { get; set; } // bit, null
        public bool? enablegumtree { get; set; } // bit, null
        public bool? enableAllAuto { get; set; } // bit, null
        public bool? enableHippoCoZa { get; set; } // bit, null
        public bool? enablecapitec { get; set; } // bit, null
        public bool? enableredfeed { get; set; } // bit, null
        public bool? enableCarborator { get; set; } // bit, null
        public bool? enablePriceCheck { get; set; } // bit, null
        public bool enableswarm { get; set; } // bit, not null
    }

}
