﻿namespace VMGSoftware.DataSDK.GoogleSheetData.Entities
{
    public class ServiceEntry
    {
        [CellInfo('A',true)]
        public string ServiceId { get; set; }
        [CellInfo('F')]
        public string Version { get; set; }
        [CellInfo('G')]
        public string LastVersionCheck { get; set; }
        [CellInfo('D')]
        public string LastCheckIn { get; set; }
        [CellInfo('H')]
        public string ActiveCompanyIds { get; set; }
        [CellInfo('I')]
        public string ComputerName { get; set; }
        [CellInfo('J')]
        public string IP { get; set; }
        [CellInfo('M')]
        public string DevVersion { get; set; }
    }
}
