﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VMGSoftware.DataSDK.DmsData.MMCode.Entities
{
    [Dapper.Contrib.Extensions.Table("tblVehicleType")]
    public class MMCodeData
    {
        [Required]
        [Dapper.Contrib.Extensions.Key]
        public int MMCode { get; set; }
        [MaxLength(5)]
        public string VehicleType { get; set; }
        [MaxLength(30)]
        public string Make { get; set; }
        [MaxLength(60)]
        [Required]
        public string Model { get; set; }
        [MaxLength(70)]
        [Required]
        public string Variant { get; set; }
        public int? LastRegYear { get; set; }
        [MaxLength(5)]
        public string PublicationSection { get; set; }
        [MaxLength(40)]
        [Required]
        public string Master_Model { get; set; }
        public int? Make_Code { get; set; }
        [MaxLength(5)]
        public string MakeID { get; set; }
        public int? Model_Code { get; set; }
        public int? VariantCode { get; set; }
        [MaxLength(5)]
        public string AxleConfiguration { get; set; }
        [MaxLength(5)]
        public string BodyType { get; set; }
        [MaxLength(5)]
        public string NoOfDoors { get; set; }
        [MaxLength(5)]
        public string Drive { get; set; }
        public int? Seats { get; set; }
        [MaxLength(5)]
        public string Use1 { get; set; }
        [MaxLength(5)]
        public string Wheelbase { get; set; }
        [MaxLength(1)]
        public string ManualAuto { get; set; }
        public int? NoGears { get; set; }
        [MaxLength(1)]
        public string Cooling { get; set; }
        public int? CubicCapacity { get; set; }
        [MaxLength(1)]
        public string CylConfiguration { get; set; }
        public int? EngineCycle { get; set; }
        public int? FuelTankSize { get; set; }
        [MaxLength(1)]
        public string FuelType { get; set; }
        public int? Kilowatts { get; set; }
        public int? NoCylinders { get; set; }
        [MaxLength(1)]
        public string TurboOrSuperCharged { get; set; }
        public int? GCM { get; set; }
        public int? GVM { get; set; }
        public int? Tare { get; set; }
        [MaxLength(1)]
        public string Origin { get; set; }
        public int? FrontNoTyres { get; set; }
        [MaxLength(30)]
        public string FrontTyreSize { get; set; }
        public int? RearNoTyres { get; set; }
        [MaxLength(30)]
        public string RearTyreSize { get; set; }
        public DateTime? IntroDate { get; set; }
        public DateTime? DiscDate { get; set; }
        public int? CO2 { get; set; }
        public int? Length { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }
        [MaxLength(3000)]
        public string RegYears { get; set; }
    }
}
