﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper.Contrib.Extensions;
using VMGSoftware.DataSDK.DmsData.Accounting.Entities;

namespace VMGSoftware.DataSDK.DmsData.Accounting.Repositories
{
    public class TaxTypeRepository
    {
        public static List<TaxType> LoadAll(SqlConnection sqlConnection)
        {
            using (sqlConnection)
            {
                var taxTypes = sqlConnection.GetAll<TaxType>().ToList();

                return taxTypes;
            }
        }
    }
}
