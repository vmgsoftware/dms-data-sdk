﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using VMGAuto.Data;
using VMGSoftware.DataSDK;
using VMGSoftware.DataSDK.DmsData.User.Entities;

namespace VMGSoftware.DataSdk.Tests.RepositoryTests
{
    public class GenericRepositoryTest
    {
        private const string connectionString =
            "Data Source=MYSTIQUE\\LMS,54082;Initial Catalog=amgauto;Persist Security Info=True;User ID=amguser;Password=autoAUTO2275!";

        [Test]
        public void TestRepoSql()
        {
           var user = GenericRepository.Load<SysUser>(13, new SqlConnection(connectionString));
           Assert.That(user != null);
        }
    }
}
