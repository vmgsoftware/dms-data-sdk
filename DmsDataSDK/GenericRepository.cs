﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using Dapper.Contrib.Extensions;
using Dapper;
using VMGSoftware.DataSDK.LDap.Logic;

namespace VMGSoftware.DataSDK
{
    /// <summary>
    /// A repository that works with all models
    /// </summary>
    public class GenericRepository
    {
        /// <summary>
        /// Loads a database row into a entity with the given Id from the database 
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="Id">Primary key of row</param>
        /// <param name="sqlConnection">Connection object</param>
        /// <param name="timeout">timeout period in seconds</param>
        /// <returns></returns>
        public static T Load<T>(int Id, SqlConnection sqlConnection,int timeout = 60) where T : class
        {

            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                sqlConnection.Open();
            }

            return sqlConnection.Get<T>(Id,commandTimeout:timeout);

        }

        public static T Load<T>(string whereQuery, SqlConnection sqlConnection,int timeout = 60) where T : class
        {
            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                sqlConnection.Open();
            }
            var tableName = AttributeHelper.GetTableNameDapper(typeof(T));
            return sqlConnection.QueryFirstOrDefault<T>($"SELECT * FROM {tableName} WHERE {whereQuery}");

        }

        public static T LoadOrNew<T>(int Id, SqlConnection sqlConnection,int timeout = 60) where T : class, new()
        {

            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                sqlConnection.Open();
            }
            var loadedEnity = sqlConnection.Get<T>(Id,commandTimeout:timeout);
            if (loadedEnity == null)
            {
                return new T();
            }
            else
            {
                return loadedEnity;
            }
        }

        /// <summary>
        /// Loads all rows from a database table into a list of entities
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="sqlConnection"></param>
        /// <param name="timeout">timeout period in seconds</param>
        /// <returns>List of entity</returns>
        public static List<T> LoadAll<T>(SqlConnection sqlConnection, int timeout = 120) where T : class
        {

            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                sqlConnection.Open();
            }

            return sqlConnection.GetAll<T>(commandTimeout: timeout).ToList();

        }

        /// <summary>
        /// Inserts the source object into the database if it doesn't exist, updates the table if the source object already exists in it
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="source"></param>
        /// <param name="sqlConnection">connection object</param>
        /// <param name="timeout">timeout period in seconds</param>
        public static void Upsert<T>(T source, SqlConnection sqlConnection, int timeout = 20) where T : class
        {
            int idValue = 0;
            try
            {
                //Dynamically get value of the id property
                List<PropertyInfo> props = typeof(T).GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(KeyAttribute))).ToList();
                idValue = int.Parse(source.GetType().GetProperty(props[0].Name).GetValue(source).ToString());
            }
            catch (Exception ex)
            {
                throw new Exception("Source doesn't have a property with the Dapper.Contrib.Extensions.Key Attribute: " + ex.Message);
            }


            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                sqlConnection.Open();
            }

            if (idValue <= 0)
            {
                sqlConnection.Insert<T>(source, commandTimeout: timeout);
            }
            else
            {
                sqlConnection.Update<T>(source, commandTimeout: timeout);
            }

        }

        /// <summary>
        /// Updates the existing row in the database
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="source">source object</param>
        /// <param name="sqlConnection">connection object</param>
        /// <param name="timeout">timeout period in seconds</param>
        public static void Update<T>(T source, SqlConnection sqlConnection, int timeout = 20) where T : class
        {

            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                sqlConnection.Open();
            }

            sqlConnection.Update<T>(source,commandTimeout:timeout);

        }

        /// <summary>
        /// Updates the existing row in the database
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="source">source object</param>
        /// <param name="sqlConnection">connection object</param>
        /// <param name="timeout">timeout period in seconds</param>
        public static int Insert<T>(T source, SqlConnection sqlConnection, int timeout = 20) where T : class
        {

            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                sqlConnection.Open();
            }

            return int.Parse(sqlConnection.Insert<T>(source, commandTimeout: timeout).ToString());

        }
        
        /// <summary>
        /// Inserts source object into the database
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="source">source object</param>
        /// <param name="sqlConnection">connection object</param>
        /// <param name="timeout">timeout period in seconds</param>
        public static void Delete<T>(T source, SqlConnection sqlConnection, int timeout = 20) where T : class
        {

            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                sqlConnection.Open();
            }

            sqlConnection.Delete<T>(source, commandTimeout: timeout);

        }
    }
}
