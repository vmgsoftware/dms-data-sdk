﻿namespace VMGSoftware.DataSDK.LDap.Attributes
{
    public class TableInfo: System.Attribute
    {
        public string TableName;

        public TableInfo(string tableName)
        {
            TableName = tableName;
        }
    }
}
