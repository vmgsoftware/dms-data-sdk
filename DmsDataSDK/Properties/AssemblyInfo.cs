﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("VMGSoftware.DataSDK")]
[assembly: AssemblyDescription("Data objects and classes for working with DMS and related projects")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("VMG Software")]
[assembly: AssemblyProduct("VMGSoftware.DataSDK")]
[assembly: AssemblyCopyright("Copyright ©  2019")]
[assembly: AssemblyTrademark("VMG Software")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("21a654b3-c0d6-4bbd-aed8-eac91149b36f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("5.2.18.0")]
[assembly: AssemblyFileVersion("5.2.18.0")]
