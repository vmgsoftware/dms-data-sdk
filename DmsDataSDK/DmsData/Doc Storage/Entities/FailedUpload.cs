﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Doc_Storage.Entities
{
    [Dapper.Contrib.Extensions.Table("tblDocStorageFailedUploads")]
    public class FailedUpload
    {
        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Error Id is required")]
        public int ErrorId { get; set; } // int, not null
        [MaxLength(50)]
        [StringLength(50)]
        [Required(ErrorMessage = "Action is required")]
        public string Action { get; set; } // nvarchar(50), not null
        [Required(ErrorMessage = "Company Id is required")]
        public int CompanyId { get; set; } // int, not null
        [Required(ErrorMessage = "Stock Id is required")]
        public int StockId { get; set; } // int, not null
        [MaxLength(200)]
        [StringLength(200)]
        [Required(ErrorMessage = "File Name is required")]
        public string FileName { get; set; } // nvarchar(200), not null
        [MaxLength(200)]
        [StringLength(200)]
        [Required(ErrorMessage = "Full File Path is required")]
        public string FullFilePath { get; set; } // nvarchar(200), not null
        [Required(ErrorMessage = "Timestamp is required")]
        public DateTime Timestamp { get; set; } // datetime, not null
        [Required(ErrorMessage = "Error Message is required")]
        public string ErrorMessage { get; set; } // text, not null
        [Required(ErrorMessage = "Error Details is required")]
        public string ErrorDetails { get; set; } // text, not null

    }
}
