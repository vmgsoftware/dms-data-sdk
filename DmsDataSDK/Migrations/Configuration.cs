using System.Data.Entity.Migrations;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Feeds;
using VMGSoftware.DataSDK.OnlineStockData.Entities.Products;
using VMGSoftware.DataSDK.OnlineStockData.Migrations;

namespace VMGSoftware.DataSDK.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<StockFeedDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(StockFeedDataContext context)
        {
            context.Feeds.AddOrUpdate(f => f.feed_name,
                new Feed("Gumtree"),
                new Feed("Cars.co.za"),
                new Feed("Chase"),
                new Feed("e-GoodManners"),
                new Feed("Surf4Cars"),
                new Feed("IXOnline"),
                new Feed("CarFind"),
                new Feed("Automart"),
                new Feed("AllAuto"),
                new Feed("Hippo.co.za"),
                new Feed("Capitec"),
                new Feed("BlueChip"),
                new Feed("RedFeed"),
                new Feed("Carborator"),
                new Feed("PriceCheck"),
                new Feed("Swarm"),
                new Feed("Cars4Sa"),
                new Feed("BPFMC"),
                new Feed("Olx"),
                new Feed("AutoDealer")
            );

            context.FeedUploadStates.AddOrUpdate(s => s.state,
                new FeedUploadState {dms_web_fag = 0, state = "Nothing"},
                new FeedUploadState {dms_web_fag = 1, state = "Upload"},
                new FeedUploadState {dms_web_fag = 2, state = "On Web"},
                new FeedUploadState {dms_web_fag = 3, state = "Remove"},
                new FeedUploadState {dms_web_fag = 4, state = "On Web + Upload"},
                new FeedUploadState {dms_web_fag = 5, state = "On Web + Remove"}
            );

            context.Products.AddOrUpdate(p => p.name,
                new Product
                {
                    name = "Accounting",
                    ProductMaping = new ProductMaping
                    {
                        local_sql_query =
                            "SELECT accounting FROM dbo.tblCompanies WHERE CompanyID = (SELECT companyID FROM dbo.tblStdVar)"
                    }
                },
                new Product
                {
                    name = "CRM",
                    ProductMaping = new ProductMaping {id=2,local_sql_query = "SELECT CRM FROM dbo.tblStdVar"}
                },
                new Product
                {
                    name = "Sold Stats",
                    ProductMaping = new ProductMaping
                        {local_sql_query = "SELECT VMGStatisticsEnabled FROM dbo.tblStdVar"}
                },
                new Product
                {
                    name = "Pastel Integration",
                    ProductMaping = new ProductMaping
                        {local_sql_query = "SELECT Value FROM dbo.tblCompanySettings WHERE [KEY] = 'PastelIntegration'"}
                },
                new Product
                {
                    name = "Transunion Updates",
                    ProductMaping = new ProductMaping
                        {local_sql_query = "SELECT TransunionVehicleInformationEnabled FROM dbo.tblStdVar"}
                },
                new Product
                {
                    name = "Doc Storage",
                    ProductMaping = new ProductMaping
                    {
                        local_sql_query =
                            "SELECT Value FROM dbo.tblCompanySettings WHERE [KEY] = 'isDocumentStorageEnabled'"
                    }
                },
                new Product
                {
                    name = "Mobile v1",
                    ProductMaping = new ProductMaping
                    {
                        local_sql_query =
                            "SELECT CASE WHEN VmgMobileEnabled = 1 AND MobileVersionSelected = 1 THEN 1 ELSE 0 END FROM dbo.tblStdVar"
                    }
                },
                new Product
                {
                    name = "Mobile v2",
                    ProductMaping = new ProductMaping
                    {
                        local_sql_query =
                            "SELECT CASE WHEN VmgMobileEnabled = 1 AND MobileVersionSelected = 2 THEN 1 ELSE 0 END FROM dbo.tblStdVar"
                    }
                },
                new Product
                {
                    name = "Multibranch",
                    ProductMaping = new ProductMaping
                    {
                        local_sql_query =
                            "SELECT enableMultibranch FROM dbo.tblCompanies WHERE CompanyID = (SELECT companyID FROM dbo.tblStdVar)"
                    }
                },
                new Product
                {
                    name = "Workshop",
                    ProductMaping = new ProductMaping
                    {
                        local_sql_query =
                            "SELECT CASE WHEN  ISNULL(LastWSInvoice,'') = '' THEN 0 ELSE 1 END FROM dbo.tblStdVar"
                    }
                });

        }
    }
}