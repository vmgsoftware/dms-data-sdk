﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.Company.Entities
{
    [Dapper.Contrib.Extensions.Table("tblSettingsRegDocs")]
    public class SettingsRegDocs
    {
        public SettingsRegDocs()
        {
            this.TypeOfIdentification = "traffic";
            this.IdentificationNumber = "traffic";
            this.NameOfOrganisation = "company";
            this.EmailAddress = "company";
            this.DayContact = "company";
            this.FaxNumber = "company";
            this.CellNumber = "company";
            this.NoticesServedTo = "street";
            this.IdentificationNumberCustomValue = "";
            this.NameOfOrganisationCustomValue = "";
            this.EmailAddressCustomValue = "";
            this.DayContactCustomValue = "";
            this.FaxNumberCustomValue = "";
            this.CellNumberCustomValue = "";
            this.DayContactCodeCustomValue = "";
            this.FaxCodeCustomValue = "";
        }

        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; } // int, not null
        
        [Required(ErrorMessage = "Company Id is required")]
        public int CompanyId { get; set; } // int, not null
        [MaxLength(50)]
        [Required(ErrorMessage = "Type Of Identification is required")]
        public string TypeOfIdentification { get; set; } // nvarchar(50), not null
        [MaxLength(50)]
        [Required(ErrorMessage = "Identification Number is required")]
        public string IdentificationNumber { get; set; } // nvarchar(50), not null
        [MaxLength(50)]
        [Required(ErrorMessage = "Name Of Organisation is required")]
        public string NameOfOrganisation { get; set; } // nvarchar(50), not null
        [MaxLength(50)]
        [Required(ErrorMessage = "Email Address is required")]
        public string EmailAddress { get; set; } // nvarchar(50), not null
        [MaxLength(50)]
        [Required(ErrorMessage = "Day Contact is required")]
        public string DayContact { get; set; } // nvarchar(50), not null
        [MaxLength(50)]
        [Required(ErrorMessage = "Fax Number is required")]
        public string FaxNumber { get; set; } // nvarchar(50), not null
        [MaxLength(50)]
        [Required(ErrorMessage = "Cell Number is required")]
        public string CellNumber { get; set; } // nvarchar(50), not null
        [MaxLength(50)]
        [Required(ErrorMessage = "Notices Served To is required")]
        public string NoticesServedTo { get; set; } // nvarchar(50), not null
        [MaxLength(50)]
        public string IdentificationNumberCustomValue { get; set; } // nvarchar(50), null
        [MaxLength(50)]
        [Required(ErrorMessage = "Name Of Organisation Custom Value is required")]
        public string NameOfOrganisationCustomValue { get; set; } // nvarchar(50), not null
        [MaxLength(50)]
        public string EmailAddressCustomValue { get; set; } // nvarchar(50), null
        [MaxLength(50)]
        public string DayContactCustomValue { get; set; } // nvarchar(50), null
        [MaxLength(50)]
        public string FaxNumberCustomValue { get; set; } // nvarchar(50), null
        [MaxLength(50)]
        public string CellNumberCustomValue { get; set; } // nvarchar(50), null
        [MaxLength(255)]
        public string DayContactCodeCustomValue { get; set; } // nvarchar(255), null
        [MaxLength(255)]
        public string FaxCodeCustomValue { get; set; } // nvarchar(255), null

        [MaxLength(20)]
        public string BankDetailsSectionNCO { get; set; } // nvarchar(20), null
        
    }

}
