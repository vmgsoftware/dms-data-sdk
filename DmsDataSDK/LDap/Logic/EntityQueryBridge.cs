﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace VMGSoftware.DataSDK.LDap.Logic
{
 public class EntityQueryBridge<T>
    {
        public T Entity;
        public int Id;
        private Type _entityType;
        private string[] _columnNames;
        private object[] _columnValues;
        private string _selectQuery;
        private string _idColumnName;
        private string _tableName;

        public EntityQueryBridge(T entity,int id)
        {
            Entity = entity;
            Id = id;
            _entityType = Entity.GetType();
            _idColumnName = AttributeHelper.GetIdColumnName(entity);
            _tableName = AttributeHelper.GetTableName(entity);
            GetColumnNames();
        }

        private void GetColumnNames()
        {
            var properties = _entityType.GetProperties();
            int numberOfColumns = properties.Length;
            
            _columnNames = new string[numberOfColumns];

            for (int i = 0; i < numberOfColumns; i++)
            {
                _columnNames[i] = properties[i].Name;
            }
        }

        public string BuildSelectQuery()
        {
            string query = "SELECT ";

            foreach (var columnName in _columnNames)
            {
                query += $" {columnName},";
            }
            query = query.Remove(query.Length - 1);

            query += $" FROM {_tableName} WHERE {_idColumnName} = {Id}";
            return query;
        }

        public void PopulateEntity(SqlConnection sqlConnection)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                sqlConnection.Open();
            }
            var dataTable = new DataTable(_tableName);
            using (dataTable)
            {
                using (var dataAdapter = new SqlDataAdapter(BuildSelectQuery(), sqlConnection))
                {
                    dataAdapter.Fill(dataTable);
                }
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    ReflectionHelper.AssignValueOfPropertyToObject(Entity, _columnNames[i] ,dataTable.Rows[0][i]);
                }
            }
        }

        public void PopulateEntityReader(SqlConnection sqlConnection)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                sqlConnection.Open();
            }

            using (var sqlCommand = new SqlCommand(BuildSelectQuery(),sqlConnection))
            {
                using (var reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        for (int i = 0; i < _columnNames.Length; i++)
                        {

                            ReflectionHelper.AssignValueOfPropertyToObject(Entity, _columnNames[i], reader.GetValue(i));

                        }
                    }

                }
            }
        }


    }
}
