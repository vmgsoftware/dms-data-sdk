﻿using System.ComponentModel.DataAnnotations;

namespace VMGSoftware.DataSDK.DmsData.MMCode.Entities
{
    [Dapper.Contrib.Extensions.Table("tblvehicletypes")]
    public class VehicleType
    {
        [MaxLength(1)]
        [Required]
        [Dapper.Contrib.Extensions.Key]
        public string VehicleTypeID { get; set; }
        [MaxLength(20)]
        public string Description { get; set; }
    }
}
