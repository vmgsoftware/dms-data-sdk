﻿using System;

namespace VMGSoftware.DataSDK.LDap.Logic
{
    public static class ReflectionHelper
    {
        /// <summary>
        ///     Dynamically assigns a given value of a given type to a object's property
        ///     EXAMPLE
        ///     -------
        ///     Say you want to assign the integer value of 22 to the Age property of a Person object.
        ///     ReflectionHelpers.AssignValueOfPropertyToObject<int>(PersonObject,"Age",22);
        /// </summary>
        /// <typeparam name="T">Type of the given value for the property</typeparam>
        /// <param name="objectContainingProperty">object that has the property you wish to change the value of</param>
        /// <param name="propertyName">The string name of the property</param>
        /// <param name="value">Value to assign to the property</param>
        public static void AssignValueOfPropertyToObject<T>(object objectContainingProperty, string propertyName,
            T value)
        {
            if (value != null && value.GetType() != typeof(DBNull))
            {
                objectContainingProperty.GetType().GetProperty(propertyName)
                    ?.SetValue(objectContainingProperty, value, null);
            }

        }

        /// <summary>
        ///     Dynamically retrieves the value of property.
        ///     EXAMPLE
        ///     -------
        ///     If you have a Person Object with a integer Age property then the following code will return it's value
        ///     int age = ReflectionHelpers.GetValueOfProperty<int>(PersonObject,"Age");
        /// </summary>
        /// <typeparam name="T">Type of property</typeparam>
        /// <param name="objectContainingProperty">object that has the property</param>
        /// <param name="propertyName">String name of the property</param>
        /// <returns></returns>
        public static T GetValueOfProperty<T>(object objectContainingProperty, string propertyName)
        {
            return (T) objectContainingProperty.GetType().GetProperty(propertyName)?.GetValue(objectContainingProperty);
        }
    }
}
