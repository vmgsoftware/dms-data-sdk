﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMGSoftware.DataSDK.LDap.Logic;

namespace VMGSoftware.DataSDK.LDap
{
    public class LGenericRepository
    {
        /// <summary>
        /// Loads a database row into a entity with the given Id from the database 
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="Id">Primary key of row</param>
        /// <param name="sqlConnection">Connection object</param>
        /// <param name="timeout">timeout period in seconds</param>
        /// <returns></returns>
        public static T FastLoad<T>(int Id, SqlConnection sqlConnection,int timeout = 60) where T : class, new() 
        {

            var entity = new T();
            using (sqlConnection)
            {
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                EntityQueryBridge<T> builder = new EntityQueryBridge<T>(entity,Id);
                builder.PopulateEntityReader(sqlConnection);
                return entity;
            }

        }

    }
}
