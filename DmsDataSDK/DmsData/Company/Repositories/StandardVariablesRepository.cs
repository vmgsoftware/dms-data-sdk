﻿using System.Data.SqlClient;
using Dapper;
using Dapper.Contrib.Extensions;
using VMGAuto.Data;

namespace VMGSoftware.DataSDK.DmsData.Company.Repositories
{
    public class StandardVariablesRepository: GenericRepository
    {
        public static int GetCompanyId(string localSqlConnectioNString)
        {
            using (var sqlConnection = new SqlConnection(localSqlConnectioNString))
            {
                sqlConnection.Open();
                return sqlConnection.QueryFirstOrDefault<int>("SELECT companyID FROM dbo.tblStdVar");
            }
        }
    }
}
