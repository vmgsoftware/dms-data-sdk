﻿using System.Collections.Generic;
using Google.Apis.Sheets.v4;

namespace VMGSoftware.DataSDK.GoogleSheetData.Repositories
{
    public class SheetInteraction
    {
        private string _sheetId;
        protected readonly SheetsService _service;
       
        public SheetInteraction(string sheetId, SheetsService service )
        {
            _sheetId = sheetId;
            _service = service;
        }

        public SheetInteraction(string sheetId, string clientId,string clientSecret)
        {
            _sheetId = sheetId;
            _service = AppClient.AuthorizeGoogleApp(clientId,clientSecret);
        }

        public SheetInteraction(string sheetId, string clientId,string clientSecret,string refreshToken)
        {
            _sheetId = sheetId;
            _service = AppClient.AuthorizeGoogleApp(clientId,clientSecret,refreshToken);
        }

        public SheetInteraction(string sheetId)
        {
            _sheetId = sheetId;
            _service = AppClient.AuthorizeGoogleApp();
        }

        public void AddRow(IList<object> rowData, string range)
        {
            var data = new List<IList<object>>(){rowData};
            AppClient.AppendGoogleSheetinBatch(data,_sheetId,range,_service);
        }

        public void UpdateRow(IList<object> rowData, string range)
        {
            var data = new List<IList<object>>(){rowData};
            AppClient.UpdateGoogleSheetinBatch(data,_sheetId,range,_service);
        }

        public void UpdateCell(object value, char columnLetter, int rowNumber)
        {
            string range = $"{columnLetter}{rowNumber}:{columnLetter}";
            UpdateRow(new List<object>() {value}, range);
        }

        public IList<object> GetRow(int rowNumber)
        {
            string range = $"A{rowNumber}:Z";
            var values = AppClient.GetValuesFromSheet(_sheetId, range, _service);
            return values[0];
        }

        public IList<object> GetColumn(char columnLetter,int fromRowNumber)
        {
            string range = $"{columnLetter}{fromRowNumber}:{columnLetter}";
            var values = AppClient.GetValuesFromSheet(_sheetId, range, _service);
            var toReturn = new List<object>();
            foreach (var row in values)
            {
                toReturn.Add(row?.Count > 0 ? row[0] : null);
            }
            return toReturn;
        }

        public object GetCellValue(char columnLetter,int rowNumber)
        {
            try
            {
                string range = $"{columnLetter}{rowNumber}:{columnLetter}";
                var values = AppClient.GetValuesFromSheet(_sheetId, range, _service);
                return values[0][0];
            }
            catch
            {
                return null;
            }
        }
    }
}
