﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VMGSoftware.DataSDK.DmsData.User.Entities
{
    [Dapper.Contrib.Extensions.Table("tblSysUsers")]
    public class SysUser
    {
        public SysUser()
        {
            this.Deleted = false;
            this.DateCreated = DateTime.Now;
            this.Hidden = false;
            this.Parts = false;
            this.ReOrderLevel = false;
            this.WIPOrders = false;
            this.ProcessOrders = false;
            this.ImportedOrders = false;
            this.ImportedReciepts = false;
            this.Workshop = false;
            this.CounterSales = false;
            this.CreditNotes = false;
            this.Authorised = false;
            this.Suppliers = false;
            this.Clients = false;
            this.Employees = false;
            this.SysUsers = false;
            this.Company = false;
            this.Settings = false;
            this.StockTake = false;
            this.Reports = false;
            this.AddEditParts = false;
            this.StockMovements = false;
            this.StockReceipts = false;
            this.StockAdjustments = false;
            this.AuthLocalPurchaseOrders = false;
            this.AuthImportedOrders = false;
            this.paymentColour = "250250205";
            this.receiptColour = "240248255";
            this.showNotification = false;
            this.BackupDays = 0;
            this.WebSales = false;
            this.sublet = false;
            this.StockReturn = false;
            this.Orders = false;
            this.SystemCheck = false;
            this.SendSMS = false;
            this.DeleteOrderFromJC = false;
            this.ShowPL = false;
            this.BranchManager = false;
            this.HeadOffice = false;
            this.JCClientVehicleDuplication = false;
            this.MakesModels = false;
            this.Kits = false;
            this.DeleteLabour = false;
            this.AllBranches = false;
            this.FuelReceipts = false;
            this.disableCRMNotification = false;
            this.cant_delete_jc = false;
            this.VmgMobileRegistered = false;
            this.MobileUserId = "";
        }

        [Dapper.Contrib.Extensions.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Sys User ID is required")]
        public int SysUserID { get; set; } // int, not null
        [MaxLength(50)]
        public string UserName { get; set; } // nvarchar(50), null
        [MaxLength(50)]
        public string Password { get; set; } // nvarchar(50), null
        public int? UserLevelID { get; set; } // int, null
        public bool? Deleted { get; set; } // bit, null
        public DateTime? DateCreated { get; set; } // datetime, null
        public DateTime? DateUpdated { get; set; } // datetime, null
        public int? LastUserID { get; set; } // int, null
        public bool? Hidden { get; set; } // bit, null
        public bool? Parts { get; set; } // bit, null
        public bool? ReOrderLevel { get; set; } // bit, null
        public bool? WIPOrders { get; set; } // bit, null
        public bool? ProcessOrders { get; set; } // bit, null
        public bool? ImportedOrders { get; set; } // bit, null
        public bool? ImportedReciepts { get; set; } // bit, null
        public bool? Workshop { get; set; } // bit, null
        public bool? CounterSales { get; set; } // bit, null
        public bool? CreditNotes { get; set; } // bit, null
        public bool? Authorised { get; set; } // bit, null
        public bool? Suppliers { get; set; } // bit, null
        public bool? Clients { get; set; } // bit, null
        public bool? Employees { get; set; } // bit, null
        public bool? SysUsers { get; set; } // bit, null
        public bool? Company { get; set; } // bit, null
        public bool? Settings { get; set; } // bit, null
        public bool? StockTake { get; set; } // bit, null
        public bool? Reports { get; set; } // bit, null
        public bool? AddEditParts { get; set; } // bit, null
        public bool? StockMovements { get; set; } // bit, null
        public bool? StockReceipts { get; set; } // bit, null
        public bool? StockAdjustments { get; set; } // bit, null
        public bool? AuthLocalPurchaseOrders { get; set; } // bit, null
        public bool? AuthImportedOrders { get; set; } // bit, null
        public int? LastCB { get; set; } // int, null
        [MaxLength(9)]
        public string paymentColour { get; set; } // varchar(9), null
        [MaxLength(9)]
        public string receiptColour { get; set; } // varchar(9), null
        public bool? showNotification { get; set; } // bit, null
        public int? BackupDays { get; set; } // int, null
        public bool? WebSales { get; set; } // bit, null
        public bool? sublet { get; set; } // bit, null
        public bool? StockReturn { get; set; } // bit, null
        public bool? Orders { get; set; } // bit, null
        public bool? SystemCheck { get; set; } // bit, null
        public bool? SendSMS { get; set; } // bit, null
        public bool? DeleteOrderFromJC { get; set; } // bit, null
        public bool? ShowPL { get; set; } // bit, null
        public bool? BranchManager { get; set; } // bit, null
        public bool? HeadOffice { get; set; } // bit, null
        public decimal? CreditLimit { get; set; } // money, null
        public int? EmployeeID { get; set; } // int, null
        public int? RefreshRate { get; set; } // int, null
        public decimal? MaxDiscount { get; set; } // decimal(5,2), null
        public int? CommsRefreshRate { get; set; } // int, null
        public bool? JCClientVehicleDuplication { get; set; } // bit, null
        public bool? MakesModels { get; set; } // bit, null
        public bool? Kits { get; set; } // bit, null
        public bool? DeleteLabour { get; set; } // bit, null
        public int? DefaultBranchID { get; set; } // int, null
        public bool? AllBranches { get; set; } // bit, null
        public bool? FuelReceipts { get; set; } // bit, null
        public bool? disableCRMNotification { get; set; } // bit, null
        public bool? cant_delete_jc { get; set; } // bit, null
        public bool? VmgMobileRegistered { get; set; } // bit, null
        public string MobileUserId { get; set; }
    }
}
